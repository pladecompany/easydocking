# Easy Docking (tmp_easydocking)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v1.quasar.dev/quasar-cli/quasar-conf-js).


### backgroundGeolocation plugin config
archivo BackgroundGeolocationFacade.java

* dentro de la carpeta res crear mipmap/icon.png

* PASO 1
    public static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
    };

    public static final String[] PERMISSIONS10 = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private String[] PERMISSIONSNEW = {};
* PASO 2
    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
        PERMISSIONSNEW = PERMISSIONS10;
    } else {
        PERMISSIONSNEW = PERMISSIONS;
    }
    logger.debug("Starting service");

    PermissionManager permissionManager = PermissionManager.getInstance(getContext());
    permissionManager.checkPermissions(Arrays.asList(PERMISSIONSNEW), new PermissionManager.
* PASO 3
    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
        PERMISSIONSNEW = PERMISSIONS10;
    } else {
        PERMISSIONSNEW = PERMISSIONS;
    }
    return hasPermissions(getContext(), PERMISSIONSNEW);

### Manifest Android xml
<uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
