const formatear = function(num, simbol) {
  simbol = simbol || '';
  let separador = '.'; // separador para los miles
  let sepDecimal = ','; // separador para los decimales
  num += '';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
  }
  return simbol + splitLeft + splitRight;
}



export default function precio(num, simbolo) {
  simbolo = simbolo || '';

  if (num == null || !num){
    num = 0;
  }
  num = num.toString().replace(/,/g, '.');
  num = Math.round(num * 100) / 100;
  return formatear(num.toFixed(2), simbolo).replace(/-/g, '');
}
