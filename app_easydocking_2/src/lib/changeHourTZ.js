import { DateTime } from 'luxon';

export default function changeHourTZ(time, from_zone, to_zone, format='HH:mm:ss') {
  return DateTime.fromSQL(time, {zone: from_zone})
    .setZone(to_zone)
    .toFormat(format);
}
