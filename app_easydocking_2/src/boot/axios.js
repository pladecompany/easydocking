import axios from 'axios'
import config from '../config'
import { LocalStorage } from 'quasar'
import auth from '../store'


// Crear instancia de axios.
const axiosAPI = axios.create({
  // baseURL: config.API_URL,
  baseURL: LocalStorage.getItem('devapiurl') || config.API_URL,
})

// Configurar los headers globalmente.
Object.assign(axiosAPI.defaults.headers.common, config.headers)

// Incluir token en cada peticion.
axiosAPI.interceptors.request.use(
  config => {
    const token = LocalStorage.getItem('TOKEN')
    const id = LocalStorage.getItem('ID')

    if (token) {
      config.headers.token = token
    }
    if (id) {
      config.headers.id_usuario = id
    }
    config.headers.tipo_usuario = 'chofer';
    return config
  },
  error => { Promise.reject(error) }
)

const UNAUTHORIZED = 401;


// Para que este disponible en los componentes y en store.
export default ({ store, Vue }) => {
  Vue.prototype.$axios = axios
  store.$axios = axios

  Vue.prototype.$api = axiosAPI
  store.$api = axiosAPI
  axios.interceptors.response.use(
    response => response,
    error => {
      if(error.response){
      const {status} = error.response;
        if (status === UNAUTHORIZED) {
          store.dispatch('auth/salir')
          .then(() => document.location.href = '#/Login')
        }
      }
      return Promise.reject(error);
    }
  );
  axiosAPI.interceptors.response.use(
    response => response,
    error => {
      if(error.response){
      const {status} = error.response;
        if (status === UNAUTHORIZED) {
          store.dispatch('auth/salir')
          .then(() => document.location.href = '#/Login')
        }
      }
      return Promise.reject(error);
    }
  );
}
