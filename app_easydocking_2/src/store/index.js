import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import geo from '../lib/geo'
import auth from './modulos/auth'
import notificacion from './modulos/notificacion'
import config  from '../config'
import { LocalStorage } from 'quasar'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      notificacion,
    },
    state:{
      setinter : null,
      telefono_soporte: null,
    },
    getters: {
      obtenersetinter(state){
        return state.setinter;
      },

      telefono_soporte: state => state.telefono_soporte,
    },
    mutations: {
      setInterval(state, v){
        state.setinter = v;
      },

      setTelefonoSoporte(state, tel) {
        state.telefono_soporte = tel;
      },
    },
    actions: {
      iniciarsetinterval({ commit, getters }, callback){
        //console.log('entre');
        return new Promise(function(resolve, reject) {
          clearInterval(getters.obtenersetinter);
          commit('setInterval',null);
          var inter = setInterval(async function(){
            var id_c = LocalStorage.getItem('DATOS');
            if(!id_c){
              clearInterval(getters.obtenersetinter);
              commit('setInterval',null);
              return;
            }
            await geo().then(ubicacionActual => {
              axios({
                url: config.API_URL + 'chofer/cambiar_ubicacion',
                data: {lat: ubicacionActual.lat,lon: ubicacionActual.lon, iden: id_c.id},
                method: 'PUT',
                headers: {
                  'token': LocalStorage.getItem('TOKEN'),
                },
              }).catch(err => {
                console.log(err);
              });
            })
            .catch(err => {
              console.log(err);
              reject();
            });
          }, 10000);
          commit('setInterval',inter);
        });
      },
      detenersetinterval({ commit, getters }, callback){
        return new Promise(function(resolve, reject) {
          clearInterval(getters.obtenersetinter);
          commit('setInterval',null);
        });
      },

      actualizarSoporte(ctx) {
        return new Promise((resolve, reject) => {
          this.$api
            .get('/config/telefono_soporte')
            .then(res => {
              const tel = res.data?.telefono_soporte;
              ctx.commit('setTelefonoSoporte', tel);
              resolve(tel);
            })
            .catch(reject);
        });
      },
    },
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
