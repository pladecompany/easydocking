import Vue from 'vue'
import defaultState from './state'


export function VACIAR (state) {
  state.replaceState(defaultState());
}

export function SET_ITEM (state, payload) {
  const key = payload[0];
  const item = payload[1];
  Vue.set(state.items, key, item)
}

export function DEL_ITEM (state, key) {
  Vue.delete(state.items, key);
}

export function SET_CANTIDAD_ITEM (state, args) {
  const key = args[0];
  const cantidad = args[1];

  const item = state.items[key];
  item.cantidad = cantidad;

  Vue.set(state.pedidos, key, item);
}
