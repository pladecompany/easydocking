import { LocalStorage } from 'quasar'


function defaultState() {
  return {
    notificaciones: [],
  }
}

const getters = {
  notificaciones: state => state.notificaciones,
  numero: state => state.notificaciones?.length,
  pendientes: state => state.notificaciones?.length ? state.notificaciones.filter(n => n.estatus==0).length : 0,
}

const mutations = {
  SET: (state, data) => { state.notificaciones = data },
  RESET_STATE: state => Object.assign(state, defaultState()),
}

const actions = {
  actualizar(ctx) {
    return new Promise((resolve, reject) => {
      const id_usu = ctx.rootGetters['auth/id'];
      if(id_usu){
	      this.$api
	        .get('/notificacion', {id_usu})
	        .then(res => {
	          ctx.commit('SET', res.data);
	          resolve(res.data);
	        })
	        .catch(reject)
    	}
    });
  },
}


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
