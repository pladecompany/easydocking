
const routes = [
  // Redirigir al home.
  {
    path: '/',
    beforeEnter: (to, from, next) => next('/Login')
  },
  // primera ruta
  {
    path: '/',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Login',path: '/Login', component: () => import('pages/Login.vue') },
      { name: 'Registro',path: '/Registro', component: () => import('pages/Registro.vue') },
    ]
  },

  // rutas sin layout

  // rutas con layout
  {
    path: '/Home',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Home',path: '/Home', component: () => import('pages/Home.vue') },
    ]
  },
  {
    path: '/Perfil',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Perfil',path: '/Perfil', component: () => import('pages/Perfil.vue') },
    ]
  },
  {
    path: '/Chat',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Chat',path: '/Chat', component: () => import('pages/Chat.vue') },
    ]
  },
  {
    path: '/Historial',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Historial',path: '/Historial', component: () => import('pages/Historial.vue') },
    ]
  },
  {
    path: '/Carros',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Carros',path: '/Carros', component: () => import('pages/Carros.vue') },
    ]
  },
  {
    path: '/Notificaciones',
    component: () => import('layouts/MainLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Notificaciones',path: '/Notificaciones', component: () => import('pages/Notificaciones.vue') },
    ]
  },
]

// No encontrado.
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue')
})

export default routes
