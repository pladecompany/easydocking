import axios from "axios";
import { Notify } from 'quasar'
import { LocalStorage } from 'quasar'
import Swal from 'sweetalert2'

export default {

    Firebase: function (id_usuario, config) {
        window.FirebasePlugin.getToken(function (tokenFirebase) {
            var data = {
                id_usuario: id_usuario ? id_usuario : null,
                tokenFirebase: tokenFirebase,
                tablausuario: "chofer",
                device: 1
            };
            axios({
                url: config.API_URL + 'tokensfirebase/agregar-eliminar-firebase',
                data: data,
                method: 'POST',
                headers: {
                    'token': LocalStorage.getItem('TOKEN'),
                },
            }).catch(err => {
                console.log(err);
            });
        });

        window.FirebasePlugin.onTokenRefresh(function (tokenFirebase) {
            var data = {
                id_usuario: id_usuario ? id_usuario : null,
                tokenFirebase: tokenFirebase,
                tablausuario: "chofer",
                device: 1
            };
            axios({
                url: config.API_URL + 'tokensfirebase/agregar-eliminar-firebase',
                data: data,
                method: 'POST',
                headers: {
                    'token': LocalStorage.getItem('TOKEN'),
                },
            }).catch(err => {
                console.log(err);
            });
        });
        window.FirebasePlugin.onMessageReceived(function (data) {
            // App abierta

            if (data.tap == "background") {
                setTimeout(function () {
                    if (data.tipo_noti == '0') {
                        location.href = "index.html#/" + data.ruta + "";
                    } else if (data.tipo_noti == '1') {
                        location.href = "index.html#/" + data.ruta + "/" + data.id_usable;
                    }
                }, 2000);
            } else {
                if (!data.encontrado) {
                    Notify.create({
                        message: 'Tienes una nueva notificación',
                        icon: 'notifications_active',
                        position: 'top'
                    });
                }
            }

            var googleMID = new Date().getTime();
            if (data.google.message_id) {
                googleMID = data.google.message_id;
            }
        });
    },
    clickNotification(data) {
        if (data.tipo_noti) {

            var Bell = document.querySelector('#BellNoti');
            if (!Bell.classList.contains("__hasNoti")) {
                Bell.classList.add("__hasNoti");
            }
            //alert(data.title);
            //M.toast({
            //    html: data.title
            //});
        }
    },
    Permissens: async function (config) {
        if(LocalStorage.getItem('tienepermisoubicacion')!=1)
            return;
        if (LocalStorage.getItem('ID')) {
            var locationAccuracy = cordova.plugins.locationAccuracy
            var permissions = cordova.plugins.permissions;

            function GeoBackRun() {
                var bgGeo = BackgroundGeolocation;
                bgGeo.configure({
                    url: config.API_URL + 'chofer/cambiar-ubicacion-backgeo/' + LocalStorage.getItem('ID') + '/' + LocalStorage.getItem('TOKEN'), // <-- Android ONLY:  your server url to send locations to
                    params: {
                        auth_token: 'user_secret_auth_token',    //  <-- Android ONLY:  HTTP POST params sent to your server when persisting locations.
                        foo: 'bar'                              //  <-- Android ONLY:  HTTP POST params sent to your server when persisting locations.
                    },
                    headers: {                                   // <-- Android ONLY:  Optional HTTP headers sent to your configured #url when persisting locations
                        'token': LocalStorage.getItem('TOKEN')
                    },
                    desiredAccuracy: bgGeo.HIGH_ACCURACY,
                    stationaryRadius: 50,
                    distanceFilter: 50,
                    notificationTitle: 'EasyDocking',
                    notificationText: 'ON',
                    debug: false,
                    startOnBoot: false,
                    stopOnTerminate: true,
                    locationProvider: bgGeo.ACTIVITY_PROVIDER,
                    interval: 10000,
                    fastestInterval: 5000,
                    activitiesInterval: 10000,
                    stopOnStillActivity: false,
                });
                bgGeo.on('location', (location) => {
                    bgGeo.startTask(taskKey => {
                        bgGeo.endTask(taskKey);
                    });
                });

                bgGeo.on('stationary', (stationaryLocation) => {
                });

                bgGeo.on('error', (error) => {
                });

                bgGeo.on('start', () => {
                });

                bgGeo.on('stop', () => {
                });
                bgGeo.on('background', function () {
                });
                bgGeo.checkStatus(function(status) {
                    if (!status.isRunning) {
                        bgGeo.start();
                    }
                });
            }

            locationAccuracy.canRequest(function (canRequest) {
                // alert('locationAccuracy.canRequest');
                if (!canRequest) {
                    permissions.requestPermission(permissions.ACCESS_COARSE_LOCATION, success, error);
                    function success() {
                        locationAccuracy.request(function () {
                            setTimeout(() => {
                                GeoBackRun();
                            }, 1500);
                        }, function (errr) {
                            if (errr.code !== locationAccuracy.ERROR_USER_DISAGREED) {
                                var showSettings = confirm('No se pudo acceder a su ubicación GPS, ¿ activar manualmente ?');
                                if (showSettings) {
                                    return bgGeo.showAppSettings();
                                }
                            }
                        }, locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
                    }
                    function error(e) {
                        // alert(e);
                    }
                } else {
                    setTimeout(() => {
                        GeoBackRun();
                    }, 1500);
                }
            });
        }
    },

    ubicacionPrimerPlano: function(config, debeSolicitarPermiso=false) {
        /*
        ** status:
        ** 1: aprobado
        ** 0: sin permiso y no se intento pedirlo
        ** -1: se rechazo el permiso
        ** -2: se rechazo activar la ubicacion (pero tiene permiso)
        */
        return new Promise(async(resolve, reject) => {

            var locationAccuracy = cordova.plugins.locationAccuracy
            var permissions = cordova.plugins.permissions;

            function comprobarPermiso() {
                return new Promise((resolve, reject) => {
                    permissions.checkPermission(
                        permissions.ACCESS_FINE_LOCATION,
                        (status) => resolve(status.hasPermission),
                        (error) => reject(error)
                    );
                });
            }

            function solicitarPermiso() {
                return new Promise((resolve, reject) => {
                    permissions.requestPermission(
                        permissions.ACCESS_FINE_LOCATION,
                        (status) => resolve(status.hasPermission),
                        (error) => reject(error)
                    );
                });
            }

            function activarUbicacion() {
                return new Promise((resolve, reject) => {
                    locationAccuracy.request(
                        (status) => resolve(true),
                        (error) => resolve(false),
                        locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY
                    );
                });
            }


            const permiso = await comprobarPermiso();
            if (permiso) {
                const activacion = await activarUbicacion();
                resolve(activacion ? 1 : -2);
            }
            else if (debeSolicitarPermiso) {
                const solicitud = await solicitarPermiso();
                if (solicitud) {
                    const activacion = await activarUbicacion();
                    resolve(activacion ? 1 : -2);
                }
                else {
                    resolve(-1);
                }
            }
            else {
                resolve(0);
            }
        });
    },

    locationCanRequest() {
        return new Promise((resolve, reject) => {
            const locationAccuracy = cordova.plugins.locationAccuracy;
            locationAccuracy.canRequest((canRequest) => {
                resolve(canRequest);
            });
        });
    },

    /*SolicitarubicacionPrimerPlano: async function(config) {
        //alert('entre');
        var bgGeo = BackgroundGeolocation;
        //bgGeo.stop();
        bgGeo.configure({
            url: config.API_URL + 'chofer/cambiar-ubicacion-backgeo/' + LocalStorage.getItem('ID') + '/' + LocalStorage.getItem('TOKEN'), // <-- Android ONLY:  your server url to send locations to
            params: {
                auth_token: 'user_secret_auth_token',    //  <-- Android ONLY:  HTTP POST params sent to your server when persisting locations.
                foo: 'bar'                              //  <-- Android ONLY:  HTTP POST params sent to your server when persisting locations.
            },
            headers: {                                   // <-- Android ONLY:  Optional HTTP headers sent to your configured #url when persisting locations
                'token': LocalStorage.getItem('TOKEN')
            },
            desiredAccuracy: bgGeo.HIGH_ACCURACY,
            stationaryRadius: 50,
            distanceFilter: 50,
            notificationTitle: 'EasyDocking',
            notificationText: 'ON',
            debug: false,
            startOnBoot: false,
            stopOnTerminate: true,
            locationProvider: bgGeo.ACTIVITY_PROVIDER,
            interval: 10000,
            fastestInterval: 5000,
            activitiesInterval: 10000,
            stopOnStillActivity: false,
        });
        bgGeo.on('location', (location) => {
            bgGeo.startTask(taskKey => {
                bgGeo.endTask(taskKey);
            });
        });

        bgGeo.on('stationary', (stationaryLocation) => {
        });

        bgGeo.on('error', (error) => {
        });

        bgGeo.on('start', () => {
        });

        bgGeo.on('stop', () => {
        });
        bgGeo.on('background', function () {
        	//if(LocalStorage.getItem('OPGEO')=='1'){
        	//	bgGeo.stop();
            //}
        });
        bgGeo.on('foreground', function() {
        	//if(LocalStorage.getItem('OPGEO')=='1'){
        	//	bgGeo.start();
            //}
        });

        bgGeo.checkStatus(function(status) {
            if (!status.isRunning) {
                bgGeo.start();
            }
        });
    },*/

    ubicacionSegundoPlano: async function(config) {
        var bgGeo2 = BackgroundGeolocation;
        bgGeo2.configure({
            url: config.API_URL + 'chofer/cambiar-ubicacion-backgeo/' + LocalStorage.getItem('ID') + '/' + LocalStorage.getItem('TOKEN'), // <-- Android ONLY:  your server url to send locations to
            params: {
                auth_token: 'user_secret_auth_token',    //  <-- Android ONLY:  HTTP POST params sent to your server when persisting locations.
                foo: 'bar'                              //  <-- Android ONLY:  HTTP POST params sent to your server when persisting locations.
            },
            headers: {                                   // <-- Android ONLY:  Optional HTTP headers sent to your configured #url when persisting locations
                'token': LocalStorage.getItem('TOKEN')
            },
            httpHeaders: {
              'token': LocalStorage.getItem('TOKEN'),
              'X-FOO': 'bar'
            },
            postTemplate: {
              latitude: '@latitude',
              longitude: '@longitude',
              OPGEO: LocalStorage.getItem('OPGEO'),
              foo: 'bar' // you can also add your own properties
            },
            desiredAccuracy: bgGeo2.HIGH_ACCURACY,
            stationaryRadius: 50,
            distanceFilter: 50,
            notificationTitle: 'EasyDocking',
            notificationText: 'ON',
            debug: false,
            startOnBoot: false,
            stopOnTerminate: true,
            locationProvider: bgGeo2.ACTIVITY_PROVIDER,
            interval: 10000,
            fastestInterval: 5000,
            activitiesInterval: 10000,
            stopOnStillActivity: false,
        });
        bgGeo2.on('location', (location) => {
            bgGeo2.startTask(taskKey => {
                bgGeo2.endTask(taskKey);
            });
        });

        bgGeo2.on('stationary', (stationaryLocation) => {
        });

        bgGeo2.on('error', (error) => {
        });

        bgGeo2.on('start', () => {
        });

        bgGeo2.on('stop', () => {

        });
        bgGeo2.on('background', function () {
            if(LocalStorage.getItem('OPGEO')=='1'){
              bgGeo2.stop();
            }
        });
        bgGeo2.on('foreground', function() {
            if(LocalStorage.getItem('OPGEO')=='1'){
              bgGeo2.start();
            }
        });
        bgGeo2.checkStatus(function(status) {
            if (!status.isRunning) {
                bgGeo2.start();
            }
        });
    },

    checkUbicacionSegundoPlano() {
        return new Promise((resolve, reject) => {
            const bgGeo = BackgroundGeolocation;
            bgGeo.checkStatus((status) => {
                resolve(status.isRunning);
            });
        });
    },

    /*cerrarUbicacionSegundoPlano: async function(config) {
        const bgGeo = BackgroundGeolocation;
        bgGeo.checkStatus(function(status) {
            if (status.isRunning) {
                bgGeo.stop();
            }
        });
    },*/

    /*
    ubicacionPrimerPlano: function(config) {
        return new Promise((resolve, reject) => {

            var locationAccuracy = cordova.plugins.locationAccuracy
            var permissions = cordova.plugins.permissions;

            locationAccuracy.canRequest(function (canRequest) {
                if (!canRequest) {
                    permissions.requestPermission(permissions.ACCESS_COARSE_LOCATION, success, error);

                    function success() {
                        locationAccuracy.request(
                            function () { resolve(true); },
                            function (errr) {
                                if (errr.code !== locationAccuracy.ERROR_USER_DISAGREED) {
                                    resolve(false);
                                }
                            },
                            locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY
                        );
                    }
                    function error(e) {
                        reject(e);
                    }
                }
                else {}
            });
        });
    },
    */

};
