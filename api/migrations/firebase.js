exports.up = knex => {
    return knex.schema.createTable("firebases", table => {
        table.increments("id_firebase").primary();
        table.integer("id_usuario").nullable();
        table.string("token",300);
       	table.string("tablausuario");
        table.string("device");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("firebases");
};
