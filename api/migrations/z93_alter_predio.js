exports.up = function(knex, Promise) {
    return knex.schema.alterTable("predio", table => {
        table.integer("dias_maximo", 50).defaultTo(0);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("predio", function(table) {
        table.dropColumn("dias_maximo");
    });

};
