exports.up = function(knex, Promise) {
    return knex.schema.alterTable("turno", table => {
        table.datetime('fecha_actualizado');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("turno", function(table) {
        table.dropColumn("fecha_actualizado");
    });
};
