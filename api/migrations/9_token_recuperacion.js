
exports.up = function(knex) {
  return knex.schema.createTable('token_recuperacion', table => {
    table.increments('id').primary();
    table.string('tipo_usuario', 20);
    table.integer('id_usuario').unsigned();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('token_recuperacion');
};
