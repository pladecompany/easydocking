
exports.up = function(knex) {
  return knex.schema.createTable('usuario_predio', table => {
    table.increments('id').primary();
    table.integer("id_predio").unsigned().references('id').inTable('predio').onDelete('CASCADE');
    table.integer("id_usuario").unsigned().references('id').inTable('usuario').onDelete('CASCADE');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('usuario_predio');
};
