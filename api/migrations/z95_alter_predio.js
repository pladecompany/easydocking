exports.up = function(knex, Promise) {
    return knex.schema.alterTable("predio", table => {
        table.string("zona_horaria", 50);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("predio", function(table) {
        table.dropColumn("zona_horaria");
    });
};
