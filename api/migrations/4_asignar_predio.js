
exports.up = function(knex) {
  return knex.schema.createTable('asignar_predio', table => {
    table.increments('id').primary();
    table.integer("id_chofer").unsigned().references('id').inTable('chofer');
    table.integer("id_predio").unsigned().references('id').inTable('predio');
    table.enu("aprobacion", ['-1', '0', '1']);
    table.text("motivo");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('asignar_predio');
};
