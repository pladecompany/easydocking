
exports.up = function(knex) {
  return knex.schema.createTable('detalles_predio', table => {
    table.increments('id').primary();
    table.integer("id_predio").unsigned().references('id').inTable('predio').onDelete('CASCADE');
    table.string('tipo', 100);
    table.string('titulo', 100);
    table.text('opciones');
    table.string('variable', 100);
    table.boolean("segmenta").defaultTo(false);
    table.integer("min");
    table.integer("max");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('detalles_predio');
};
