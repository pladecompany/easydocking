exports.up = function(knex, Promise) {
    return knex.schema.createTable("rooms_chats", table => {
        table.increments("id").primary();
        table.text("id_room");
        table.integer("id_usuario");
        table.text("tipo_usuario");
        table.integer("tipo_msj"); // 1 normal, 2 image, 3 especial
        table.integer("visto"); // 0 no visto, 1 visto
        table.text("mensaje");
        table.datetime("fec_reg_mensaje");
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists("rooms_chats");
};
