
exports.up = function(knex) {
  return knex.schema.createTable('predio', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.string('telefono', 50);
    table.text("url");
    table.text("url_alternativa");
    table.string('encargado', 100);
    table.string('telefono_enc', 50);
    table.text("observacion");
    table.boolean("seguimiento").defaultTo(false);
    table.integer("id_cod_telefono").unsigned().references('id').inTable('cod_pais');
    table.string("lat", 100).defaultTo(null);
    table.string("lon", 100).defaultTo(null);
    table.enu("estatus", ['0', '1']).defaultTo('0');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('predio');
};
