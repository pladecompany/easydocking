
exports.up = function(knex) {
  return knex.schema.createTable('cod_pais', table => {
    table.increments('id').primary();
    table.string('cod', 100);
    table.string('nombre', 100);
    table.text("bandera");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('cod_pais');
};
