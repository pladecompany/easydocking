exports.up = knex => {
    return knex.schema.createTable("emails", table => {
        table.increments("id").primary();
        table.text("encabezado");
        table.text("cuerpo");

        table.enu("tipo_correo", [
            'turno_aprobado',
            'turno_rechazado',
            'turno_editado',
            'turno_presentado'
        ]).unique();
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("emails");
};
