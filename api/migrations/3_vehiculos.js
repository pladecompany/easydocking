
exports.up = function(knex) {
  return knex.schema.createTable('vehiculo', table => {
    table.increments('id').primary();
    //table.string('tipo', 20);
    table.string('patente', 20);
    table.string('patente2', 20);
    table.date('vencimiento');
    table.date('vencimiento_seguro');
    table.date('vencimiento_vtv');
    table.text("img");
    table.text("img_vtv");
    table.text("img_seguro");
    table.text("img_clausula");
    table.text("img_art");
    table.boolean("estatus").defaultTo(false);
    table.enu("aprobacion", ['-1', '0', '1']).defaultTo('0'); // -1:Rechazado, 0:Esperando, 1:Aprobado
    table.integer("id_chofer").unsigned().references('id').inTable('chofer');
    table.integer("tipo").unsigned().references('id').inTable('tipo_vehiculo');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('vehiculo');
};
