
exports.up = function(knex) {
  return knex.schema.createTable('detalles_turno', table => {
    table.increments('id').primary();
    table.integer("id_turno").unsigned().references('id').inTable('turno').onDelete('CASCADE');
    table.string('titulo', 100);
    table.text('respuesta');
    table.boolean("segmenta").defaultTo(false);
    table.boolean("esimagen").defaultTo(false);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('detalles_turno');
};
