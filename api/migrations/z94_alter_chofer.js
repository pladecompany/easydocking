exports.up = function(knex, Promise) {
    return knex.schema.alterTable("chofer", table => {
        table.integer("estatus_coordenada").defaultTo(0);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("chofer", function(table) {
        table.dropColumn("estatus_coordenada");
    });

};
