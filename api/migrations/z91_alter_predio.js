exports.up = function(knex, Promise) {
    return knex.schema.alterTable("predio", table => {
        table.integer("horas_maxima", 50);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("predio", function(table) {
        table.dropColumn("horas_maxima");
    });

};
