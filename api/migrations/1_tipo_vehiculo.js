
exports.up = function(knex) {
  return knex.schema.createTable('tipo_vehiculo', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.boolean("vtv").defaultTo(false);
    table.boolean("dpatente").defaultTo(false);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('tipo_vehiculo');
};
