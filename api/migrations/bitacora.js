
exports.up = function(knex) {
    return knex.schema.createTable('bitacora', table => {
      table.increments('id').primary();
      table.integer("usuario_predio")
      table.integer("usuario")
      table.integer("predio", 50);
      table.text("accion")
      table.datetime("fecha")
    });
  };
  
  exports.down = function(knex) {
    return knex.schema.dropTableIfExists('bitacora');
  };