exports.up = function(knex, Promise) {
    return knex.schema.alterTable("detalles_predio", table => {
        table.boolean('obligatorio').defaultTo(false);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("detalles_predio", function(table) {
        table.dropColumn("obligatorio").defaultTo(false);
    });

};
