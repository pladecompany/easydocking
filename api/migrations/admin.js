
exports.up = function(knex) {
  return knex.schema.createTable('admin', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.string('usuario', 100);
    table.string('correo', 50);
    table.string('telefono', 50);
    table.string('pass', 100);
    table.string('token', 100).nullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('admin');
};
