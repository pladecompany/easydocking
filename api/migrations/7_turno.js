
exports.up = function(knex) {
  return knex.schema.createTable('turno', table => {
    table.increments('id').primary();
    table.string('nombre_chofer', 200);
    table.datetime('fecha');
    table.datetime('fecha_fin');
    table.datetime('fecha_ingresado');
    //table.enu("estatus", ['-1', '0', '1', '2']).defaultTo('0');
    table.integer("estatus").defaultTo('0');
    table.integer("id_chofer").unsigned().references('id').inTable('chofer');
    table.integer("id_vehiculo").unsigned().references('id').inTable('vehiculo');
    table.integer("id_predio").unsigned().references('id').inTable('predio');
    table.string('id_tanda', 50);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('turno');
};
