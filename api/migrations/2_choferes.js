
exports.up = function(knex) {
  return knex.schema.createTable('chofer', table => {
    table.increments('id').primary();
    table.string('nombre', 100);
    table.string('apellido', 100);
    table.string('correo', 50);
    table.integer("id_cod_telefono").unsigned().references('id').inTable('cod_pais');
    table.string('telefono', 50);
    table.string('pass', 100);
    table.text("dni");
    table.boolean("estatus").defaultTo(false);
    table.string('token', 100).nullable();
    table.string("lat_cho", 100).defaultTo(null);
    table.string("lon_cho", 100).defaultTo(null);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('chofer');
};
