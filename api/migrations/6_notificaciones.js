
exports.up = function(knex) {
  const USUARIOS = ['admin', 'chofer', 'todos'];

  return knex.schema.createTable('notificaciones', table => {
    table.increments('id').primary();
    table.text('titulo');
    table.text('contenido');
    table.datetime('fecha').defaultTo(knex.fn.now());
    table.enu('usuario_receptor', USUARIOS);
    table.integer('id_receptor').unsigned();
    table.boolean('estatus').defaultTo(false);
    table.integer('id_usable').unsigned();
    table.integer('id_emisor').unsigned();
    table.enu('usuario_emisor', USUARIOS);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('notificaciones');
};
