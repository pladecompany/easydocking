
exports.up = function(knex) {
  return knex.schema.createTable('usuario', table => {
    table.increments('id').primary();
    table.integer("id_predio").unsigned().references('id').inTable('predio').onDelete('CASCADE');
    table.string('nombre', 50);
    table.string('correo', 100);
    table.string('pass', 100);
    table.string('token', 100).nullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('usuario');
};
