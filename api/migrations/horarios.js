
exports.up = function(knex) {
    return knex.schema.createTable('horarios', table => {
      table.increments('id').primary();
      table.date("fecha");
      table.time("hora_a");
      table.time("hora_b");
      table.integer("frecuencia", 50);
      table.integer("disponibilidad", 50);
      table.text("tipo");
      table.text("tipo_operacion"); // arrays by comma
      table.integer("predio", 50);
      table.text('motivo');
      table.string('id_tanda', 50);
    });
  };
  
  exports.down = function(knex) {
    return knex.schema.dropTableIfExists('horarios');
  };