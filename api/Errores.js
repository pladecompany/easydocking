class ValidationError extends Error {}
class AuthError extends Error {}

module.exports = {
    ValidationError,
    AuthError
};
