const hashPass = require('../functions/hashPass');

exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries
  console.log("Borrando Datos...");
  let y = 0;
  let x = true;

  x = await knex("admin").del();
  x = await knex("detalles_predio").del();
  x = await knex("predio").del();
  x = await knex("cod_pais").del();
  x = await knex("tipo_vehiculo").del();

  await knex("admin").insert({
    id: 1,
    nombre: "admin",
    usuario: "admin",
    correo: "admin@admin.com",
    pass: await hashPass('12345'),
  });

  await knex("admin").insert({
    id: 2,
    nombre: "admineasydocking",
    usuario: "admineasydocking",
    correo: "easydocking_admin",
    pass: await hashPass('easydocking12345'),
  });

  await knex("cod_pais").insert([
    {id: 1, cod : '+54', nombre: 'Argentina', bandera : 'img/flag-icons/Argentina.png'},
    {id: 2, cod : '+56', nombre: 'Chile', bandera : 'img/flag-icons/Chile.png'},
    {id: 3, cod : '+57', nombre: 'Colombia', bandera : 'img/flag-icons/Colombia.png'},
    {id: 4, cod : '+593', nombre: 'Ecuador', bandera : 'img/flag-icons/Ecuador.png'},
    {id: 5, cod : '+58', nombre: 'Venezuela', bandera : 'img/flag-icons/Venezuela.png'}
  ]);

  await knex("predio").insert({
    id: 1,
    nombre: "Plade",
    telefono: "000-00000001",
    url: 'https://easydocking.ar/srv_01/',
    encargado: "Encargado 1",
    telefono_enc: "000-00000002",
    id_cod_telefono: 5,
    lat: '9.041939964471917',
    lon: '-69.74381089403502'
  });

  await knex("detalles_predio").insert({
    id: 1,
    id_predio: 1,
    tipo: "Texto",
    titulo: "Edad"
  });


  await knex("tipo_vehiculo").insert({
    id: 1,
    nombre: 'Auto',
    vtv: 0,
    dpatente: 0,
  });
  await knex("tipo_vehiculo").insert({
    id: 2,
    nombre: 'Camión',
    vtv: 1,
    dpatente: 0,
  });
  await knex("tipo_vehiculo").insert({
    id: 3,
    nombre: 'Chasis',
    vtv: 0,
    dpatente: 0,
  });
  await knex("tipo_vehiculo").insert({
    id: 4,
    nombre: 'Semi',
    vtv: 1,
    dpatente: 1,
  });
  await knex("tipo_vehiculo").insert({
    id: 5,
    nombre: 'Tractor',
    vtv: 1,
    dpatente: 0,
  });

};
