exports.seed = function(knex) {


// Deletes ALL existing entries
return knex('emails').del()
  .then(function () {

    // Inserts seed entries
    return knex('emails').insert([
      {
        id: 1,
        encabezado: 'easydocking le informa a $nombre',
        cuerpo: 'Se ha aprobado un turno',
        tipo_correo: 'turno_aprobado'
      },
      {
        id: 2, encabezado: 'easydocking le informa a $nombre',
        cuerpo: 'Se ha rechazado un turno',
        tipo_correo: 'turno_rechazado'
      },
      {
        id: 3, encabezado: 'easydocking le informa a $nombre',
        cuerpo: 'Se ha editado un turno',
        tipo_correo: 'turno_editado'
      },
      {
        id: 4, encabezado: 'easydocking le informa a $nombre',
        cuerpo: 'Se ha presentado un turno',
        tipo_correo: 'turno_presentado'
      },
    ]);

  });


};
