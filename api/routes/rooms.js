//llamar al modelo
const Rooms_chats = require("../models/Rooms_chats");
const Rooms_list = require("../models/Rooms_list");

//Llamar otras librerias
const validar = require('./sesion2');
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const paginate = require("paginate-array");
const _ = require("lodash");
const fs = require("fs");
const mime = require('mime-types');
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
//Obligatorio para trabajar con FormData

const multer = require("multer");
const upload = multer({
  limits: {
    fieldSize: 8 * 1024 * 1024,
  }
});

var router = express.Router();
async function buscarRoom(predio, chofer) {
  let salon = undefined;
  let rooms_chats = await Rooms_list.query().catch(err => { });
  if (rooms_chats) {
    for (let index = 0; index < rooms_chats.length; index++) {
      const element = rooms_chats[index];
      let bu, bud = false;
      let usuarios = JSON.parse(element.usuarios);
      for (let jindex = 0; jindex < usuarios.length; jindex++) {
        const jelement = usuarios[jindex];
        if (String(jelement.id) === String(chofer) && jelement.tipo === "chofer") {
          bu = true;
        }
        if (String(jelement.id) === String(predio) && jelement.tipo === "predio") {
          bud = true;
        }
      }
      if (bu && bud) {
        salon = element.id_room;
        break;
      }
    }
  }
  if (!salon) {
    let add = await Rooms_list.query()
      .insert({
        id_room: `${new Date().getTime()}${predio}`,
        usuarios: JSON.stringify([{ id: predio, tipo: "predio" }, { id: chofer, tipo: "chofer" }]),
        fec_reg_room: moment().format("YYYY-MM-DD HH:mm:ss")
      })
      .catch(err => {

      });
    salon = add.id_room;
  }
  return salon;
}
//registrar
async function add_room(req, res) {
  data = req.body;

  let errr = false;
  // inicio validaciones
  if (!data.id_room) errr = "Ingresa un salón";
  else if (!data.tipo_usuario) errr = "Ingresa un tipo de usuario";
  else if (!data.id_usuario) errr = "Ingresa un usuario";
  else if (!data.mensaje) errr = "Ingresa un mensaje";
  else if (!data.fec_reg_mensaje) errr = "Ingresa una fecha";

  //fin validaciones
  if (errr) return AR.enviarDatos({ r: false, msj: errr }, res);


  await Rooms_chats.query()
    .insert({
      id_room: data.id_room,
      id_usuario: data.id_usuario,
      tipo_usuario: data.tipo_usuario,
      mensaje: data.mensaje,
      tipo_msj: 1,
      visto: 0,
      fec_reg_mensaje: data.fec_reg_mensaje
    })
    .then(async resp => {
      let search = await Rooms_chats.query().findOne({ id: resp.id }).catch(err => { });
      AR.enviarDatos(search, res);
      global.IO.emit(`recibir-mensaje-${resp.id_room}`, search);
      buscarUsuario(resp.id_room, data.tipo_usuario);
    })
    .catch(err => {
      err.r = false;
      err.msj = 'Error al registrar Mensaje';
      res.send(err);
    });
}

router.post("/add-from-predio", upload.none(), validar('admin'), async (req, res, next) => {
  add_room(req, res)
});

router.post("/add", upload.none(), validar('chofer'), async (req, res, next) => {
  add_room(req, res)
});

async function buscarUsuario(room, tipo) {
  let send = false;
  for (let index = 0; index < global.USUARIOSC.length; index++) {
    const element = global.USUARIOSC[index];
    if (String(element.user.roomChat) === String(room) && String(element.user.tabla) !== String(tipo)) {
      await Rooms_chats.query()
        .where({ id_room: room })
        .where({ visto: 0 })
        .where("tipo_usuario", "=", tipo)
        .then(async trae => {
          let ids = []; for (let index = 0; index < trae.length; index++)  ids.push(trae[index].id);
          await Rooms_chats.query()
            .whereIn("id", ids)
            .update({ visto: 1 })
            .catch(err => { });
        })
        .catch(err => { });
      send = true;
      break;
    }
  }
  // if (!send) {
  Rooms_list.query().findOne({ id_room: room }).then(async trae => {
    if (trae) {
      let users = JSON.parse(trae.usuarios);
      for (let index = 0; index < users.length; index++) {
        const element = users[index];
        if (String(element.tipo) !== String(tipo)) {
          // console.log(element);
          // ENVIAR PUSH EN ESTE LUGAR A ESTE USUARIO
          let datap = {
            id_usuario: element.id,
            title: "Tienes un nuevo mensaje",
            body: "Verifica tus chats, tienes algunos mensajes sin leer.",
            id_usable: room,
            tipo_noti: "0",
            encontrado: `${send ? "true" : ""}`,
            ruta: "Chat",
            tablausuario: element.tipo
          };
          // console.log(datap);
          await Firebase.enviarPushNotification(datap, function (datan) { console.log(datan); });
          break;
        }
      }
    }
  }).catch(err => {

  })
  // }
}
router.post("/file/:filename", async function (req, res) {
  let file = __dirname.replace("routes", "") + "public/uploads/rooms/" + req.params.filename;
  if (!fs.existsSync(file)) {
    AR.enviarError("Error Not Found", res, 404);
    return;
  } else {
    let body = fs.readFileSync(file);
    let retorno = `data:${mime.lookup(file)};base64,${body.toString('base64')}`;
    AR.enviarDatos(retorno, res);
    return;
  }
});

router.get("/file/:filename", function (req, res) {
  let file = __dirname.replace("routes", "") + "public/uploads/rooms/" + req.params.filename;
  if (!fs.existsSync(file)) {
    AR.enviarError("Error Not Found", res, 404);
    return;
  } else {
    res.sendFile(file);
    return;
  }
});

async function add_img(req, res) {
  data = req.body;

  let errr = false;
  // inicio validaciones
  if (!data.id_room) errr = "Ingresa un salón";
  else if (!data.tipo_usuario) errr = "Ingresa un tipo de usuario";
  else if (!data.id_usuario) errr = "Ingresa un usuario";
  else if (!data.mensaje) errr = "Ingresa una mensaje";
  else if (!data.fec_reg_mensaje) errr = "Ingresa una fecha";


  //fin validaciones
  if (errr) return AR.enviarDatos({ r: false, msj: errr }, res);
  let extension = data.mensaje.split("data:")[1]; extension = extension.split(";")[0]; extension = extension.split("/")[1];

  let file = "file_room_" + data.id_room + new Date().getTime() + "." + extension;
  fs.writeFile(__dirname.replace("routes", "") + "public/uploads/rooms/" + file, new Buffer.from(data.mensaje.split(",")[1], 'base64'), 'base64', function (err) {
    console.log(err);
  });

  await Rooms_chats.query()
    .insert({
      id_room: data.id_room,
      id_usuario: data.id_usuario,
      tipo_usuario: data.tipo_usuario,
      mensaje: file,
      tipo_msj: 2,
      visto: 0,
      fec_reg_mensaje: data.fec_reg_mensaje
    })
    .then(async resp => {
      let search = await Rooms_chats.query().findOne({ id: resp.id }).catch(err => { });
      AR.enviarDatos(search, res);
      global.IO.emit(`recibir-mensaje-${resp.id_room}`, search);
      buscarUsuario(resp.id_room, data.tipo_usuario);
    })
    .catch(err => {
      err.r = false;
      err.msj = 'Error al registrar Mensaje';
      res.send(err);
    });
}
router.post("/add-img-from-predio", upload.none(), validar('admin'), async (req, res, next) => {
  add_img(req, res)
});

router.post("/add-img", upload.none(), validar('chofer'), async (req, res, next) => {
  add_img(req, res)
});
async function delete_room(req, res) {
  let data = req.query;
  const eliminado = await Rooms_chats.query()
    .delete()
    .where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });
  if (eliminado) {
    AR.enviarDatos({ msj: "Mensaje eliminado exitosamente" }, res);
    global.IO.emit(`mensaje-eliminado-${String(req.params.room)}`, req.params.id);
  }
  else { AR.enviarDatos("0", res); }
}
router.delete("/delete-from-predio/:id/:room", upload.none(), validar('admin'), async (req, res) => {
  delete_room(req, res)
});
router.delete("/delete/:id/:room", upload.none(), validar('chofer'), async (req, res) => {
  delete_room(req, res)
});
async function delete_room_all(req, res) {
  let data = req.query;
  const eliminado = await Rooms_chats.query()
    .delete()
    .where({ id_room: req.params.id_room })
    .catch(err => {
      console.log(err);
      res.send(err);
    });
  if (eliminado) {
    AR.enviarDatos({ msj: "Mensajes eliminados exitosamente" }, res);
    global.IO.emit(`mensajes-eliminados-${req.params.id_room}`, req.params.id_room);
  }
  else { AR.enviarDatos("0", res); }
}
router.delete("/delete-all-from-predio/room/:id_room", upload.none(), validar('admin'), async (req, res) => {
  delete_room_all(req, res)
});
router.delete("/delete-all/room/:id_room", upload.none(), validar('chofer'), async (req, res) => {
  delete_room_all(req, res)
});

async function get_room(req, res) {
  let salon = undefined;
  let data = req.params;
  salon = await buscarRoom(data.predio, data.id_usuario);
  AR.enviarDatos({ room: salon }, res);
}
router.get("/get-room-from-predio/:id_usuario/:predio", upload.none(), validar('admin'), async (req, res) => {
  get_room(req, res)
});
router.get("/get-room/:id_usuario/:predio", validar('chofer'), async (req, res) => {
  get_room(req, res)
});
async function global_get(req, res) {
  let data = req.params;

  await Rooms_chats.query()
    .where({ id_room: data.salon })
    .orderBy("id", "asc")
    .then(rooms_chats => {
      var arr = _.orderBy(rooms_chats, ["id"], ["desc"]);
      const paginateCollection = paginate(arr, data.num_page, data.num_items);
      AR.enviarDatos(paginateCollection, res);
    });
}
router.get("/getone-from-predio/:num_page/:num_items/:salon", upload.none(), validar('admin'), async (req, res) => {
  global_get(req, res)
});
router.get("/:num_page/:num_items/:salon", validar('chofer'), async (req, res) => {
  global_get(req, res)
});
async function marcar_vistos(req, res) {
  let data = req.params;
  Rooms_chats.query()
    .where({ id_room: data.salon })
    .where({ visto: 0 })
    .where("tipo_usuario", "!=", data.tipo)
    .then(trae => {
      let ids = []; for (let index = 0; index < trae.length; index++)  ids.push(trae[index].id);
      Rooms_chats.query()
        .whereIn("id", ids)
        .update({ visto: 1 })
        .catch(err => { });
    })
    .catch(err => { });
  AR.enviarDatos({ msj: true }, res);
}
router.put("/marcar-vistos-from-predio/:id_usuario/:tipo/:salon", validar('admin'), async (req, res) => {
  marcar_vistos(req, res)
});
router.put("/marcar-vistos/:id_usuario/:tipo/:salon", validar('chofer'), async (req, res) => {
  marcar_vistos(req, res)
});

async function obtener_novistos(req, res) {
  let data = req.params;
  Rooms_list.query()
    .then(hals => {
      let salones = [];
      let detalles_new = [];
      for (let index = 0; index < hals.length; index++) {
        const element = hals[index];
        let set = false;
        let users = JSON.parse(element.usuarios);
        for (let jindex = 0; jindex < users.length; jindex++) {
          const jelement = users[jindex];
          if (String(jelement.id) === String(data.id_usuario) && String(String(jelement.tipo)) === String(data.tipo)) {
            salones.push(element.id_room);
            set = true;
          }
        }
        for (let jindex = 0; jindex < users.length; jindex++) {
          const jelement = users[jindex];
          if (String(jelement.id) === String(data.id_usuario) && String(String(jelement.tipo)) === String(data.tipo)) {
            // NO HACER NADA
          } else {
            if (set) detalles_new.push(jelement);
          }
        }

      }
      let detalles = detalles_new.reduce((acc, item) => {
        if (!acc.includes(item)) {
          acc.push(item);
        }
        return acc;
      }, [])

      Rooms_chats.query()
        .whereIn("id_room", salones)
        .where({ visto: 0 })
        .where("tipo_usuario", "!=", data.tipo)
        .then(trae => {
          let details = [];
          for (let index = 0; index < detalles.length; index++) {
            const element = detalles[index];
            for (let jindex = 0; jindex < trae.length; jindex++) {
              const jelement = trae[jindex];
              if (String(jelement.id_usuario) === String(element.id) && String(String(jelement.tipo_usuario)) === String(element.tipo)) {
                details.push(element);
              }
            }
          }
          let details_new = details.reduce((acc, item) => {
            if (!acc.includes(item)) {
              acc.push(item);
            }
            return acc;
          }, [])
          AR.enviarDatos({ no_vistos: trae.length, otros: details_new }, res);
        })
        .catch(err => {
          AR.enviarError("Error Not Found", res, 404);
        });
    })
    .catch(err => {
      AR.enviarError("Error Not Found", res, 404);
    });
}
router.get("/obtener-from-predio/no-vistos/:id_usuario/:tipo", validar('admin'), async (req, res) => {
  obtener_novistos(req, res)
});

router.get("/obtener/no-vistos/:id_usuario/:tipo", validar('chofer'), async (req, res) => {
  obtener_novistos(req, res)
});


module.exports = router;