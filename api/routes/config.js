const validar = require('./sesion2');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();
const jsonfile = require('jsonfile');
const config = require('../config');

const CONFIG_FILE = __dirname+ '/../configuracion.json';

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500


jsonfile.readFile(CONFIG_FILE)
  .then()
  .catch(() => {
    jsonfile.writeFile(CONFIG_FILE, {}).catch(console.error);
  });


router.get('/telefono_soporte', async(req, res) => {
  const config = await jsonfile.readFile(CONFIG_FILE).catch(console.error);
  AR.enviarDatos({ telefono_soporte: config ? config.telefono_soporte : '' }, res);
});


router.post('/telefono_soporte', upload.none(), validar('admin'), async(req, res) => {
  const telefono_soporte = parseInt(req.body.telefono_soporte, 10);

  if (!telefono_soporte)
    return res.sendStatus(400);

  // const file = __dirname+ '/../configuracion.json';
  let obj = await jsonfile.readFile(CONFIG_FILE).catch(console.error);

  if (!obj)
    obj = {};

  obj.telefono_soporte = telefono_soporte.toString();
  await jsonfile.writeFile(CONFIG_FILE, obj).catch(console.error);

  AR.enviarDatos({telefono_soporte: obj.telefono_soporte}, res);
});


router.get('/zonas_horarias', async(req, res) => {
  AR.enviarDatos(config['zonas_horarias'] || [], res);
});

module.exports = router;
