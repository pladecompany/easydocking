//llamar al modelo
const Horarios = require("../models/Horarios");
const Bitacora = require('../models/Bitacora');
const Turnos = require('../models/Turno');

//Llamar otras librerias
const validar = require('./sesion2');

const request = require('request');
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const _ = require("lodash");
const sanitizeString = require('../functions/sanitizeString');
var Firebase = require("../routes/firebase");
Firebase = new Firebase();
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer({
    limits: {
        fieldSize: 8 * 1024 * 1024,
    }
});

var router = express.Router();
function serrch_coinciden(arr, arrd) {
    let sum = 0;
    for (let ffindex = 0; ffindex < arr.length; ffindex++) {
        for (let ffindexd = 0; ffindexd < arrd.length; ffindexd++)
            if (String(arr[ffindex]).toUpperCase() == String(arrd[ffindexd]).toUpperCase()) {
                sum++;
            }
    }
    return sum === arrd.length ? true : false;
}
function serrch_coincidend(arr, arrd) {
    let sum = 0;
    for (let ffindex = 0; ffindex < arr.length; ffindex++) {
        for (let ffindexd = 0; ffindexd < arrd.length; ffindexd++)
            if (String(arr[ffindex]).toUpperCase() == String(arrd[ffindexd]).toUpperCase()) {
                sum++;
            }
    }
    return sum === arrd.length &&  arr.length === arrd.length ? true : false;
}

async function saveList(array, usuario) {
    let data = array || [];
    let guardados = [];
    let no_guardados = [];
    let message, r;
    for (let index = 0; index < data.length; index++) {
        const element = data[index];

        let changue = false;
        if (element.tipo) {
            if (element.tipo.length <= 0) {
                changue = true;
            }
        } else if (!element.tipo) {
            changue = true;
        }
        if (changue) {
            element["tipo"] = [{
                text: "todos",
                operaciones: ["todos"]
            }];
        }

        let id_tanda = new Date().getTime();
        let validados = 0;
        let tandas = {};

        for (let j = 0; j < element.tipo.length; j++) {
            let TIPO = element.tipo[j];
            let variable = TIPO.text

            variable = String(variable).toUpperCase();

            let trae, traed;
            let string = "";
            if (!TIPO.operaciones) {
                TIPO.operaciones = ["todos"];
            } else {
                if (TIPO.operaciones.length <= 0) {
                    TIPO.operaciones = ["todos"];
                }
            }
            for (let indexope = 0; indexope < TIPO.operaciones.length; indexope++) string = string + `FIND_IN_SET('${TIPO.operaciones[indexope]}', tipo_operacion) <> 0 ${TIPO.operaciones.length > 0 && (indexope + 1) < TIPO.operaciones.length ? ' AND ' : ''}`;

            if (!trae) {

                await Horarios.query()
                    .where({ fecha: element.fecha, tipo: variable, predio: element.predio })
                    .whereRaw(`(${string} AND ('${element.hora_a}' >= hora_a AND '${element.hora_a}' <= hora_b) OR ('${element.hora_b}' >= hora_a AND '${element.hora_b}' <= hora_b))`)
                    .then(async traed_find => {
                        traed = traed_find[0];
                        if (traed) {
                            let new_trae = false;
                            for (let hindex = 0; hindex < traed_find.length; hindex++) {
                                let helement = traed_find[hindex];
                                if (serrch_coincidend(helement.tipo_operacion.split(","), TIPO.operaciones)) {
                                    new_trae = true;
                                    if (tandas[`${helement.id_tanda}`]) {
                                        tandas[`${helement.id_tanda}`] = (tandas[`${helement.id_tanda}`] + 1);
                                    } else {
                                        tandas[`${helement.id_tanda}`] = 1;
                                    }
                                }
                            }

                            traed = new_trae;
                        }
                    }).catch(err => { console.log(err); });
            }
            let set_new = JSON.parse(JSON.stringify(element));

            if (!trae && !traed) {
            } else {
                validados = (validados + 1);
                no_guardados.push({ error: "Ya existe.", ...set_new });
            }
        }

        if(Object.keys(tandas).length > 1 && element.tipo.length > 1) {
            validados = (element.tipo.length * 100);
            Object.keys(tandas).map(async function(__obj, __index) {
                if (tandas[__obj] > 1) {
                    validados = element.tipo.length;
                }
            });
        }

        if (validados != element.tipo.length) {

            for (let j = 0; j < element.tipo.length; j++) {
                let TIPO = element.tipo[j];
                let variable = TIPO.text

                variable = String(variable).toUpperCase();

                if (!TIPO.operaciones) {
                    TIPO.operaciones = ["todos"];
                } else {
                    if (TIPO.operaciones.length <= 0) {
                        TIPO.operaciones = ["todos"];
                    }
                }
                let set_new = JSON.parse(JSON.stringify(element));
                set_new.tipo_operacion = String(TIPO.operaciones.join()).toUpperCase();
                set_new.tipo = variable;
                set_new.id_tanda = id_tanda;
                await Horarios.query()
                    .insert(set_new)
                    .then(async save => {
                        guardados.push(save);
                        let accion = `${usuario.name} creó un nuevo horario para el día: (${moment(set_new.fecha).format("DD-MM-YYYY")}) de ${set_new.hora_a} a ${set_new.hora_b}, variable: (${set_new.tipo}), operaciones: ${set_new.tipo_operacion}, frecuencia: ${set_new.frecuencia}, disponibilidad: ${set_new.disponibilidad}.`;
                        await Bitacora.query()
                        .insert({predio: element.predio, usuario_predio: usuario.id, accion: accion, fecha: moment().format("YYYY-MM-DD HH:mm")})
                        .then(() => { })
                        .catch(() => { });

                    }).catch(err => {
                        console.log(err);
                        no_guardados.push({ error: "Error not found", ...set_new });
                    });
            }
        }
    }

    if (guardados.length > 0) {
        message = `Registro exitoso, ${guardados.length} horario${((guardados.length <= 1) ? '' : 's')} guardado${((guardados.length <= 1) ? '' : 's')}.`; r = true;
    } else {
        message = `No se registro ningún horario.`; r = false;
    }

    return {
        guardados: guardados,
        no_guardados: no_guardados,
        r: r,
        msg: message
    };
}
async function saveListbyFecha(array, motivo, predio, tipo, hora_a, hora_b, frecuencia, disponibilidad, op, usuario) {
    let data = array || [];
    let guardados = [];
    let no_guardados = [];
    let message, r;
    for (let index = 0; index < data.length; index++) {
        const fecha = data[index];
        let changue = false;
        if (tipo) {
            if (tipo.length <= 0) {
                changue = true;
            }
        } else if (!tipo) {
            changue = true;
        }
        if (changue) {
            tipo = [{
                text: "todos",
                operaciones: ["todos"]
            }];
        }
        let id_tanda = new Date().getTime();
        let tandas = {};
        let validados = 0;
        for (let j = 0; j < tipo.length; j++) {
            let TIPO = tipo[j];
            let variable = TIPO.text

            variable = String(variable).toUpperCase();

            let string = "";
            if (!TIPO.operaciones) {
                TIPO.operaciones = ["todos"];
            } else {
                if (TIPO.operaciones.length <= 0) {
                    TIPO.operaciones = ["todos"];
                }
            }

            let set_data = {
                "fecha": moment(fecha).format("YYYY-MM-DD"),
                "hora_a": hora_a,
                "hora_b": hora_b,
                "frecuencia": frecuencia,
                "disponibilidad": disponibilidad,
                "tipo": variable,
                "tipo_operacion": String(TIPO.operaciones.join()).toUpperCase(),
                "predio": predio,
                "motivo": motivo,
                "id_tanda": id_tanda
            };
            for (let indexope = 0; indexope < TIPO.operaciones.length; indexope++) string = string + `FIND_IN_SET('${TIPO.operaciones[indexope]}', tipo_operacion) <> 0 ${TIPO.operaciones.length > 0 && (indexope + 1) < TIPO.operaciones.length ? ' AND ' : ''}`;
            if (op) {
                await Horarios.query().where({ fecha: set_data.fecha, tipo: variable, predio: predio })
                    .whereRaw(string)
                    .then(async trae_find => {
                        for (let indexn = 0; indexn < trae_find.length; indexn++) {
                            const element = trae_find[indexn];
                            if (serrch_coinciden(element.tipo_operacion.split(","), TIPO.operaciones)) {
                                await Horarios.query()
                                    .delete().where({ id_tanda: element.id_tanda, predio: predio })
                                    .catch(() => { });
                                //await Borrarturno([element.id_tanda]);

                            }
                        }

                    }).catch(err => { console.log(err); });

                await Horarios.query()
                    .insert(set_data)
                    .then(async save => {
                        guardados.push(save);
                        let accion = `${usuario.name} creó un nuevo horario no laborable para el día: (${moment(set_data.fecha).format("DD-MM-YYYY")}) de ${set_data.hora_a} a ${set_data.hora_b}, variable: (${set_data.tipo}), operaciones: ${set_data.tipo_operacion}, frecuencia: ${set_data.frecuencia}, disponibilidad: ${set_data.disponibilidad}.`;
                        await Bitacora.query()
                        .insert({predio: predio, usuario_predio: usuario.id, accion: accion, fecha: moment().format("YYYY-MM-DD HH:mm")})
                        .then(() => { })
                        .catch(() => { });
                    }).catch(() => {
                        no_guardados.push({ error: "Error not found", ...set_data });
                    });
            } else {
                let trae, traed;

                if (!trae) {
                    await Horarios.query()
                        .where({ fecha: set_data.fecha, tipo: variable, predio: predio })
                        .whereRaw(`(${string} AND ('${hora_a}' >= hora_a AND '${hora_a}' <= hora_b) OR ('${hora_b}' >= hora_a AND '${hora_b}' <= hora_b))`)
                        .then(async traed_find => {
                            traed = traed_find[0];
                            if (traed) {
                                let new_trae = false;
                                for (let hindex = 0; hindex < traed_find.length; hindex++) {
                                    let helement = traed_find[hindex];
                                    if (serrch_coincidend(helement.tipo_operacion.split(","), TIPO.operaciones)) {
                                        new_trae = true;
                                        if (tandas[`${helement.id_tanda}`]) {
                                            tandas[`${helement.id_tanda}`] = (tandas[`${helement.id_tanda}`] + 1);
                                        } else {
                                            tandas[`${helement.id_tanda}`] = 1;
                                        }
                                    }
                                }
                                traed = new_trae;
                            }
                        }).catch(err => { console.log(err); });
                }
                if (!trae && !traed) {
                } else {
                    validados = (validados + 1);
                    no_guardados.push({ error: "Ya existe.", ...set_data });
                }
            }
        }
        if (!op) {
            if(Object.keys(tandas).length > 1 && tipo.length > 1) {
                validados = (tipo.length * 100);
                Object.keys(tandas).map(async function(__obj, __index) {
                    if (tandas[__obj] > 1) {
                        validados = tipo.length;
                    }
                });
            }
        }
        if (!op && validados != tipo.length) {
            for (let j = 0; j < tipo.length; j++) {
                let TIPO = tipo[j];
                let variable = TIPO.text

                variable = String(variable).toUpperCase();

                if (!TIPO.operaciones) {
                    TIPO.operaciones = ["todos"];
                } else {
                    if (TIPO.operaciones.length <= 0) {
                        TIPO.operaciones = ["todos"];
                    }
                }

                let set_data = {
                    "fecha": moment(fecha).format("YYYY-MM-DD"),
                    "hora_a": hora_a,
                    "hora_b": hora_b,
                    "frecuencia": frecuencia,
                    "disponibilidad": disponibilidad,
                    "tipo": variable,
                    "tipo_operacion": String(TIPO.operaciones.join()).toUpperCase(),
                    "predio": predio,
                    "motivo": motivo,
                    "id_tanda": id_tanda
                };
                await Horarios.query()
                .insert(set_data)
                .then(async save => {
                    guardados.push(save);
                    let accion = `${usuario.name} creó un nuevo horario para el día: (${moment(set_data.fecha).format("DD-MM-YYYY")}) de ${set_data.hora_a} a ${set_data.hora_b}, variable: (${set_data.tipo}), operaciones: ${set_data.tipo_operacion}, frecuencia: ${set_data.frecuencia}, disponibilidad: ${set_data.disponibilidad}.`;
                    await Bitacora.query()
                    .insert({predio: predio, usuario_predio: usuario.id, accion: accion, fecha: moment().format("YYYY-MM-DD HH:mm")})
                    .then(() => { })
                    .catch(() => { });
                }).catch(() => {
                    no_guardados.push({ error: "Error not found", ...set_data });
                });
            }
        }

    }

    if (guardados.length > 0) {
        message = `Registro exitoso, ${guardados.length} horario${((guardados.length <= 1) ? '' : 's')} guardado${((guardados.length <= 1) ? '' : 's')}.`; r = true;
    } else {
        message = `No se registro ningún horario.`; r = false;
    }

    return {
        guardados: guardados,
        no_guardados: no_guardados,
        r: r,
        msg: message
    };
}

let getDaysArray = function (start, end) {
    let arr = [];
    let init = new Date(start);
    let finish = new Date(end.setDate(end.getDate() + 1));
    for (let dt = new Date(init.setDate(init.getDate() + 1)); dt <= finish; dt.setDate(dt.getDate() + 1)) {
        arr.push(new Date(dt));
    }
    return arr;
};

router.post("/add-one", upload.none(), validar('usuario_predio'), async (req, res, next) => {
    let save = await saveList(req.body.list || [], req.body.usuario);
    if (save.r) {
        AR.enviarDatos(save, res);
    } else {
        AR.enviarError(save.msg, res, 500);
    }
    return;
});

router.post("/add-list", upload.none(), validar('usuario_predio'), async (req, res, next) => {
    let save = await saveList(req.body.list || [], req.body.usuario);
    if (save.r) {
        AR.enviarDatos(save, res);
    } else {
        AR.enviarError(save.msg, res, 500);
    }
    return;
});

router.post("/add-list-by-fechas", upload.none(), validar('usuario_predio'), async (req, res, next) => {
    let data = req.body;
    let dates_list = getDaysArray(new Date(data.fecha1), new Date(data.fecha2));
    dates_list.map((v) => v.toISOString().slice(0, 10)).join("");

    let save = await saveListbyFecha(dates_list || [], data.motivo || "", data.predio, data.tipo || [], data.hora_a, data.hora_b, data.frecuencia, data.disponibilidad, false, data.usuario);
    if (save.r) {
        AR.enviarDatos(save, res);
    } else {
        AR.enviarError(save.msg, res, 500);
    }
    return;
});

router.post("/add-list-by-fechas-by-daysweeks", upload.none(), validar('usuario_predio'), async (req, res, next) => {
    let data = req.body;
    let dates_list = getDaysArray(new Date(data.fecha1), new Date(data.fecha2));
    dates_list.map((v) => v.toISOString().slice(0, 10)).join("");

    let new_dates = [];
    let days = ["lun.", "mar.", "mié.", "jue.", "vie.", "sáb.", "dom."];
    for (let index = 0; index < dates_list.length; index++) {
        const element = dates_list[index];
        let mydate = element;
        let weekDayName = moment(mydate).format('ddd');
        for (let jindex = 0; jindex < days.length; jindex++) if (weekDayName == days[jindex] && (jindex >= data.dia1 && jindex <= data.dia2)) new_dates.push(element);
    }

    let save = await saveListbyFecha(new_dates || [], data.motivo || "", data.predio, data.tipo || [], data.hora_a, data.hora_b, data.frecuencia, data.disponibilidad, false, data.usuario);
    if (save.r) {
        AR.enviarDatos(save, res);
    } else {
        AR.enviarError(save.msg, res, 500);
    }
    return;
});

router.post("/add-one-no-laborable", upload.none(), validar('usuario_predio'), async (req, res, next) => {
    let data = req.body;
    let save = { msj: "Error al registrar." };

    save = await saveListbyFecha([data.fecha], data.motivo || "", data.predio, data.tipo || [], "00:00", "23:59", 0, 0, true, data.usuario);


    if (save.r) {
        AR.enviarDatos(save, res);
    } else {
        AR.enviarError(save.msg, res, 500);
    }
    return;
});

router.post("/add-list-no-laborable-by-fechas", upload.none(), validar('usuario_predio'), async (req, res, next) => {
    let data = req.body;
    let save = { msg: "Error al registrar." };

    let dates_list = getDaysArray(new Date(data.fecha1), new Date(data.fecha2));
    dates_list.map((v) => v.toISOString().slice(0, 10)).join("");

    save = await saveListbyFecha(dates_list || [], data.motivo || "", data.predio, data.tipo || [], "00:00", "23:59", 0, 0, true, data.usuario);

    if (save.r) {
        AR.enviarDatos(save, res);
    } else {
        AR.enviarError(save.msg, res, 500);
    }
    return;
});
async function Borrarturno(horario) {
    for (let index = 0; index < horario.length; index++) {
        let id_tanda = typeof horario[index] == "string" ? horario[index] : horario[index].id_tanda;
        await Turnos.query()
        .where({id_tanda: id_tanda})
        .then(async trae => {
            for (let jindex = 0; jindex < trae.length; jindex++) {
                const element = trae[jindex];
                let datap = {
                    id_usuario: element.id_chofer,
                    title: `Turno no disponible (${moment(element.fecha).format("DD-MM-YYYY HH:mm")} - ${moment(element.fecha_fin).format("HH:mm")})`,
                    body: "Se han detectado cambios administrativos en los turnos.",
                    tipo_noti: "2",
                    encontrado: false,
                    tablausuario: "chofer"
                  };
                //   console.log(datap);
                  await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
            }
            await Turnos.query()
            .delete()
            .where({id_tanda: id_tanda})
            .catch(err => {

            });
        })
        .catch(err => { });

    }
}
router.post("/delete/:id", upload.none(), validar('usuario_predio'), async (req, res) => {
    let data = req.params;
    let usuario = req.body.usuario;
    let find = await Horarios.query()
               .findOne({id: data.id})
               .catch(err => { });
    /*const INFO = await Turno.query().where('id_tanda',find.id_tanda).catch(err => { });
    if(INFO[0])
        return AR.enviarError("El horario no puede ser eliminado, información usada.", res, 500);
    */
    const eliminado = await Horarios.query()
        .delete()
        .where({ id_tanda: find.id_tanda, predio: usuario.predio })
        .catch(err => {
            res.send(err);
            return;
        });
    if (eliminado && find) {
        AR.enviarDatos({ msg: "Horario eliminado exitosamente." }, res);
        let accion = `${usuario.name} eliminó el horario para el día: (${moment(find.fecha).format("DD-MM-YYYY")}) de ${find.hora_a} a ${find.hora_b}, variable: (${find.tipo}), operaciones: ${find.tipo_operacion}, frecuencia: ${find.frecuencia}, disponibilidad: ${find.disponibilidad}.`;
        await Bitacora.query()
        .insert({predio: usuario.predio, usuario_predio: usuario.id, accion: accion, fecha: moment().format("YYYY-MM-DD HH:mm")})
        .then(() => { })
        .catch(() => { });
        //await Borrarturno([find.id_tanda]);
    } else {
        AR.enviarError("Horario no disponible.", res, 500);
    }
});

router.post("/delete-by-fecha/", upload.none(), validar('usuario_predio'), async (req, res) => {
    let data = req.body;
    let finds = await Horarios.query()
    .select("id_tanda")
    .where({ fecha: data.fecha })
    .groupBy("id_tanda")
    .catch(err => {
        res.send(err);
        return;
    });

    /*const INFO = await Turno.query().where('id_tanda',finds[0].id_tanda).catch(err => { });
    if(INFO[0])
        return AR.enviarError("El horario no puede ser eliminado, información usada.", res, 500);
*/
    const eliminado = await Horarios.query()
        .delete()
        .where({ fecha: data.fecha, predio: data.usuario.predio})
        .catch(err => {
            res.send(err);
            return;
        });
    if (eliminado) {
        AR.enviarDatos({ msg: "Horarios eliminados exitosamente." }, res);
        let accion = `${data.usuario.name} eliminó los horarios para el día: (${moment(data.fecha).format("DD-MM-YYYY")}), motivo: ${data.motivo}.`;
        await Bitacora.query()
        .insert({predio: data.usuario.predio, usuario_predio: data.usuario.id, accion: accion, fecha: moment().format("YYYY-MM-DD HH:mm")})
        .then(() => { })
        .catch(() => { });
        //await Borrarturno(finds || []);
    } else {
        AR.enviarError("Horario no disponible.", res, 500);
    }
});

router.post("/delete-by-fechas/", upload.none(), validar('usuario_predio'), async (req, res) => {
    let data = req.body;
    let message = "";
    let r;
    let count = 0;
    let dates_list = getDaysArray(new Date(data.fecha1), new Date(data.fecha2));
    dates_list.map((v) => v.toISOString().slice(0, 10)).join("");
    for (let index = 0; index < dates_list.length; index++) {
        const fecha = dates_list[index];
        let finds = await Horarios.query()
        .select("id_tanda")
        .where({ fecha: moment(fecha).format("YYYY-MM-DD") })
        .groupBy("id_tanda")
        .catch(err => {
        });
        /*const INFO = await Turno.query().where('id_tanda',finds[0].id_tanda).catch(err => { });
        if(!INFO[0]){*/
            let eli = await Horarios.query()
                .delete()
                .where({ fecha: moment(fecha).format("YYYY-MM-DD"), predio: data.usuario.predio })
                .catch(err => {
                });
            if (eli) {
                count = (count + 1);
                let accion = `${data.usuario.name} eliminó los horarios para el día: (${moment(fecha).format("DD-MM-YYYY")}).`;
                await Bitacora.query()
                .insert({predio: data.usuario.predio, usuario_predio: data.usuario.id, accion: accion, fecha: moment().format("YYYY-MM-DD HH:mm")})
                .then(() => { })
                .catch(() => { });
                //await Borrarturno(finds || []);
            }
        //}
    }
    if (count > 0) {
        message = `Exito, ${count} horario${((count <= 1) ? '' : 's')} eliminado${((count <= 1) ? '' : 's')}.`; r = true;
    } else {
        message = `No se eliminó ningún horario.`; r = false;
    }
    AR.enviarDatos({ msg: message, r: r }, res);

});

router.get("/:predio/:fecha/:tipo?", validar('usuario_predio'), async (req, res) => {
    let data = req.params;
    let filter = {
        fecha: data.fecha, predio: data.predio
    }
    if (!data.tipo) {
        data.tipo = "";
    } else {
        data.tipo = String(data.tipo).toUpperCase();
    }
    await Horarios.query()
        .having('tipo', 'LIKE', '%' + sanitizeString(data.tipo) + '%')
        .orHaving('tipo_operacion', 'LIKE', '%' + sanitizeString(data.tipo) + '%')
        .where(filter)
        .orderBy("hora_a", "asc")
        .then(horarios => {
            AR.enviarDatos(horarios, res);
        });
});
router.get("/calendar/month/:fecha/:predio", validar('usuario_predio'), async (req, res) => {
    let data = req.params;

    let dates_list = getDaysArray(new Date(data.fecha + "01"), new Date(data.fecha + "31"));
    dates_list.map((v) => v.toISOString().slice(0, 10)).join("");
    let horarios = [];
    for (let index = 0; index < dates_list.length; index++) {
        const fecha = moment(dates_list[index]).format("YYYY-MM-DD")
        await Horarios.query().where({ fecha: fecha, predio: data.predio })
            .then(times => {
                if (times.length > 0) {
                    horarios.push({
                        key: new Date().getTime(),
                        highlight: true,
                        customData: {
                            title: `(${times.length})`,
                            class: "",
                        },
                        dates: moment(dates_list[index]).add(1, "days").format("YYYY-MM-DD"),
                    });
                }
            }).catch(err => { console.log(err) })
    }

    AR.enviarDatos(horarios, res);
    return;
});

router.get("/zonas", async(req,res) => {
    if (!req.headers.query.hasOwnProperty('tipo_usuario'))
        return res.sendStatus(401);

    request(
        { url: 'https://worldtimeapi.org/api/timezone' },
        (error, response, body) => {
          if (error || response.statusCode !== 200) {
            return res.sendStatus(500);
          }

          res.json(JSON.parse(body));
        }
    )
});

module.exports = router;
