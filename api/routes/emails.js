const Emails = require('../models/Emails');

const validar = require('./sesion2');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();
const ejs = require('ejs');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

const correo_base = __dirname + '/../mail/templates/Turno_correo_base.html';

// const rex_varname = /\$(\w*)/g;
const rex_body = /<body[^>]*>(.*?)<\/body>/;


const variables = [
  ['$nombre', 'NOMBRE CHOFER'],
  ['$fecha_turno', 'FECHA TURNO'],
  ['$hora_turno', 'HORA TURNO'],
  ['$predio', 'PREDIO'],
];


router.get('/', /*validar('admin'),*/ async(req,res) => {
  const render = req.query.hasOwnProperty('render');
  const emails = await Emails.query().catch(console.error);

  if (!render) {
    return AR.enviarDatos(emails, res);
  }

  const data = variables;

  // Reemplazar variables
  for (let [varname, value] of variables) {
    for (let email of emails) {
      if (email.encabezado.includes(varname)) {
        email.encabezado = email.encabezado.split(varname).join(value);
      }
      if (email.cuerpo.includes(varname)) {
        email.cuerpo = email.cuerpo.split(varname).join(value);
      }
    }
  }

  AR.enviarDatos(emails, res);
});



router.get('/:id(\\d+)', validar('admin'), async(req,res) => {
  const email = await Emails.query().findById(req.params.id).catch(console.error);
  if (!email) {
    return res.sendStatus(HTTP_NOT_FOUND);
  }
  AR.enviarDatos(email, res);
});



router.put('/:id(\\d+)', validar('admin'), async(req,res) => {
  const datos = {
    encabezado: req.body.encabezado,
    cuerpo: req.body.cuerpo,
  };

  let err = false;
  if (!datos.encabezado)
    err = 'Ingrese el encabezado del email';
  else if (!datos.cuerpo)
    err = 'Ingrese el cuerpo del email';

  await Emails.query()
    .updateAndFetchById(req.params.id, datos)
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});



router.get('/variables', validar('admin'), async(req, res) => {
  AR.enviarDatos(variables, res);
});



module.exports = router;
