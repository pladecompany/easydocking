const Cod_Telefono = require('../models/Cod_Telefono');

const AR = require('../ApiResponser');
const validar = require('./sesion2');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

// Listar
router.get('/', async(req, res) => {
  const params = req.query;

  const Query = Cod_Telefono.query();

  // Paginacion.
  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined);


  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});


// Crear.
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const nombre = req.body.nombre;
  const cod = req.body.codigo;
  const bandera = req.body.bandera;

  let err = false;
  if (!nombre)
    err = 'Ingrese el nombre';
  else if (!cod)
    err = 'Ingrese el código';
  else if (!bandera)
    err = 'Ingrese la bandera';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  await Cod_Telefono.query()
    .insert({ nombre, bandera, cod })
    .then(c => AR.enviarDatos(c,res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});


router.put('/:id(\\d+)', upload.none(), validar('admin'), async(req, res) => {
  const nombre = req.body.nombre;
  const cod = req.body.codigo;
  const bandera = req.body.bandera;

  let err = false;
  if (!nombre)
    err = 'Ingrese el nombre';
  else if (!cod)
    err = 'Ingrese el código';
  else if (!bandera)
    err = 'Ingrese la bandera';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Cod_Telefono.query()
    .findById(req.params.id)
    .patch({ nombre, cod, bandera })
    .then(() => AR.enviarDatos({r:true}, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});


router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Cod_Telefono.query()
    .findById(req.params.id)
    .del()
    .then(() => AR.enviarDatos({r:true}, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

module.exports = router;
