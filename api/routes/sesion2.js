/* sesion2.js
**
** const sesion2 = require('./sesion2.js');
**
** sesion2('admin', 'cliente', ...) //-> Function(req, res, next){...}
**
** sesion2() //Validacion general.
*/

const { knex } = require("objection");

/* Los usuarios a validar. Deben tener una columna `token` en comun */
const MODELOS = {
  admin: require("../models/Admin"),
  chofer: require("../models/Chofer"),
  usuario_predio: require("../models/Usuario"),
}

/* Mantener las tablas en cache */
const TABLAS = Object.values(MODELOS).map(modelo => modelo.tableName);

function unionAllTokens(builder){
  for (let tabla of TABLAS)
    builder.unionAll(q => q.select('token').from(tabla));
}

async function validador(ModeloUsuario, token) {
    const usuarios = await ModeloUsuario.query().count('* AS count').first().where({token}).catch(console.log);

    if(usuarios && usuarios.count > 0){
      return true
    }

    return false;
}

async function validador_general(token) {
  /*
  ** Para validar a todos se unen las tablas en la columna `token`
  **  y se comprueba si el token existe.
  */
  return await knex.select().from({tokens: unionAllTokens}).where('token', '=', token).first();
}

module.exports = function sesion2(...con_acceso) {
  return async function (req, res, next) {
    const token = req.body.token || req.query.token || req.headers.token || req.params.token;
    if (!token) return res.sendStatus(401);

    if(con_acceso.length === 0 || con_acceso[0] == 'general') {
      let acceso = await validador_general(token);
      if (acceso) return next();
    }
    else {
      for (tipo_usuario of con_acceso) {
        let acceso = await validador(MODELOS[tipo_usuario], token);
        if (acceso) {
          req.headers['tipo_usuario_validado'] = tipo_usuario;
          return next();
        }
      }
    }

    res.sendStatus(401);
  }
}
