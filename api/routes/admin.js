const Admin = require('../models/Admin');

const validar = require('./sesion2');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();
const hashPass = require('../functions/hashPass');
const jsonfile = require('jsonfile');
const sanitizeString = require('../functions/sanitizeString');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

router.get('/', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Admin.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  await Query
    .then(resp => AR.enviarDatos(resp, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/:id(\\d+)', /*validar('admin'),*/ async(req, res) => {
  await Admin.query().findById(req.params.id)
    .then(resp => {
      if (!resp) {
        return res.sendStatus(HTTP_NOT_FOUND);
      }
      resp.type = "admin";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.post('/', upload.none(), async(req, res) => {
  const data = req.body;

  let errr = false;
  if (!data.nombre) errr = "Ingresa el nombre";
  else if (!data.usuario) errr = "Ingresa el usuario";
  else if (!data.pass) errr = "Ingresa la contraseña";
  else if (!data.email) errr = "Ingresa el email";

  if (errr)
    return AR.enviarError(errr, res, HTTP_BAD_REQUEST);


  const datos = {
    nombre: data.nombre,
    usuario: data.usuario,
    pass: await hashPass(data.pass),
    correo: data.email,
  };


  await Admin.query()
    .insert(datos)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      resp.msg = "Se registro correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.put('/editar', upload.none(), /*validar('admin'),*/ async(req, res) => {
  const data = req.body;
  let errr = false;

  if (!data.nombre) errr = "Ingresa el nombre";
  else if (!data.usuario) errr = "Ingresa el usuario";
  else if (!data.email) errr = "Ingresa el email";

  if (errr)
    return AR.enviarError(errr, res, HTTP_BAD_REQUEST);


  const datos = {
    nombre: data.nombre,
    usuario: data.usuario,
    correo: data.email,
  };
  if(data.pass)
    datos.pass = await hashPass(data.pass);
  await Admin.query()
    .patchAndFetchById(data.ide, datos)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      resp.msg = "Se editó correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.delete('/:id(\\d+)', /*validar('admin'),*/ async(req, res) => {
  await Admin.query()
    .findById(req.params.id)
    .delete()
    .then(() => {
      resp = {};
      resp.r=true;
      resp.msg="Eliminado correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});



module.exports = router;
