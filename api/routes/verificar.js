const moment = require("moment");
moment.locale("es");
const Turno = require('../models/Turno');
const Vehiculo = require('../models/Vehiculo');
const Notificacion = require('../models/Notificacion');
const { raw } = require('objection');

class Verificar {

  static verificarturnos() {
    var dif = moment().subtract(12, 'hours')
    var fecha = moment(dif).format('YYYY-MM-DD HH:mm:ss')
    //console.log('entre',fecha);
      Turno.query()
        .where('fecha','<',fecha)
        .whereRaw('(estatus="0" OR estatus="1")')
        //.where('estatus','0')
        .then(async ret => {
          //console.log(ret);
          if(ret.length>0){
            for (var i = 0; i < ret.length; i++) {
              await Turno.query().updateAndFetchById(ret[i].id, {estatus: '3'}).catch(err => {console.log(err)});
            }
          }
        }).catch(err => {console.log(err)});
  }

  static async verificardocumentos() {
    let registros = await Vehiculo.query()
      .where('vencimiento', '<', raw('DATE_ADD(CURDATE(), INTERVAL 1 DAY)'))
      .catch(console.error);

    let seguros = await Vehiculo.query()
      .where('vencimiento_seguro', '<', raw('DATE_ADD(CURDATE(), INTERVAL 1 DAY)'))
      .catch(console.error);

    let vtv = await Vehiculo.query()
      .whereNotNull('vencimiento_vtv')
      .where('vencimiento_vtv', '<', raw('DATE_ADD(CURDATE(), INTERVAL 1 DAY)'))
      .catch(console.error);


    for (let veh of registros) {
      Notificacion.emitir({
        titulo: `Alerta de vencimiento del registro`,
        contenido: `El registro del vehículo ${veh.patente} vence el dia ${moment(veh.vencimiento).format('DD')}`,
        id_receptor: veh.id_chofer,
        usuario_receptor: Notificacion.usuario.CHOFER,
        push: true,
      });
    }

    for (let veh of seguros) {
      Notificacion.emitir({
        titulo: `Alerta de vencimiento del seguro`,
        contenido: `El seguro del vehículo ${veh.patente} vence el dia ${moment(veh.vencimiento_seguro).format('DD')}`,
        id_receptor: veh.id_chofer,
        usuario_receptor: Notificacion.usuario.CHOFER,
        push: true,
      });
    }

    for (let veh of vtv) {
      Notificacion.emitir({
        titulo: `Alerta de vencimiento de VTV`,
        contenido: `El VTV del vehículo ${veh.patente} vence el dia ${moment(veh.vencimiento_vtv).format('DD')}`,
        id_receptor: veh.id_chofer,
        usuario_receptor: Notificacion.usuario.CHOFER,
        push: true,
      });
    }

    console.log(registros, seguros);

  }

}

module.exports = Verificar;
