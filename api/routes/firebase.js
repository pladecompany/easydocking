//llamar al modelo
const Firebase = require("../models/Firebase");
// const express = require("express");
const config = require("../knexfile");


var FCMN = require('fcm-node');
var serverKey = config.production.firebase.firebase_key_server;
var fcmn = new FCMN(serverKey);
var fcm = require('fcm-notification');
var FCM = new fcm(__dirname.replace("firebase") + '/../servicios/privatekey.json');

var htmlToText = require('html-to-text');
function html_a_text(html) {
    var text = htmlToText.fromString(html, {
      format: {
        heading: function (elem, fn, options) {
          var h = fn(elem.children, options);
          return '====\n' + h.toUpperCase() + '\n====';
        }
      }
    });

    return text;
}
const sendPush = (data) => {
  return new Promise((resolve, reject) => {
    fcmn.send(data, async function(err, res){
      if (err) {
        reject(err);  // calling `reject` will cause the promise to fail with or without the error passed as an argument
        return;        // and we don't want to go any further
      }
      resolve(res);
      return;
    })
  })
}
const sendPushD = (data) => {
  return new Promise((resolve, reject) => {
    FCM.send(data, async function(err, res){
      if (err) {
        reject(err);  // calling `reject` will cause the promise to fail with or without the error passed as an argument
        return;        // and we don't want to go any further
      }
      resolve(res);
      return;
    })
  })
}
let firebase = function(){


    this.enviarPushNotification = async function(data, retorno){
                data.id_usuario = "" + data.id_usuario + "";
               
                await Firebase.query().where({id_usuario: String(data.id_usuario), tablausuario: String(data.tablausuario)}).groupBy('token').then(async Usuarios => {
                  
                    if(Usuarios.length >= 1 || Usuarios.length >= "1") {
                      let resul = Usuarios || [];
                        if (!data.icon) {
                          data.icon = "https://ytuqestashaciendo.com/easydocking/icons/favicon-128x128.png";
                        }
                        
                        for(let i = 0 ; i < resul.length; i++){
                            let fila = resul[i];
                            
                            let messaged = { 
                                registration_ids: [fila.token],
                                notification: {
                                    title: html_a_text(data.title),
                                    body: html_a_text(data.body),
                                    icon: data.icon,
                                    sound : "default",
                                    badge: "1"
                                },
                                data: data,
                                priority: 'high',
                                timeToLive: 86400,
                                time_to_live: 86400,
                            };  
                            if (data.icon) {
                              messaged.android = {
                                notification: {
                                  icon: data.icon
                                }
                              };
                            }
                          await sendPush(messaged).then(data => console.log(data)).catch(async err => {
                            console.error(err);
                                let message = {
                                  token: fila.token, 
                                  notification: {
                                      title: html_a_text(data.title), 
                                      body: html_a_text(data.body)
                                  },
                                  data: data
                              };
                              if (fila.device == 1 || fila.device == 2) {
                                message.android = {
                                  ttl: 3600 * 1000,
                                  priority: 'normal',
                                  notification: {
                                    title: html_a_text(data.title), 
                                    body: html_a_text(data.body)
                                  }
                                };
                                if (data.icon) {
                                  message.android.notification.icon = data.icon;
                                }
                              } else if (fila.device == 3) {
                                message.webpush = {
                                  notification: {
                                    title: html_a_text(data.title), 
                                    body: html_a_text(data.body)
                                  }
                                };
                                if (data.icon) {
                                  message.webpush.notification.icon = data.icon;
                                }
                            }
                            await sendPushD(message).then(data => console.log(data)).catch(err => console.error(err));
                          });
                        }
                        retorno({r: true});
                    } else {
                       retorno({r: false});
                    }
                })
                .catch(err => {
                     retorno({r: true});
                });
    }
}

module.exports = firebase;
