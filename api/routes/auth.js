const Admin = require('../models/Admin');
const Usuario = require('../models/Usuario');
const Chofer = require('../models/Chofer');
const TokenRecuperacion = require('../models/TokenRecuperacion');

const AR = require('../ApiResponser');
const validar = require('./sesion2');
const comparePass = require('../functions/comparePass');
const hashPass = require('../functions/hashPass');
const genToken = require('../functions/genToken');
const sendMail = require('../mail');
const upload = require('multer')();
const router = require('express').Router();


const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_UNAUTHORIZED = 401,
  HTTP_FORBIDDEN = 403,
  HTTP_NOT_FOUND = 404,
  HTTP_SERVER_ERROR = 500



router.post('/login', upload.none(), async(req, res) => {
  const correo = req.body.correo || req.body.email;
  const pass = req.body.pass || req.body.password || req.body.clave;

  if (!correo || !pass) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const admin = await Admin.query()
    .findOne('correo', correo)
    .catch(console.error);

  if (!admin) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  const access = await comparePass(pass, admin.pass);
  if (!access){
    return AR.enviarError('Datos incorrectos', res, HTTP_UNAUTHORIZED);
    // return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  admin.token = await genToken(64);
  admin.type = "admin";
  delete admin.pass;

  await Admin.query()
    .findById(admin.id)
    .patch({ token: admin.token })
    .catch(console.error);

  AR.enviarDatos(admin, res);
});

router.post('/login-user', upload.none(), async(req, res) => {
  const correo = req.body.correo || req.body.email;
  const pass = req.body.pass || req.body.password || req.body.clave;

  if (!correo || !pass) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const usu = await Usuario.query()
    .findOne('correo', correo)
    .withGraphFetched('predios')
    .catch(console.error);

  if (!usu) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  const access = await comparePass(pass, usu.pass);
  if (!access){
    return AR.enviarError('Datos incorrectos', res, HTTP_UNAUTHORIZED);
    // return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  usu.token = await genToken(64);
  usu.type = "user-predio";
  delete usu.pass;

  await Usuario.query()
    .findById(usu.id)
    .patch({ token: usu.token })
    .catch(console.error);

  AR.enviarDatos(usu, res);
});

router.get('/user-predio/:id(\\d+)', /*validar('admin'),*/ async(req, res) => {
  await Usuario.query().findById(req.params.id)
    .then(resp => {
      if (!resp) {
        return res.sendStatus(HTTP_NOT_FOUND);
      }
      resp.type = "user-predio";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.post('/login_chofer', upload.none(), async(req, res) => {
  const cod = req.body.cod;
  const tlf = req.body.tlf;
  const pass = req.body.pass;

  if (!cod || !tlf || !pass) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const chofer = await Chofer.query()
    .findOne('telefono', tlf)
    .where('id_cod_telefono', cod)
    .catch(console.error);
    //console.log(req.body,chofer);
  if (!chofer) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  if(chofer.estatus==0)
    return AR.enviarError('Pendiente de aprobación', res, HTTP_UNAUTHORIZED);

  const access = await comparePass(pass, chofer.pass);
  if (!access){
    return AR.enviarError('Datos incorrectos', res, HTTP_UNAUTHORIZED);
    // return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  chofer.token = await genToken(64);
  delete chofer.pass;

  await Chofer.query()
    .findById(chofer.id)
    .patch({ token: chofer.token })
    .catch(console.error);

  AR.enviarDatos(chofer, res);
});

// Cambiar pass del chofer.
router.put('/pass_chofer', upload.none(), validar('chofer'), async(req, res) => {
  const id_chofer = req.headers['id_usuario'] || req.body.id_chofer;
  const nuevo = req.body.new_pass;
  const actual = req.body.old_pass;

  let err;

  if (!nuevo || !actual)
    err = 'Ingrese la contraseña';

  else if (nuevo === actual)
    err = 'La contraseña nueva no debe ser igual a la actual';

  else if (!id_chofer)
    err = 'No se especificó la id del chofer';

  else if (nuevo.length < 8)
    err = 'La contraseña debe tener un mínimo de 8 caracteres';

  if (err)
    return AR.enviarError(errr, res, HTTP_BAD_REQUEST);


  const chofer = await Chofer.query().findById(id_chofer).catch(console.error);

  if (!chofer || !(await comparePass(actual, chofer.pass)))
    return res.sendStatus(HTTP_UNAUTHORIZED);

  await Chofer.query()
    .patchAndFetchById(id_chofer, { pass: await hashPass(nuevo) })
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Recuperar pass.
router.post('/pass_recuperar', upload.none(), async(req,res) => {
  const id_usuario = req.headers.id_usuario || req.body.id_usuario;
  const tipo_usuario = req.headers.tipo_usuario;
  const correo = req.body.correo;

  if (!correo)
    return AR.enviarError('Ingrese el correo', res, HTTP_BAD_REQUEST);

  let ModeloUsuario;

  if (tipo_usuario === 'chofer')
    ModeloUsuario = Chofer;
  else if (tipo_usuario === 'admin')
    ModeloUsuario = Admin;
  else
    return AR.enviarError('Usuario no encontrado', res, HTTP_BAD_REQUEST);


  let usuario = await ModeloUsuario.query().findOne({correo}).catch(console.error);

  if (!usuario)
    return AR.enviarError('Correo no encontrado', res, HTTP_BAD_REQUEST);

  const config = {
    to: usuario.correo,
    subject: 'Recuperar contraseña',
    data: {
      nombre: (usuario.nombre||'') +' '+ (usuario.apellido||''),
      pass: await genToken(8),
    },
  };

  await usuario
    .$query()
    .patch({ pass: await hashPass(config.data.pass) })
    .catch(console.error);

  await sendMail('contraseña.html', config)
    .then(r => {
      res.sendStatus(HTTP_OK);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Recuperar pass.
router.post('/pass_recuperar-user', upload.none(), async(req,res) => {
  const id_usuario = req.headers.id_usuario || req.body.id_usuario;
  const tipo_usuario = req.headers.tipo_usuario;
  const correo = req.body.correo;

  if (!correo)
    return AR.enviarError('Ingrese el correo', res, HTTP_BAD_REQUEST);

  let ModeloUsuario;

  if (tipo_usuario === 'user-predio')
    ModeloUsuario = Usuario;
  else
    return AR.enviarError('Usuario no encontrado', res, HTTP_BAD_REQUEST);


  let usuario = await ModeloUsuario.query().findOne({correo}).catch(console.error);

  if (!usuario)
    return AR.enviarError('Correo no encontrado', res, HTTP_BAD_REQUEST);

  const config = {
    to: usuario.correo,
    subject: 'Recuperar contraseña',
    data: {
      nombre: (usuario.nombre||'') +' '+ (usuario.apellido||''),
      pass: await genToken(8),
    },
  };

  await usuario
    .$query()
    .patch({ pass: await hashPass(config.data.pass) })
    .catch(console.error);

  await sendMail('contraseña.html', config)
    .then(r => {
      res.sendStatus(HTTP_OK);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.post('/Api_login', upload.none(), async(req, res) => {
  const correo = req.body.usuario;
  const pass = req.body.password;

  if (!correo || !pass) {
    return res.sendStatus(HTTP_BAD_REQUEST);
  }

  const admin = await Admin.query()
    .findOne('correo', correo)
    .catch(console.error);

  if (!admin) {
    return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  const access = await comparePass(pass, admin.pass);
  if (!access){
    return AR.enviarError('Datos incorrectos', res, HTTP_UNAUTHORIZED);
    // return res.sendStatus(HTTP_UNAUTHORIZED);
  }

  admin.token = await genToken(64);
  delete admin.pass;

  await Admin.query()
    .findById(admin.id)
    .patch({ token: admin.token })
    .catch(console.error);

  AR.enviarDatos({ token: admin.token }, res);
});

module.exports = router;