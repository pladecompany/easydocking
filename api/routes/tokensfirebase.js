const Firebase = require("../models/Firebase");
var FirebaseB = require("./firebase");
FirebaseB = new FirebaseB();

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();
  
  
 
router.post('/agregar-eliminar-firebase', upload.none(), async (req, res) => {
    // console.log(req.body);
    var obj = {
        'id_usuario' : req.body.id_usuario,
        'token' : req.body.tokenFirebase,
        'tablausuario' : req.body.tablausuario,
        'device': req.body.device ? req.body.device : 2 
        };
        await Firebase.query().delete()
        .where({token: String(obj.token)})
        .catch(err => {
            res.send(err);
            return;
        });
        await Firebase.query()
        .insert(obj)
        .then(fire => {
            AR.enviarDatos(fire, res);
            return;
        })
        .catch(async err => {
                        
                        await Firebase.query()
                        .patchAndFetchById(
                            obj.token,{
                                id_usuario: obj.id_usuario
                            })
                        .then(firebase => {
                            firebase.msj = "Se editó correctamente!";
                            AR.enviarDatos(firebase, res);
                            return;
                        })
                        .catch(err => {
                            res.send(err);
                            return;
                        });
        });
}); 

router.post('/pruebas', upload.none(), async (req, res) => {
   var datap = {
        id_usuario: req.body.id_usuario,
        title: 'Pruebas noti',
        body: 'Notificaciones push',
        tablausuario: req.body.tablausuario
    };
    await FirebaseB.enviarPushNotification(datap, function(datan) { console.log(datan); });
    AR.enviarDatos({msj: true}, res);
}); 

module.exports = router;
