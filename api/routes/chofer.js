const Chofer = require('../models/Chofer');
const Vehiculo = require('../models/Vehiculo');
const Predio = require('../models/Predio');
const Asignar_Predio = require('../models/Asignar_Predio');
const Notificacion = require('../models/Notificacion');
const Admin = require('../models/Admin');

const AR = require('../ApiResponser');
const validar = require('./sesion2');
const upload = require('multer')();
const router = require('express').Router();
const hashPass = require('../functions/hashPass');
const sanitizeString = require('../functions/sanitizeString');
//var fs = require("fs-extra");
const crypto = require("crypto");
const config = require("../config");

const sendMail = require('../mail');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

router.get('/', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Chofer.query()
    .select(
      'chofer.*',
      b => b.count().from('asignar_predio').where('aprobacion', '0').whereRaw('asignar_predio.id_chofer=chofer.id').as('predios_por_aprobar')
    );

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  await Query
    .then(resp => AR.enviarDatos(resp, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/:id(\\d+)', validar('admin','chofer'), async(req, res) => {
  await Chofer.query().findById(req.params.id)
    .then(resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/:id(\\d+)/predios_pendientes', validar('admin'), async(req, res) => {
  await Asignar_Predio.query()
    .select('asignar_predio.*', 'predio.nombre')
    .join('predio', 'predio.id', 'asignar_predio.id_predio')
    .where('id_chofer', req.params.id)
    .where('asignar_predio.aprobacion', '0')
    .then(data => { AR.enviarDatos(data, res) })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.post('/', upload.none(), async(req, res) => {
  const data = req.body;
  let errr = false;

  //if (!data.dni) errr = "Selecciona la imagen del DNI";
  if (!data.nombre) errr = "Ingresa el nombre";
  else if (!data.apellido) errr = "Ingresa el apellido";
  else if (!data.correo) errr = "Ingresa el email";
  else if (!data.telefono) errr = "Ingresa el teléfono";
  else if (!data.predios) errr = "Debes seleccionar al menos un predio";
  else if (!data.pass) errr = "Ingresa la contraseña";
  else if (data.pass.length<=4) errr = "La contraseña debe tener mas de 4 caracteres";
  else if (!data.pass_c) errr = "Ingresa el confirmar contraseña";
  else if (data.pass_c.length<=4) errr = "El confirmar contraseña debe tener mas de 4 caracteres";
  else if (data.pass!=data.pass_c) errr = "Las contraseñas no coinciden";
  else if (!data.tipo) errr = "Selecciona el tipo de vehículo";
  else if (!data.patente) errr = "Ingresa la patente";
  else if (data.requerido_patente2 && data.requerido_patente2 == '1' && !data.patente2) errr = "Ingresa la segunda patente";
  else if (!data.vencimiento) errr = "Ingresa el vencimiento del registro";
  else if (!data.vencimiento_vtv && (data.tipo=='Tractor' || data.tipo=='Semi')) errr = "Ingresa el vencimiento del VTV";
  else if (!data.vencimiento_seguro) errr = "Ingresa el vencimiento del seguro";
  //else if (!data.imgv2 && (data.tipo=='Tractor' || data.tipo=='Semi')) errr = "Selecciona la imagen del VTV";
  //else if (!data.imgv1) errr = "Selecciona la imagen del seguro";
  //else if (!data.imgv3) errr = "Selecciona la imagen del registro";
  //else if (!data.imgv4) errr = "Selecciona la imagen de la clausula de no repetición";
  //else if (!data.imgv5) errr = "Selecciona la imagen de la ART";

  if (!errr){
    const verificar_tlf = await Chofer.query().where('id_cod_telefono',data.cod_ped).where('telefono',data.telefono).catch(err => { console.error(err); });
    if(verificar_tlf[0]) errr = "Este numero de teléfono ya esta registrado";
  }

  if (!errr){
    const verificar_email = await Chofer.query().where('correo',data.correo).catch(err => { console.error(err); });
    if(verificar_email[0]) errr = "Este email ya esta registrado";
  }

  if (errr)
    return AR.enviarError(errr, res, HTTP_BAD_REQUEST);


  const datos = {
    nombre: data.nombre,
    apellido: data.apellido,
    correo: data.correo,
    id_cod_telefono: data.cod_ped,
    telefono: data.telefono,
    pass: await hashPass(data.pass),
  };
  if(data.dni){
    var file = "img_dni__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
    await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.dni.split(",")[1], 'base64'), 'base64', function(err) {
      console.log(err);
    });
    datos.dni = config.rutaArchivo(file);
  }


  await Chofer.query()
    .insert(datos)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;

      AR.enviarDatos(resp, res);

      const ADMIN = await Admin.query();
      for (var i = 0; i < ADMIN.length; i++) {
        Notificacion.emitir({
          titulo: 'Nuevo chofer registrado',
          contenido: `El chofer ${resp.nombre.toUpperCase()} ${resp.apellido.toUpperCase()}, esta pendiente por aprobar`,
          id_receptor: ADMIN[i].id,
          usuario_receptor: Notificacion.usuario.ADMIN,
          id_usable: resp.id,
        });
      }

      const datos_veh = {
        tipo: data.tipo,
        patente: data.patente,
        vencimiento: data.vencimiento,
        vencimiento_seguro: data.vencimiento_seguro,
        id_chofer: resp.id
      };
      if(data.patente2)
        datos_veh.patente2 = data.patente2;
      if(data.tipo_vtv==1)
        datos_veh.vencimiento_vtv = data.vencimiento_vtv;


      if(data.imgv1){
        var file = "img_veh1__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.imgv1.split(",")[1], 'base64'), 'base64', function(err) {
          console.log(err);
        });
        datos_veh.img = config.rutaArchivo(file);
      }
      if(data.imgv2 && data.tipo_vtv==1){
        var file = "img_veh2__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.imgv2.split(",")[1], 'base64'), 'base64', function(err) {
          console.log(err);
        });
        datos_veh.img_vtv = config.rutaArchivo(file);
      }
      if(data.imgv3){
        var file = "img_veh3__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.imgv3.split(",")[1], 'base64'), 'base64', function(err) {
          console.log(err);
        });
        datos_veh.img_seguro = config.rutaArchivo(file);
      }
      if(data.imgv4){
        var file = "img_veh4__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.imgv4.split(",")[1], 'base64'), 'base64', function(err) {
          console.log(err);
        });
        datos_veh.img_clausula = config.rutaArchivo(file);
      }
      if(data.imgv5){
        var file = "img_veh5__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.imgv5.split(",")[1], 'base64'), 'base64', function(err) {
          console.log(err);
        });
        datos_veh.img_art = config.rutaArchivo(file);
      }
      await Vehiculo.query().insert(datos_veh).catch(err => { console.error(err); })
      if(data.predios){
        let predios = data.predios.split(',');
        for (var i = 0; i < predios.length; i++) {
          await Asignar_Predio.query().insert({id_chofer:resp.id,id_predio:predios[i]}).catch(err => { console.error(err); })
        }
      }

    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.put('/aprobar', upload.none(), validar('admin'), async(req, res) => {
  const data = req.body;
  const datos = {
    estatus: 1
  };
  await Chofer.query()
    .patchAndFetchById(data.idc, datos)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      await Asignar_Predio.query().delete().where('id_chofer',resp.id).catch(err => {console.error(err);});
      await Vehiculo.query().update({estatus: 1,aprobacion: '1'}).where('id_chofer',resp.id).catch(err => {console.error(err);});
      if(data.predios){
        let predios = data.predios.split(',');
        for (var i = 0; i < predios.length; i++) {
          await Asignar_Predio.query().insert({id_chofer:resp.id,id_predio:predios[i]}).catch(err => { console.error(err); })
        }
      }

      const veh = await Vehiculo.query().findOne('id_chofer', resp.id).catch(console.error);

      const mail = {
        to: resp.correo,
        subject: 'Registro aprobado',
        data: {
          ocultar_nombre: true,
          ocultar_informa: true,
          nombre: resp.nombre,
          apellido: resp.apellido,
          correo: resp.correo,
          patente: veh.patente,
        }
      };

      sendMail('aprobado_chofer.html', mail)
        .then(console.log)
        .catch(console.error);


      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Asignar predio.
router.put('/actualizar-predios', upload.none(), validar('admin'), async(req, res) => {
  const id_predios = (req.body.id_predios && req.body.id_predios.length) ?
    req.body.id_predios : [];

  const id_predio = req.body.id_predio;
  const id_chofer = req.body.id_chofer;

  let err = false;

  if (!id_predios)
    err = 'Predios no especificados';
  else if (!id_chofer)
    err = 'Chofer no especificado';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  const chofer = await Chofer.query().findById(id_chofer).catch(console.error);
  const asignaciones = await Asignar_Predio.query()
    .where({ id_chofer })
    .whereIn('id_predio', id_predios)
    .catch(console.error);

  // Evitar duplicados.
  const id_predios_nuevos = id_predios.filter(
    idp => !asignaciones.some(a => a.id_predio==idp)
  );


  if (!chofer)
    err = 'Chofer no encontrado';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  // Eliminar los que no se seleccionaron.
  await Asignar_Predio.query()
    .where({ id_chofer })
    .whereNotIn('id_predio', id_predios)
    .del()
    .catch(console.error);


  await Asignar_Predio.query()
    .insertGraph(
      id_predios_nuevos.map(id_predio => ({ id_chofer, id_predio }))
    )
    .then(async() => {
      const predios = await Predio.query().whereIn('id', id_predios).catch(console.error);

      let contenido;
      if (predios.length)
        contenido =  'Tus predios asignados son: ' + predios.map(p => p.nombre).join(', ');
      else
        contenido = 'No hay predios asignados';

      Notificacion.emitir({
        titulo: 'Actualización de predios',
        contenido: contenido,
        id_receptor: id_chofer,
        usuario_receptor: Notificacion.usuario.CHOFER,
        push: true,
      });

      res.sendStatus(HTTP_OK);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Listar predios.
router.get('/:id(\\d+)/predios', upload.none(), validar('chofer'), async(req, res) => {
  await Chofer.query()
    .findById(req.params.id)
    .then(data => {
      if (!data) return res.sendStatus(HTTP_NOT_FOUND)
      AR.enviarDatos(data.predios, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Editar perfil
router.put('/:id(\\d+)', upload.none(), validar('chofer'), async(req, res) => {
  const data = req.body;
  let id_predios_solicitados = data.predios_solicitados;
  if (typeof id_predios_solicitados === 'string') {
    id_predios_solicitados = id_predios_solicitados.split(',');
  }

  let errr;

  if (!data.nombre) errr = "Ingresa el nombre";
  else if (!data.apellido) errr = "Ingresa el apellido";
  else if (!data.correo) errr = "Ingresa el email";
  else if (!data.telefono) errr = "Ingresa el teléfono";
  else if (!data.id_cod_telefono) errr = "Seleccionar código de área";

  if (errr)
    return AR.enviarError(errr, res, HTTP_BAD_REQUEST);

  const datos = {
    nombre: data.nombre,
    apellido: data.apellido,
    correo: data.correo,
    telefono: data.telefono,
    id_cod_telefono: data.id_cod_telefono,
  };
  if(data.dni){
    var file = "img_dni__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
    await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.dni.split(",")[1], 'base64'), 'base64', function(err) {
      console.log(err);
    });
    datos.dni = config.rutaArchivo(file);
  }
  await Chofer.query()
    .patchAndFetchById(req.params.id, datos)
    .then(async chofer => {

      if (id_predios_solicitados && id_predios_solicitados.length)
      {
        const asignados = await Asignar_Predio.query()
          .where('id_chofer', chofer.id)
          .whereIn('id_predio', id_predios_solicitados)
          .where('aprobacion', '1')

        for (let idp of id_predios_solicitados) {
          const idx = asignados.find(a => a.id_predio == idp);

          if (idx >= 0) {
            await Asignar_Predio.query()
              .patchAndFetchById(asignados[idx].id, { aprobacion: '0' })
              .catch(console.error);
          }
          else {
            await Asignar_Predio.query()
              .insert({ id_predio: idp, id_chofer: chofer.id, aprobacion: '0' })
              .catch(console.error);
          }
        }

        // Notificar asignaciones.
        const nombre_predios = await Asignar_Predio.query()
          .select('predio.nombre')
          .where('asignar_predio.id_chofer', chofer.id)
          .where('aprobacion', '0')
          .join('predio', 'predio.id', 'asignar_predio.id_predio')

        const nombres = nombre_predios.map(p => p.nombre).join(', ')
        Notificacion.emitir({
          usuario_receptor: Notificacion.usuario.ADMIN,
          id_receptor: null,
          usuario_emisor: Notificacion.usuario.CHOFER,
          id_emisor: chofer.id,
          id_usable: chofer.id,
          titulo: 'Solicitud de predios',
          contenido: `El chofer ${chofer.nombre.toUpperCase()} ${chofer.apellido.toUpperCase()} ha solicitado los predios: ${nombres}`,
        });
      }

      AR.enviarDatos(chofer, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.post("/cambiar-ubicacion-backgeo/:iden/:token", validar('chofer'), async (req, res) => {
  let errr = false;
  let data = req.body;
  console.log(data);

  // inicio validaciones
  if (data) {
    if (data[0]) {
      data = data[0];
    } else {
      errr = "content empty";
    }
  } else {
    errr = "content empty";
  }

  if (!data.latitude || !data.longitude) errr = "Ingresa las cordenadas";
  if (!req.params.iden) errr = "Ingresa la id del chofer";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    lat_cho: data.latitude,
    lon_cho: data.longitude
  }
  // console.log(datos);
  await Chofer.query().updateAndFetchById(req.params.iden, datos)
  .then(async resp => {
    resp.msj = "Cordenadas actualizada con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar cordenadas';
    res.send(err);
  });
});

//editar cordenadas
router.put("/cambiar_ubicacion", upload.none(), validar('chofer'), async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.lat || !data.lon) errr = "Ingresa las cordenadas";
  if (!data.iden) errr = "Ingresa la id del chofer";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    lat_cho: data.lat,
    lon_cho: data.lon
  }

  await Chofer.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Cordenadas actualizada con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar cordenadas';
    res.send(err);
  });
});

router.put("/cambiar_sts_ubicacion", upload.none(), validar('chofer'), async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.iden) errr = "Ingresa la id del chofer";

  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }
  var sts = 0;
  if(data.sts)
    sts = data.sts;
  var datos = {
    estatus_coordenada: sts
  }

  await Chofer.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    res.send(err);
  });
});

router.put('/:id(\\d+)/aprobacion', upload.none(), validar('admin'), async(req, res) => {

  const datos = {
    estatus: req.body.aprobacion ? 1 : 0,
  };

  // Cerrar sesion.
  if (datos.estatus == 0) {
    datos.token = null;
  }

  await Chofer.query()
    .patchAndFetchById(req.params.id, datos)
    .then(r => {
      if (!r) {
        res.sendStatus(HTTP_NOT_FOUND);
      }
      else {
        res.sendStatus(HTTP_OK);
      }
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.put('/:id(\\d+)/aprobacion_predios', upload.none(), validar('admin'), async(req, res) => {
  const id_chofer = req.params.id;
  const predios = req.body.predios;

  const chofer = await Chofer.query().findById(id_chofer).catch(console.error);

  const aprobados = predios.filter(p => p.aprobacion == 1);
  const rechazados = predios.filter(p => p.aprobacion == -1);
  if (aprobados.length) {
    await Asignar_Predio.query()
      .whereIn('id', aprobados.map(a=>a.id))
      .patch({ aprobacion: '1' })

    const mail = {
      to: chofer.correo,
      subject: 'Aprobacion de predios',
      data: {
        ocultar_nombre: true,
        ocultar_informa: true,
        nombre: chofer.nombre,
        apellido: chofer.apellido,
        predios_aprobados: aprobados.map(a => a.nombre),
      }
    };

    sendMail('predios_aprobados.html', mail)
      .then(console.log)
      .catch(console.error);

    Notificacion.emitir({
      id_receptor: chofer.id,
      usuario_receptor: Notificacion.usuario.CHOFER,
      titulo: 'Predios aprobados',
      contenido: 'Se han aprobado los siguientes predios: ' + aprobados.map(p => p.nombre).join(', '),
      push: true,
    });

  }
  await Asignar_Predio.query()
    .whereIn('id', rechazados.map(a=>a.id))
    .del()
    .catch(console.error);
    // .patch({ aprobacion: '-1' })

  for (let pre of rechazados) {
    const mail = {
      to: chofer.correo,
      subject: 'Predio rechazado',
      data: {
        ocultar_nombre: true,
        ocultar_informa: true,
        nombre: chofer.nombre,
        apellido: chofer.apellido,
        predio: pre.nombre,
        motivo: pre.motivo,
      }
    };

    sendMail('predio_rechazado.html', mail)
      .then(console.log)
      .catch(console.error);


    Notificacion.emitir({
      id_receptor: chofer.id,
      usuario_receptor: Notificacion.usuario.CHOFER,
      titulo: 'Predios rechazados',
      contenido: 'Se ha rechazado el predio: ' + pre.nombre,
      push: true,
    });
  }


  AR.enviarDatos({a: aprobados, r: rechazados}, res);
});


module.exports = router;
