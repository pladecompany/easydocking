const Predio = require('../models/Predio');
const Chofer = require('../models/Chofer');
const Turno = require('../models/Turno');
const TurnoApi = require('../models/TurnoApi');
const Detalles_Predio = require('../models/Detalles_Predio');
const Detalles_Turnos = require('../models/Detalles_Turnos');
const Asignar_Predio = require('../models/Asignar_Predio');
const Usuario = require('../models/Usuario');
const Horarios = require("../models/Horarios");
const UsuarioPredio = require("../models/UsuarioPredio");
const Bitacora = require("../models/Bitacora");
const Emails = require("../models/Emails");
const Vehiculo = require('../models/Vehiculo');

const AR = require('../ApiResponser');
const validar = require('./sesion2');
const upload = require('multer')();
const router = require('express').Router();
const {raw} = require('objection');
const axios = require('axios')
const crypto = require("crypto");
const config_ar = require('../config');
const moment = require("moment");
const moment2 = require("moment-timezone");
const { takeRightWhile } = require('lodash');
const sanitizeString = require('../functions/sanitizeString');
const checkEmail = require('../functions/checkEmail');
const hashPass = require('../functions/hashPass');
const reemplazarVariables = require('../functions/reemplazarVariables');
const sendMail = require('../mail');

const ExcelJS = require('exceljs');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500

async function obtener_token_apieasy(){
  await axios.post(config_ar.API_EASY_TOKEN, config_ar.API_EASY_DATOS)
    .then(resp => {
      global.TOKENAPIEASY = resp.data.Token;
      global.FECHA_TOKENAPIEASY = resp.data['Fecha y Hora Vencimiento del token:'];
      //console.log(resp.data)
    })
    .catch(error => {
      console.log(error)
    })
}

async function consultar_apieasy(url,datos){
  //PARA VERIFICAR TOKEN
  if(global.TOKENAPIEASY){//token existe
    var fecha = moment(global.FECHA_TOKENAPIEASY,["DD/MM/YYYY  HH:mm:ss", "YYYY-MM-DD HH:mm:ss"])
    var fecha_act = moment()
    var diferencia =fecha.diff(fecha_act, 'minutes');
    if(diferencia<0)
      await obtener_token_apieasy();
  }else{ //token no existe
    await obtener_token_apieasy();
  }
  var config_header = {
    headers: {
      'Authorization': 'Bearer '+global.TOKENAPIEASY
    }
  };
  const resp = await axios.post(url, datos, config_header).catch(error => { console.log(error);})
  if(resp)
    return resp.data;
  else
    return false;
}

/*router.post('/solicitar-api-easy', async(req, res) => {
  const params = req.body;
  let resp = await consultar_apieasy(params.url, params.datos);
  console.log(resp);
  AR.enviarDatos(resp, res)
});*/

// Listar
router.get('/', async(req, res) => {
  const params = req.query;

  const Query = Predio.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('nombre', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('telefono', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('encargado', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    Query.orHaving('telefono_enc', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }

  // No asignado a chofer.
  if (+params.naac) {
    const asignados = Asignar_Predio.query().select('id_predio AS id').where('id_chofer', +params.naac);
    Query.whereNotIn('predio.id', asignados);
  }

  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/bitacora-list/:id', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Bitacora.query().where({predio: req.params.id});

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined)

  if (params.filter && params.filter.length > 1){
    Query.having('accion', 'LIKE', '%'+sanitizeString(params.filter)+'%');
  }
  Query.orderBy("fecha", "desc");
  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/info-predio/:id', /*validar('admin'),*/ async(req, res) => {
  const params = req.query;

  const Query = Predio.query();
  if(req.params.id)
    Query.where('id',req.params.id)
  await Query
    .then(datos => AR.enviarDatos(datos[0], res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/segmenta-predio/:id', validar('admin','chofer'), async(req, res) => {
  const data = req.query;
  var lista_res = JSON.parse(data.respuesta);
  const PRE = await Predio.query().findById(req.params.id).catch(err => {console.error(err);});
  const Query = Detalles_Predio.query().where('id_predio',req.params.id).where('segmenta',1)
  await Query
    .then(async datos => {
      for (var i = 0; i < datos.length; i++) {
        if(PRE.zona_horaria)
          datos[i].fecha_actual = moment2().tz(PRE.zona_horaria).format("YYYY-MM-DD HH:mm:ss");
        else
          datos[i].fecha_actual = moment().format("YYYY-MM-DD HH:mm:ss");
        var respuesta = lista_res.filter(p => p.variable == datos[i].variable);
        if(respuesta[0]){
          const Horario = await Horarios.query()
            .select('horarios.*','predio.horas_maxima')
            .join('predio','predio.id','horarios.predio')
            .where('horarios.predio',req.params.id)
            .where('horarios.fecha',data.fecha)
            .where('horarios.tipo',respuesta[0].variable)
            //.where('tipo',respuesta[0].variable.trim())
            .whereRaw('find_in_set("'+respuesta[0].respuesta+'",horarios.tipo_operacion) <> 0')
            if(Horario[0])
              datos[i].horario = Horario;

          /*
          datos_consultar = {
            fecha: data.fecha,
            variables: [{
              "name": datos[i].variable,
              "value": respuesta[0].respuesta
            }]
          }let resp = await consultar_apieasy(PRE.url+'api/v1/ED_ObtenerTurnosDia', datos_consultar);
          if(resp.code==0){
            await obtener_token_apieasy();
            let resp2 = await consultar_apieasy(PRE.url+'api/v1/ED_ObtenerTurnosDia', datos_consultar);
            datos[i].horario = resp2;
          }else
            datos[i].horario = resp;*/
        }
      }
      AR.enviarDatos(datos, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/horarios-sin-segmenta-predio/:id', validar('admin','chofer'), async(req, res) => {
  const data = req.query;
  const Query = Horarios.query()
      .where('predio',req.params.id)
      //.where('fecha','>=',data.fecha)
      .where('tipo','todos')
      .where('tipo_operacion','todos')
      .select(raw('count(*)').as('total'));
  await Query
    .then(async datos => {
      AR.enviarDatos(datos[0], res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/sin-segmenta-predio/:id', validar('admin','chofer'), async(req, res) => {
  const data = req.query;
  const Query = Horarios.query()
      .select('horarios.*','predio.horas_maxima','predio.zona_horaria')
      .join('predio','predio.id','horarios.predio')
      .where('horarios.predio',req.params.id)
      .where('horarios.fecha',data.fecha)
      .where('horarios.tipo','todos')
      .where('horarios.tipo_operacion','todos')
  await Query
    .then(async datos => {
      for (var i = 0; i < datos.length; i++) {
        if(datos[i].zona_horaria)
          datos[i].fecha_actual = moment2().tz(datos[i].zona_horaria).format("YYYY-MM-DD HH:mm:ss");
        else
          datos[i].fecha_actual = moment().format("YYYY-MM-DD HH:mm:ss");
      }
      AR.enviarDatos(datos, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/horarios-turno/:id', validar('admin','chofer'), async(req, res) => {
  const data = req.query;
  f_tur_ini = moment(data.fecha+' '+data.hora).format('YYYY-MM-DD HH:mm:ss');
  f_tur_fin = moment(data.fecha+' '+data.hora).add(data.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
  //console.log(data);
  let sql_ext = '';
  /*if(data.horarios){
    let hor = JSON.parse(data.horarios);
    for (var i = 0; i < hor.length; i++) {
      var h = hor[i];
      if(sql_ext=='')
        sql_ext = 'id_horario='+h.id;
      else
        sql_ext += ' OR id_horario='+h.id;
    };
    if(sql_ext!='')
      sql_ext = '('+sql_ext+')';
  }*/
  const Query = Turno.query()
    .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
    .whereRaw('(estatus="0" OR estatus="1")')
    .where('id_predio',req.params.id)
    .where('id_tanda',data.id_horario)
    //.where('id_horario',data.id_horario)
    .havingRaw('fecha_m = "'+f_tur_ini+'"')
    //.havingRaw('((fecha_m <= "'+f_tur_ini+'" AND fecha_fin_m >= "'+f_tur_ini+'") OR (fecha_m <= "'+f_tur_fin+'" AND fecha_fin_m >= "'+f_tur_fin+'") OR (fecha_m >= "'+f_tur_ini+'" AND fecha_m <= "'+f_tur_fin+'") OR (fecha_fin_m >= "'+f_tur_ini+'" AND fecha_fin_m <= "'+f_tur_fin+'"))')
    .groupBy('id');
  if(sql_ext!='')
    Query.whereRaw(sql_ext)
  if(data.id_turno)
    Query.where('id','<>',data.id_turno);
  await Query
    .then(async datos => {
      AR.enviarDatos({turnos:datos.length}, res)
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/detalles/:id(\\d+)', validar('admin','chofer'), async(req, res) => {
  const Query = Detalles_Predio.query().where("detalles_predio.id_predio",req.params.id)
      .select('detalles_predio.*','predio.dias_maximo', 'predio.zona_horaria')
      .join('predio','predio.id','detalles_predio.id_predio');

  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

// Registrar
router.post('/', upload.none(), validar('admin'), async(req, res) => {
  const body = req.body;
  if (!body.horas)
  	body.horas=0;
  const datos = {
    nombre: body.nombre,
    telefono: body.telefono,
    url: body.url,
    url_alternativa: body.url_alt,
    encargado: body.encargado,
    telefono_enc: body.telefono_enc,
    id_cod_telefono: body.id_pais,
    observacion: body.obs,
    lat:body.lat,
    lon:body.lon,
    estatus:body.estatus.toString(),
    usar_correo: body.usar_correo,
    horas_maxima: body.horas,
    dias_maximo: body.dias,
    usar_correo: body.usar_correo ? 1 : 0,
    zona_horaria: body.zona_horaria,
    //usuarios: body.usuarios,
  };
  const usuarios_editados = body.usuarios_editados || [];
  const usuarios_nuevos = body.usuarios_nuevos || [];

  if(body.seguimiento)
    datos.seguimiento = body.seguimiento;
  //console.log(body,datos);


  // Validar datos
  let err = false;

  if (!datos.nombre)
    err = 'Ingrese el nombre de la empresa';
  else if (!datos.telefono)
    err = 'Ingrese el teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El teléfono debe tener de 9 a 12 caracteres';
  else if (!datos.url)
    err = 'Ingrese la url';
  else if (!datos.encargado)
    err = 'Ingrese el encargado de la empresa';
  else if (!datos.telefono_enc)
    err = 'Ingrese el teléfono del encargado';
  else if (datos.telefono_enc.length<9 || datos.telefono_enc.length>12)
    err = 'El teléfono del encargado debe tener de 9 a 12 caracteres';
  else if (!datos.id_cod_telefono)
    err = 'Selecciona el pais';
  else if (!datos.estatus)
    err = 'Selecciona el estatus para el turno';
  //else if (!body.horas)
  //  err = 'Ingresa las horas previas para el turno';
  else if (!datos.lat || !datos.lon)
    err = 'Selecciona una ubicación en el mapa';
  else if (!body.detalles)
    err = 'Ingresa al menos un detalle';
  else if (!body.zona_horaria)
    err = 'Selecciona la zona horaria';

  if(!err){
    const Pre = await Predio.query().where('url',datos.url).catch(err => {console.error(err);});
    if(Pre[0])
      err = 'Ésta URL existe';
  }

  if(!err){
    for (var i = 0; i < body.detalles.length; i++) {
      let datos_det = body.detalles[i];
      if(!datos_det.tipo || !datos_det.titulo)
        err = 'Debes completar todos los campos para el turno';
      else if(datos_det.tipo=='Select' && !datos_det.opciones)
        err = 'Debes completar todos los campos para el turno';
      else if(datos_det.tipo=='Texto' && (!datos_det.min || !datos_det.max))
        err = 'Debes completar todos los campos para el turno';
      else if(datos_det.tipo=='Texto' && parseFloat(datos_det.min)>parseFloat(datos_det.max))
        err = 'El campo caracter minimo no debe ser mayor a los caracteres máximos';
    }

    // for (let i = 0; i < datos.usuarios.length; i++) {
    //   let usu = datos.usuarios[i];
    //   if (!usu.nombre)
    //     err = 'Debes completar todos los campos de usuario';
    //   else if (!usu.correo || !checkEmail(usu.correo))
    //     err = 'Debes completar todos los campos de usuario';
    //   else if (!usu.pass || usu.pass.length < 8)
    //     err = 'Las contraseñas deben tener mínimo 8 caracteres';
    // }

    for (let i = 0; i < usuarios_nuevos.length; i++) {
      let usu = usuarios_nuevos[i];
      if (!usu.nombre)
        err = 'Debes completar todos los campos de usuario';
      else if (!usu.correo)
        err = 'Debes completar todos los campos de usuario';
      else if (!checkEmail(usu.correo))
        err = 'Verifica los correos de los usuarios';
      else if (usu.id && usu.pass && usu.pass.length < 8)
        err = 'Las contraseñas deben tener mínimo 8 caracteres';
      else if (!usu.id && !usu.pass)
        err = 'Debes completar todos los campos de usuario';

      if (!usu.pass)
        delete usu.pass;
    }

    for (let i = 0; i < usuarios_editados.length; i++) {
      let usu = usuarios_editados[i];
      if (!usu.nombre)
        err = 'Debes completar todos los campos de usuario';
      else if (!usu.correo)
        err = 'Debes completar todos los campos de usuario';
      else if (!checkEmail(usu.correo))
        err = 'Verifica los correos de los usuarios';
      else if (usu.id && usu.pass && usu.pass.length < 8)
        err = 'Las contraseñas deben tener mínimo 8 caracteres';
      else if (!usu.id && !usu.pass)
        err = 'Debes completar todos los campos de usuario';

      if (!usu.pass)
        delete usu.pass;
    }

  }

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  // for (var i = 0; i < datos.usuarios.length; i++) {
  //   datos.usuarios[i].pass = await hashPass(datos.usuarios[i].pass);
  // }


  await Predio.query()
    .insert(datos)
    .then(async resp => {
      if(body.detalles){
        for (var i = 0; i < body.detalles.length; i++) {
          let datos_det = body.detalles[i];
          const datos_d = {
            tipo:datos_det.tipo,
            titulo:datos_det.titulo,
            opciones:datos_det.opciones,
            segmenta:datos_det.segmenta,
            variable:datos_det.variable,
            obligatorio:datos_det.obligatorio,
            id_predio: resp.id
          }
          if(datos_det.min)
            datos_d.min = datos_det.min;
          if(datos_det.max)
            datos_d.max = datos_det.max;
          await Detalles_Predio.query().insert(datos_d).catch(err => {console.error(err);});
        }
      }


      // Eliminar usuarios removidos.
      await Usuario.query()
        .where('id_predio',resp.id)
        // .whereNotIn('id', usuarios.map(u=>u.id).filter(Boolean))
        .whereNotIn('id', usuarios_editados.map(u=>u.id).filter(Boolean))
        .delete()
        .catch(console.error);

      // Limpiar usuarios asignados para evitar duplicados.
      await UsuarioPredio.query().del().where('id_predio', resp.id).catch(console.error);
      const usuarios_asignados = [];

      // Editar usuarios existentes.
      for (usu of usuarios_editados.filter(u => u.id)) {
        const data = {
          nombre: usu.nombre,
          correo: usu.correo,
        };

        if (usu.pass)
          data.pass = await hashPass(usu.pass);

        await Usuario.query().findById(usu.id).patch(data).catch(console.error);

        if (usu.asignar) {
          // await Predio.relatedQuery('usuario').for(resp.id).relate(usu.id);
          usuarios_asignados.push(usu);
        }
      }


      // Crear usuarios nuevos.
      for (usu of usuarios_nuevos.filter(u => !u.id)) {
        const data = {
          id_predio: resp.id,
          nombre: usu.nombre,
          correo: usu.correo,
          pass: await hashPass(usu.pass),
        };

        const nuevo = await Usuario.query().insert(data).catch(console.error);

        if (usu.asignar) {
          usuarios_asignados.push(nuevo);
        }
      }

      // Reasignar los usuarios.
      await UsuarioPredio.query()
        .insertGraph(usuarios_asignados.map(u => ({ id_usuario:u.id, id_predio:resp.id })))
        .catch(console.error);


      resp.r = true;
      resp.msg = "Se registro correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Editar
router.put('/editar', upload.none(), validar('admin'), async(req, res) => {
  const body = req.body;
  if (!body.horas)
  	body.horas=0;

  const datos = {
    nombre: body.nombre,
    telefono: body.telefono,
    url: body.url,
    url_alternativa: body.url_alt,
    encargado: body.encargado,
    telefono_enc: body.telefono_enc,
    id_cod_telefono: body.id_pais,
    observacion: body.obs,
    lat:body.lat,
    lon:body.lon,
    seguimiento: body.seguimiento,
    estatus:body.estatus.toString(),
    usar_correo: body.usar_correo,
    horas_maxima: body.horas,
    dias_maximo: body.dias,
    usar_correo: body.usar_correo,
    zona_horaria: body.zona_horaria,
  };
  // const usuarios = body.usuarios;
  const usuarios_editados = body.usuarios_editados || [];
  const usuarios_nuevos = body.usuarios_nuevos || [];

  //console.log(body);
  // Validar datos
  let err = false;

  if (!datos.nombre)
    err = 'Ingrese el nombre de la empresa';
  else if (!datos.telefono)
    err = 'Ingrese el teléfono';
  else if (datos.telefono.length<9 || datos.telefono.length>12)
    err = 'El teléfono debe tener de 9 a 12 caracteres';
  else if (!datos.url)
    err = 'Ingrese la url';
  else if (!datos.encargado)
    err = 'Ingrese el encargado de la empresa';
  else if (!datos.telefono_enc)
    err = 'Ingrese el teléfono del encargado';
  else if (datos.telefono_enc.length<9 || datos.telefono_enc.length>12)
    err = 'El teléfono del encargado debe tener de 9 a 12 caracteres';
  else if (!datos.id_cod_telefono)
    err = 'Selecciona el pais';
  else if (!datos.estatus)
    err = 'Selecciona el estatus para el turno';
  //else if (!body.horas)
  //  err = 'Ingresa las horas previas para el turno';
  else if (!datos.lat || !datos.lon)
    err = 'Selecciona una ubicación en el mapa';
  else if (!body.detalles)
    err = 'Ingresa al menos un detalle';
  else if (!body.id_v)
    err = 'Ingresa la ID';
  else if (!body.zona_horaria)
    err = 'Selecciona la zona horaria';


  if(!err){
    const Pre = await Predio.query().where('url',datos.url).where('id','<>',body.id_v).catch(err => {console.error(err);});
    if(Pre[0])
      err = 'Ésta URL existe';
  }

  if(!err){
    for (var i = 0; i < body.detalles.length; i++) {
      let datos_det = body.detalles[i];
      if(!datos_det.tipo || !datos_det.titulo)
        err = 'Debes completar todos los campos para el turno';
      else if(datos_det.tipo=='Select' && !datos_det.opciones)
        err = 'Debes completar todos los campos para el turno';
      else if(datos_det.tipo=='Texto' && (!datos_det.min || !datos_det.max))
        err = 'Debes completar todos los campos para el turno';
      else if(datos_det.tipo=='Texto' && parseFloat(datos_det.min)>parseFloat(datos_det.max))
        err = 'El campo caracter minimo no debe ser mayor a los caracteres máximos';
    }

    // for (let i = 0; i < usuarios.length; i++) {
    //   let usu = usuarios[i];
    //   if (!usu.nombre)
    //     err = 'Debes completar todos los campos de usuario';
    //   else if (!usu.correo)
    //     err = 'Debes completar todos los campos de usuario';
    //   else if (!checkEmail(usu.correo))
    //     err = 'Verifica los correos de los usuarios';
    //   else if (usu.id && usu.pass && usu.pass.length < 8)
    //     err = 'Las contraseñas deben tener mínimo 8 caracteres';
    //   else if (!usu.id && !usu.pass)
    //     err = 'Debes completar todos los campos de usuario';

    //   if (!usu.pass)
    //     delete usu.pass;
    // }

    for (let i = 0; i < usuarios_nuevos.length; i++) {
      let usu = usuarios_nuevos[i];
      if (!usu.nombre)
        err = 'Debes completar todos los campos de usuario';
      else if (!usu.correo)
        err = 'Debes completar todos los campos de usuario';
      else if (!checkEmail(usu.correo))
        err = 'Verifica los correos de los usuarios';
      else if (usu.id && usu.pass && usu.pass.length < 8)
        err = 'Las contraseñas deben tener mínimo 8 caracteres';
      else if (!usu.id && !usu.pass)
        err = 'Debes completar todos los campos de usuario';

      if (!usu.pass)
        delete usu.pass;
    }

    for (let i = 0; i < usuarios_editados.length; i++) {
      let usu = usuarios_editados[i];
      if (!usu.nombre)
        err = 'Debes completar todos los campos de usuario';
      else if (!usu.correo)
        err = 'Debes completar todos los campos de usuario';
      else if (!checkEmail(usu.correo))
        err = 'Verifica los correos de los usuarios';
      else if (usu.id && usu.pass && usu.pass.length < 8)
        err = 'Las contraseñas deben tener mínimo 8 caracteres';
      else if (!usu.id && !usu.pass)
        err = 'Debes completar todos los campos de usuario';

      if (!usu.pass)
        delete usu.pass;
    }
  }
  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);




  await Predio.query()
    .patchAndFetchById(body.id_v, datos)
    .then(async resp => {
      await Detalles_Predio.query().delete().where('id_predio',body.id_v).catch(err => {console.error(err);});
      if(body.detalles){
        for (var i = 0; i < body.detalles.length; i++) {
          let datos_det = body.detalles[i];
          const datos_d = {
            tipo:datos_det.tipo,
            titulo:datos_det.titulo,
            opciones:datos_det.opciones,
            segmenta:datos_det.segmenta,
            variable:datos_det.variable,
            obligatorio:datos_det.obligatorio,
            id_predio: resp.id
          }
          if(datos_det.min)
            datos_d.min = datos_det.min;
          if(datos_det.max)
            datos_d.max = datos_det.max;
          await Detalles_Predio.query().insert(datos_d).catch(err => {console.error(err);});
        }
      }

      // Eliminar usuarios removidos.
      await Usuario.query()
        .where('id_predio',resp.id)
        // .whereNotIn('id', usuarios.map(u=>u.id).filter(Boolean))
        .whereNotIn('id', usuarios_editados.map(u=>u.id).filter(Boolean))
        .delete()
        .catch(console.error);

      // Limpiar usuarios asignados para evitar duplicados.
      await UsuarioPredio.query().del().where('id_predio', resp.id).catch(console.error);
      const usuarios_asignados = [];

      // Editar usuarios existentes.
      for (usu of usuarios_editados.filter(u => u.id)) {
        const data = {
          nombre: usu.nombre,
          correo: usu.correo,
        };

        if (usu.pass)
          data.pass = await hashPass(usu.pass);

        await Usuario.query().findById(usu.id).patch(data).catch(console.error);

        if (usu.asignar) {
          // await Predio.relatedQuery('usuario').for(resp.id).relate(usu.id);
          usuarios_asignados.push(usu);
        }
      }


      // Crear usuarios nuevos.
      for (usu of usuarios_nuevos.filter(u => !u.id)) {
        const data = {
          id_predio: resp.id,
          nombre: usu.nombre,
          correo: usu.correo,
          pass: await hashPass(usu.pass),
        };

        const nuevo = await Usuario.query().insert(data).catch(console.error);

        if (usu.asignar) {
          usuarios_asignados.push(nuevo);
        }
      }

      // Reasignar los usuarios.
      await UsuarioPredio.query()
        .insertGraph(usuarios_asignados.map(u => ({ id_usuario:u.id, id_predio:resp.id })))
        .catch(console.error);

      resp.r = true;
      resp.msg = "Se editó correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.delete('/:id(\\d+)', validar('admin'), async(req, res) => {
  await Predio.query()
    .findById(req.params.id)
    .delete()
    .then(() => {
      resp = {};
      resp.r=true;
      resp.msg="Eliminado correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/choferes-seguimiento/:id(\\d+)', validar('admin'), async(req, res) => {
  const estatus_listar = [
    '1' // aprobado
  ];
  let fecha_hoy =  moment().format('YYYY-MM-DD');
  const Query = Turno.query()
  //.havingIn('turno.estatus', estatus_listar)
  .join('chofer','chofer.id','turno.id_chofer')
  .where("turno.id_predio",req.params.id)
  .where("turno.fecha",'>=',fecha_hoy+' 00:00:00')
  //.where("turno.fecha_fin",'<=',fecha_hoy+' 23:59:59')
  .where("turno.estatus",'<>','-1')
  .where("turno.estatus",'<>','-2')
  .select('turno.*','chofer.nombre','chofer.apellido','chofer.lat_cho','chofer.lon_cho')
  //.groupBy('turno.id_chofer');

  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/usuarios', validar('admin'), async(req, res) => {
  await Usuario.query()
    .withGraphFetched('predios')
    .then(data => { AR.enviarDatos(data, res) })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.delete('/usuario/:id(\\d+)', validar('admin'), async(req, res) => {
  await Usuario.query()
    .findById(req.params.id)
    .del()
    .then(() => { res.sendStatus(HTTP_OK) })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});


/////////////// FUNCIONES TURNOS ///////////////////
// Registrar
router.post('/turno', upload.none(),  validar('admin','chofer'), async(req, res) => {
  const body = req.body;
  //console.log(body);
  const datos = {
    id_chofer: body.chofer,
    nombre_chofer: body.nombre,
    //id_vehiculo: body.vehiculo,
    id_predio: body.predio,
    fecha: body.fecha+" "+body.hora,
    fecha_fin: body.fecha+" "+body.hora,
    estatus: body.estatus_predio,
    id_tanda: body.id_horario,
    fecha_actualizado: moment().format('YYYY-MM-DD HH:mm:ss')
  };
  if(body.vehiculo)
  	datos.id_vehiculo = body.vehiculo;

  // Validar datos
  let err = false;

  if (!datos.id_chofer)
    err = 'Ingrese el chofer';
  else if (!datos.id_predio)
    err = 'Selecciona el predio';
  else if (!datos.nombre_chofer)
    err = 'Ingresa el nombre del chofer';
  if(!err){
    for (var i = 0; i < body.turnos.length; i++) {
      let datos_det = body.turnos[i];
      if(!datos_det.respuesta && datos_det.obligatorio){
        err = 'Debes completar el campo '+datos_det.titulo;
        i = body.turnos.length;
      }
      else if(datos_det.tipo=='Texto' && datos_det.obligatorio && datos_det.respuesta.length<datos_det.min){
        err = 'El campo "'+datos_det.titulo+'" debe tener más de '+datos_det.min+' caracteres';
        i = body.turnos.length;
      }
      else if(datos_det.tipo=='Texto' && datos_det.respuesta && datos_det.respuesta.length>0 && datos_det.respuesta.length<datos_det.min){
        err = 'El campo "'+datos_det.titulo+'" debe tener más de '+datos_det.min+' caracteres';
        i = body.turnos.length;
      }

    }
  }

  //if (!datos.id_vehiculo && !err)
  //  err = 'Selecciona el vehículo';
  if (!body.fecha && !err)
    err = 'Ingrese la fecha';
  else if (!body.hora && !err)
    err = 'Ingrese la hora';


  var datos_consultar = '';
  if(!err){
    for (var i = 0; i < body.turnos.length; i++) {
      let datos_det = body.turnos[i];
      if(datos_det.segmenta){
        /*datos_consultar = {
          fecha: body.fecha,
          variables: [{
            "name":datos_det.variable,
            "value":datos_det.respuesta
          }]
        }
        let resp = await consultar_apieasy(body.url_predio+'api/v1/ED_ObtenerTurnosDia', datos_consultar);*/
        //console.log(resp);

          const Horario = await Horarios.query()
            .where('id_tanda',datos.id_tanda)
            .where('predio',body.predio)
            .where('fecha',body.fecha)
            .where('tipo',datos_det.variable)
            .whereRaw('find_in_set("'+datos_det.respuesta+'",tipo_operacion) <> 0')
            //.where('tipo_vehiculo.nombre',datos_det.respuesta)
            //.whereRaw('(horarios.tipo_operacion = "'+datos.operacion+'" || horarios.tipo_operacion = "CARGA,DESCARGA")')
          //console.log(body,Horario)
          if(Horario[0]){
            if(Horario.length>0){
              if(err){ // comprobar error cuando hay mas de una segmenta por consultar y si lo termina el for para mandar el error
                i = body.turnos.length;
              }else{
                for (var t = 0; t < Horario.length; t++) {
                  let turnos = Horario[t];
                  f_tur = moment(body.hora+':00', 'hh:mm:ss');
                  f_ini = moment(turnos.hora_a, 'hh:mm:ss');
                  f_fin = moment(turnos.hora_b, 'hh:mm:ss');
                  var fec_tur = moment(f_tur).isBetween(f_ini, f_fin, undefined, '[]');
                  //console.log(fec_tur,f_tur,f_ini, f_fin);
                  if(fec_tur){
                    t = Horario.length;
                    f_tur_ini = moment(body.fecha+' '+body.hora).format('YYYY-MM-DD HH:mm:ss');
                    f_tur_fin = moment(body.fecha+' '+body.hora).add(turnos.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
                    datos.fecha_fin = f_tur_fin;
                    const turnos_rango = await Turno.query()
                    .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
                    .whereRaw('(estatus="0" OR estatus="1")')
                    .where('id_predio',datos.id_predio)
                    //.where('id_horario',datos.id_horario)
                    .where('id_tanda',datos.id_tanda)
                    .havingRaw('fecha_m = "'+f_tur_ini+'"')
                    //.havingRaw('((fecha_m <= "'+f_tur_ini+'" AND fecha_fin_m >= "'+f_tur_ini+'") OR (fecha_m <= "'+f_tur_fin+'" AND fecha_fin_m >= "'+f_tur_fin+'") OR (fecha_m >= "'+f_tur_ini+'" AND fecha_m <= "'+f_tur_fin+'") OR (fecha_fin_m >= "'+f_tur_ini+'" AND fecha_fin_m <= "'+f_tur_fin+'"))')
                    .groupBy('id');

                    if(turnos_rango.length>=turnos.disponibilidad)
                      err = 'No hay disponibilidad para la hora seleccionada, intente con otra hora';
                    else
                      err = false;
                    //console.log(turnos_rango,f_tur_ini,f_tur_fin);
                  }else{
                    if(err)
                      err +=', rango valido ('+turnos.hora_a+' - '+turnos.hora_b+')';
                    else
                      err ='No existen turnos para la hora seleccionada, rango valido ('+turnos.hora_a+' - '+turnos.hora_b+')';
                  }
                }
              }
            }else{
              err = 'No hay disponibilidad de turnos para: '+datos_det.respuesta;
            }
          }else
            err = datos_det.titulo+' no tiene un horario establecido';


      }
    }

    var sin_seg = body.turnos.filter(p => p.segmenta == 1);
    if(sin_seg.length==0){
      const Horario = await Horarios.query()
        .where('predio',body.predio)
        .where('fecha',body.fecha)
        .where('tipo','todos')
        .where('tipo_operacion','todos')
      if(Horario[0]){
        if(Horario.length>0){
          for (var t = 0; t < Horario.length; t++) {
            if(err){ // comprobar error cuando hay mas de una segmenta por consultar y si lo termina el for para mandar el error
              t = Horario.length;
            }
            let turnos = Horario[t];
            f_tur = moment(body.hora+':00', 'hh:mm:ss');
            f_ini = moment(turnos.hora_a, 'hh:mm:ss');
            f_fin = moment(turnos.hora_b, 'hh:mm:ss');
            var fec_tur = moment(f_tur).isBetween(f_ini, f_fin, undefined, '[]');
            if(fec_tur){
              t = Horario.length;
              f_tur_ini = moment(body.fecha+' '+body.hora).format('YYYY-MM-DD HH:mm:ss');
              f_tur_fin = moment(body.fecha+' '+body.hora).add(turnos.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
              datos.fecha_fin = f_tur_fin;
              const turnos_rango = await Turno.query()
              .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
              .whereRaw('(estatus="0" OR estatus="1")')
              .where('id_predio',datos.id_predio)
              .havingRaw('fecha_m = "'+f_tur_ini+'"')
              .groupBy('id');

              if(turnos_rango.length>=turnos.disponibilidad)
                err = 'No hay disponibilidad para la hora seleccionada, intente con otra hora';
              else
                err = false;
            }
          }
        }
      }
    }

  }

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  if(!err){
    if(datos.id_tanda && body.verificar_horario){
      f_tur_ini = moment(body.fecha+' 00:00:00').format('YYYY-MM-DD HH:mm:ss');
      f_tur_fin = moment(body.fecha+' 23:59:59').format('YYYY-MM-DD HH:mm:ss');

      const turnos_rango = await Turno.query()
        .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
        .whereRaw('(estatus="0" OR estatus="1")')
        .where('id_predio',datos.id_predio)
        .where('id_chofer',datos.id_chofer)
        .havingRaw('fecha_m >= "'+f_tur_ini+'" AND fecha_m <= "'+f_tur_fin+'"')
        .groupBy('id');
      //console.log(turnos_rango);
      if(turnos_rango.length>0){
        return AR.enviarDatos({r:false,confirmar:true,msg:'Ya tienes un turno para este predio.'}, res);;
      }
    }
  }

  const PRE = await Predio.query().findById(datos.id_predio).select('estatus').catch(err => {console.error(err);});
  if(PRE['estatus'])
  	datos.estatus = PRE['estatus'];

  await Turno.query()
    .insert(datos)
    .then(async resp => {
      if(body.turnos){
        for (var i = 0; i < body.turnos.length; i++) {
          let datos_det = body.turnos[i];
          const datos_d = {
            titulo:datos_det.titulo,
            segmenta:datos_det.segmenta,
            respuesta:datos_det.respuesta,
            id_turno: resp.id
          }
          if(datos_det.tipo=='Imagen' && datos_det.respuesta){
            datos_d.esimagen = true;
            var file = "img_dp"+datos_det.id+"__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
            await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(datos_det.respuesta.split(",")[1], 'base64'), 'base64', function(err) {
              console.log(err);
            });
            datos_d.respuesta = config_ar.rutaArchivo(file);
          }
          await Detalles_Turnos.query().insert(datos_d).catch(err => {console.error(err);});
        }

        // Enviar correo de turnos aprobados
        const turno_detalle = await Turno.query()
          .select('turno.*', 'chofer.correo as correo_chofer', 'predio.estatus AS estatus_predio')
          .join('predio', 'predio.id', 'turno.id_predio')
          .join('chofer', 'chofer.id', 'turno.id_chofer')
          // .where('turno.id', resp.id)
          .findById(resp.id)
          // .where('turno.estatus', '1')
          .catch(console.error);

        const correo_turno = await Emails.query()
          .findOne('tipo_correo', 'turno_presentado')
          .catch(console.error);


        // Enviar correo personalizado
        const variables = await Emails.variablesPorIdTurno(resp.id);
        const correo = {...correo_turno};
        correo.encabezado = reemplazarVariables(correo.encabezado, variables);
        correo.cuerpo = reemplazarVariables(correo.cuerpo, variables).split('\n').join('<br>');

        const config = {
          to: turno_detalle.correo_chofer,
          subject: 'Información sobre turnos',
          data: correo,
        };
        console.log('enviando correo...')
        sendMail('Turno_correo_base.html', config)
          .then((r) => console.log(r))
          .catch(console.error);

        /*
        for (let turno of turnos_detalle) {
          if (turno.estatus_predio != 1) continue;

          await Turno.query()
            .patchAndFetchById(turno.id, {estatus:'1'})
            .then(async data => {
              // Enviar correo personalizado

              const variables = await Emails.variablesPorIdTurno(turno.id);
              const correo = {...correo_turno};
              correo.encabezado = reemplazarVariables(correo.encabezado, variables);
              correo.cuerpo = reemplazarVariables(correo.cuerpo, variables).split('\n').join('<br>');

              const config = {
                to: turno.correo_chofer,
                subject: 'Información sobre turnos',
                data: correo,
              };

              sendMail('Turno_correo_base.html', config)
                .then()
                .catch(console.error);
            })
            .catch(console.error);
        }
        */
      }
      resp.r = true;
      resp.msg = "Se guardo correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.put('/turno_editar', upload.none(),  validar('chofer'), async(req, res) => {
  const body = req.body;
  //console.log(body);
  const datos = {
    nombre_chofer: body.nombre,
    id_tanda: body.id_horario
  };
  if(body.vehiculo)
    datos.id_vehiculo = body.vehiculo;
  else
    datos.id_vehiculo = null;
  if(body.edito_fecha){
    datos.fecha = body.fecha+" "+body.hora;
    datos.fecha_fin = body.fecha+" "+body.hora;
  }
  // Validar datos
  let err = false;

  if (!body.id_turno)
    err = 'Ingrese el ID turno';
  else if (!datos.nombre_chofer)
    err = 'Ingresa el nombre del chofer';
  if(!err){
    for (var i = 0; i < body.turnos.length; i++) {
      let datos_det = body.turnos[i];
      if(!datos_det.respuesta && datos_det.obligatorio){
        err = 'Debes completar el campo '+datos_det.titulo;
        i = body.turnos.length;
      }
      else if(datos_det.tipo=='Texto' && datos_det.obligatorio && datos_det.respuesta.length<datos_det.min){
        err = 'El campo "'+datos_det.titulo+'" debe tener más de '+datos_det.min+' caracteres';
        i = body.turnos.length;
      }
      else if(datos_det.tipo=='Texto' && datos_det.respuesta && datos_det.respuesta.length>0 && datos_det.respuesta.length<datos_det.min){
        err = 'El campo "'+datos_det.titulo+'" debe tener más de '+datos_det.min+' caracteres';
        i = body.turnos.length;
      }

    }
  }

  if (!body.fecha && !err)
    err = 'Ingrese la fecha';
  else if (!body.hora && !err)
    err = 'Ingrese la hora';


  var datos_consultar = '';
  if(!err && body.edito_fecha){
    for (var i = 0; i < body.turnos.length; i++) {
      let datos_det = body.turnos[i];
      if(datos_det.segmenta){

          const Horario = await Horarios.query()
            .where('id_tanda',datos.id_tanda)
            .where('predio',body.predio)
            .where('fecha',body.fecha)
            .where('tipo',datos_det.variable)
            .whereRaw('find_in_set("'+datos_det.respuesta+'",tipo_operacion) <> 0')
          if(Horario[0]){
            if(Horario.length>0){
              if(err){ // comprobar error cuando hay mas de una segmenta por consultar y si lo termina el for para mandar el error
                i = body.turnos.length;
              }else{
                for (var t = 0; t < Horario.length; t++) {
                  let turnos = Horario[t];
                  f_tur = moment(body.hora+':00', 'hh:mm:ss');
                  f_ini = moment(turnos.hora_a, 'hh:mm:ss');
                  f_fin = moment(turnos.hora_b, 'hh:mm:ss');
                  var fec_tur = moment(f_tur).isBetween(f_ini, f_fin, undefined, '[]');
                  if(fec_tur){
                    t = Horario.length;
                    f_tur_ini = moment(body.fecha+' '+body.hora).format('YYYY-MM-DD HH:mm:ss');
                    f_tur_fin = moment(body.fecha+' '+body.hora).add(turnos.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
                    datos.fecha_fin = f_tur_fin;
                    const turnos_rango = await Turno.query()
                    .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
                    .whereRaw('(estatus="0" OR estatus="1")')
                    .where('id_predio',body.predio)
                    .where('id_tanda',datos.id_tanda)
                    .havingRaw('fecha_m = "'+f_tur_ini+'"')
                    .groupBy('id');

                    if(turnos_rango.length>=turnos.disponibilidad)
                      err = 'No hay disponibilidad para la hora seleccionada, intente con otra hora';
                    else
                      err = false;
                  }else{
                    if(err)
                      err +=', rango valido ('+turnos.hora_a+' - '+turnos.hora_b+')';
                    else
                      err ='No existen turnos para la hora seleccionada, rango valido ('+turnos.hora_a+' - '+turnos.hora_b+')';
                  }
                }
              }
            }else{
              err = 'No hay disponibilidad de turnos para: '+datos_det.respuesta;
            }
          }else
            err = datos_det.titulo+' no tiene un horario establecido';


      }
    }

    var sin_seg = body.turnos.filter(p => p.segmenta == 1);
    if(sin_seg.length==0){
      const Horario = await Horarios.query()
        .where('predio',body.predio)
        .where('fecha',body.fecha)
        .where('tipo','todos')
        .where('tipo_operacion','todos')
      if(Horario[0]){
        if(Horario.length>0){
          for (var t = 0; t < Horario.length; t++) {
            if(err){ // comprobar error cuando hay mas de una segmenta por consultar y si lo termina el for para mandar el error
              t = Horario.length;
            }
            let turnos = Horario[t];
            f_tur = moment(body.hora+':00', 'hh:mm:ss');
            f_ini = moment(turnos.hora_a, 'hh:mm:ss');
            f_fin = moment(turnos.hora_b, 'hh:mm:ss');
            var fec_tur = moment(f_tur).isBetween(f_ini, f_fin, undefined, '[]');
            if(fec_tur){
              t = Horario.length;
              f_tur_ini = moment(body.fecha+' '+body.hora).format('YYYY-MM-DD HH:mm:ss');
              f_tur_fin = moment(body.fecha+' '+body.hora).add(turnos.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
              datos.fecha_fin = f_tur_fin;
              const turnos_rango = await Turno.query()
              .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
              .whereRaw('(estatus="0" OR estatus="1")')
              .where('id_predio',body.predio)
              .havingRaw('fecha_m = "'+f_tur_ini+'"')
              .groupBy('id');

              if(turnos_rango.length>=turnos.disponibilidad)
                err = 'No hay disponibilidad para la hora seleccionada, intente con otra hora';
              else
                err = false;
            }
          }
        }
      }
    }

  }

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Turno.query()
    .patchAndFetchById(body.id_turno, datos)
    .then(async resp => {
      //console.log(body.turnos);
      if(body.turnos){
        for (var i = 0; i < body.turnos.length; i++) {
          let datos_det = body.turnos[i];
          const datos_d = {
            titulo:datos_det.titulo,
            segmenta:datos_det.segmenta,
            respuesta:datos_det.respuesta,
            id_turno: resp.id
          }
          if(datos_det.tipo=='Imagen' && datos_det.respuesta){
            datos_d.esimagen = true;
            var img_vieja = datos_det.respuesta.split("uploads")[1];
            if(img_vieja){
              datos_d.respuesta = false;
            }else{
              var file = "img_dp"+datos_det.id+"__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
              await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(datos_det.respuesta.split(",")[1], 'base64'), 'base64', function(err) {
                console.log(err);
              });
              datos_d.respuesta = config_ar.rutaArchivo(file);
            }
          }
          if(datos_d.respuesta!=false){
            if(datos_det.id_dt)
              await Detalles_Turnos.query().patchAndFetchById(datos_det.id_dt, datos_d).catch(err => {console.error(err);});
            else
              await Detalles_Turnos.query().insert(datos_d).catch(err => {console.error(err);});

          }
        }
      }

      const chofer = await Chofer.query().findById(resp.id_chofer).catch(console.error);
      let query = Emails.query();
      query.where('tipo_correo', 'turno_editado');

      const correo_turno = await query.first();
      const variables = await Emails.variablesPorIdTurno(resp.id);

      correo_turno.encabezado = reemplazarVariables(correo_turno.encabezado, variables);
      correo_turno.cuerpo = reemplazarVariables(correo_turno.cuerpo, variables).split('\n').join('<br>');

      const config = {
        to: chofer.correo,
        subject: 'Información sobre turnos',
        data: correo_turno,
      };

      sendMail('Turno_correo_base.html', config)
        .then()
        .catch(console.error);


      resp.r = true;
      resp.msg = "Se actualizó correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.put('/turno/cancelar/:id(\\d+)', validar('chofer'), upload.none(), /*validar('admin'),*/ async(req, res) => {
  // Validar datos
  let err = false;

  if (!req.params.id)
    err = 'Ingresa la ID';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  await Turno.query()
    .patchAndFetchById(req.params.id, {estatus:'-1',fecha_actualizado: moment().format('YYYY-MM-DD HH:mm:ss')})
    .then(async resp => {
      resp.r = true;
      resp.msg = "Se cancelo correctamente";
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/turnos/',  validar('chofer'), async(req, res) => {
  let data = req.query;
  const estatus_listar = [
    '0',
    '1' // aprobado
  ];
  let fecha_hoy =  moment().format('YYYY-MM-DD');
  const Query = Turno.query()
  .where("id_chofer",data.id_chofer)
  .whereRaw('(estatus="0" OR estatus="1")')
  //.where("id_predio",data.id_predio)
  //.havingIn('estatus', estatus_listar)
  //.where("fecha_fin",'>',fecha_hoy+' 00:00:00')
  //.where("estatus",'0')
  .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_comp_turno'),raw('DATE_FORMAT(turno.fecha, "%d-%m-%Y")').as('fecha_turno'),raw('DATE_FORMAT(turno.fecha, "%h:%i %p")').as('hora_turno'));
  Query.orderBy('fecha');
  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/turnos-historial/',  validar('chofer'), async(req, res) => {
  let data = req.query;
  let page = data.page;
  const Query = Turno.query().where("id_chofer",data.id_chofer).where("id_predio",data.id_predio).where("estatus",'<>','0').where("estatus",'<>','1')
  .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_comp_turno'),raw('DATE_FORMAT(turno.fecha, "%d-%m-%Y")').as('fecha_turno'),raw('DATE_FORMAT(turno.fecha, "%h:%i %p")').as('hora_turno'));
  Query.orderBy('fecha', 'DESC');
  if (page)
    Query.page(page, 10);
  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/proximo-turno/',  validar('chofer'), async(req, res) => {
  let data = req.query;
  let fec_a_buscar = moment().add(2,'h').format('YYYY-MM-DD HH:mm:ss');
  const Query = Turno.query().where("id_chofer",data.id_chofer).whereRaw('(estatus="0" OR estatus="1")').where('fecha','<=',fec_a_buscar)
  .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_comp_turno'),raw('DATE_FORMAT(turno.fecha, "%d-%m-%Y")').as('fecha_turno'),raw('DATE_FORMAT(turno.fecha, "%h:%i %p")').as('hora_turno'));
  Query.orderBy('fecha', 'ASC').limit(1);
  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/choferes-en-predios/:predio', validar('admin'), async(req, res) => {

  Asignar_Predio.query()
  .join('chofer','chofer.id','asignar_predio.id_chofer')
  .join('predio','predio.id','asignar_predio.id_predio')
  .join('cod_pais','cod_pais.id','chofer.id_cod_telefono')
  .where("asignar_predio.id_predio", req.params.predio)
  .select('cod_pais.cod', 'chofer.telefono', 'chofer.nombre', 'chofer.apellido','chofer.id')
  .then(trae => {
    AR.enviarDatos(trae, res)
  }).catch(err =>{
    res.sendStatus(HTTP_SERVER_ERROR);
  });

});

///////// API PARA PAGINAR EXTERNA /////////
router.get('/turnos-api/', validar('admin'), async(req, res) => {
  let data = req.query;

  const Query = TurnoApi.query()
  .select('id','estatus','id_chofer','id_vehiculo','id_predio','nombre_chofer',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_comp_turno'),raw('DATE_FORMAT(turno.fecha, "%d-%m-%Y")').as('fecha_turno'),raw('DATE_FORMAT(turno.fecha, "%h:%i %p")').as('hora_turno'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_comp_turno_fin'),raw('DATE_FORMAT(turno.fecha_fin, "%d-%m-%Y")').as('fecha_turno_fin'),raw('DATE_FORMAT(turno.fecha_fin, "%h:%i %p")').as('hora_fin_turno'),raw('DATE_FORMAT(fecha_actualizado, "%Y-%m-%d %H:%i:%s")').as('fecha_fue_actualizado'));

  if(data.estatus)
    Query.where("estatus",data.estatus);
  else
    Query.where("estatus",'0');
  if(data.id_chofer)
    Query.where("id_chofer",data.id_chofer);
  if(data.id_predio)
    Query.where("id_predio",data.id_predio)
  if(data.fecha_desde && data.fecha_hasta)
    Query.where("fecha",'>=',data.fecha_desde+' 00:00:00').where("fecha",'<=',data.fecha_hasta+' 23:59:59')

  Query.orderBy('fecha');
  await Query
    .then(datos => AR.enviarDatos(datos, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.post('/turno-editar-api/', upload.none(), validar('admin'), async(req, res) => {
  // Validar datos
  let err = false;

  if (!req.body.turno)
    err = 'Ingresa la ID del turno';
  else if (!req.body.estatus)
    err = 'Ingresa es estatus';
  //else if (req.body.estatus!='1' && req.body.estatus!='0')
  //  err = 'Estatus no existe';
  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  const datos = {
    estatus:req.body.estatus,
    fecha_actualizado:moment().format('YYYY-MM-DD HH:mm:ss')
  }
  if(datos.estatus=='2')
    datos.fecha_ingresado = moment().format('YYYY-MM-DD HH:mm:ss');

  const turno = await Turno.query().findById(req.body.turno).catch(console.error);

  if (turno.estatus == req.body.estatus) {
    return AR.enviarDatos({ r: true, msg: 'Actualizado correctamente' }, res);
  }

  await Turno.query()
    .patchAndFetchById(req.body.turno, datos)
    .then(async resp => {
      const predio = await Predio.query().findById(resp.id_predio).catch(console.error);

      if (predio.usar_correo==1 && (resp.estatus == -2 || resp.estatus == 1 || resp.estatus == 2)) {
        // Enviar correo personalizado
        const chofer = await Chofer.query().findById(resp.id_chofer).catch(console.error);
        let query = Emails.query();
        if (resp.estatus == -2)
          query.where('tipo_correo', 'turno_rechazado')
        else if (resp.estatus == 1)
          query.where('tipo_correo', 'turno_aprobado')
        else if (resp.estatus == 2)
          query.where('tipo_correo', 'turno_presentado')

        const correo_turno = await query.first();
        const variables = await Emails.variablesPorIdTurno(resp.id);

        correo_turno.encabezado = reemplazarVariables(correo_turno.encabezado, variables);
        correo_turno.cuerpo = reemplazarVariables(correo_turno.cuerpo, variables).split('\n').join('<br>');

        const config = {
          to: chofer.correo,
          subject: 'Información sobre turnos',
          data: correo_turno,
        };

        sendMail('Turno_correo_base.html', config)
          .then()
          .catch(console.error);
      }

      r = {};
      r.r = true;
      r.msg = "Actualizado correctamente";
      AR.enviarDatos(r, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/exportar/excel/:predio/', /*validar('chofer'),*/ async(req, res) => {
    const Query = await Detalles_Predio.query().where('id_predio',req.params.predio);
    const PRE = await Predio.query().findById(req.params.predio).select('nombre').catch(err => {console.error(err);});
    var datos_tit = [{ header: 'Nombre chofer', key: 'Nombre chofer', width: 20 },{ header: 'Vehículo(Patente)', key: 'Vehículo(Patente)', width: 20 },{header: 'Fecha', key: 'Fecha', width: 20},{header: 'Hora', key: 'Hora', width: 20}];
    for (var i = 0; i < Query.length; i++) {
      var variables = '';
      if(Query[i].tipo=='Select')
        variables = ":("+Query[i].opciones+")";
      datos_tit.push({header: Query[i].titulo+variables, key: Query[i].titulo, width: 20})
    }
    var workbook = new ExcelJS.Workbook();

    workbook.creator = 'EasyDocking';
    workbook.lastModifiedBy = 'EasyDocking';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;

    workbook.views = [
        {
            x: 0, y: 0, width: 10000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ];
    var worksheet = workbook.addWorksheet('Solicitar turnos');
    worksheet.columns = datos_tit;

    //worksheet.addRow({ id: 1, name: 'John Doe', dob: new Date(1970, 1, 1) });

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", "attachment; filename=" + "Formato turno "+PRE['nombre']+".xlsx");
    workbook.xlsx.write(res)
        .then(function (data) {
            res.end();
        });
});

router.post('/turno/importar/excel/', upload.none(), validar('chofer'), async(req, res) => {
  const body = req.body;
  var turnos = [];
  var err = false;
  var err_general = false;
  var fecha_act = moment().format('YYYY-MM-DD');
  //console.log(body);
  if(body.respuestas&&body.respuestas.length>0){
    const Pre = await Predio.query().findById(body.predio).catch(err => {console.error(err);})

    for (var p = 0; p < body.titulos.length; p++) {
      body.titulos[p] = body.titulos[p].split(':(')[0];
    }

    for (var i = 0; i < body.respuestas.length; i++) {
      turnoarray = {
        id_chofer: body.chofer,
        nombre_chofer: body.respuestas[i][0],
        id_predio: body.predio,
        fec: body.respuestas[i][2],
        hora: body.respuestas[i][3],
        fecha: moment(body.respuestas[i][2]+" "+body.respuestas[i][3], "YYYY-MM-DD HH:mm").format("YYYY-MM-DD HH:mm"),
        fecha_fin: moment(body.respuestas[i][2]+" "+body.respuestas[i][3], "YYYY-MM-DD HH:mm").format("YYYY-MM-DD HH:mm"),
        estatus: body.estatus_predio,
        fecha_actualizado: moment().format('YYYY-MM-DD HH:mm:ss'),
        detalles: [],
        respuestas: body.respuestas[i]
      };

      if(body.respuestas[i][1]){
        const Veh = await Vehiculo.query().where('id_chofer', body.chofer).where('aprobacion','1').where('patente', body.respuestas[i][1]);
        if(Veh[0]){
          turnoarray.id_vehiculo = Veh[0].id;
        }
      }

      for (var p = 4; p < body.titulos.length; p++) {
        //console.log(body.titulos[p],body.respuestas[i][p]);
        const Variables = await Detalles_Predio.query().where('id_predio',body.predio).where('titulo',body.titulos[p]);
        if(Variables[0]){
            var esimg = 0;
            if(Variables[0].tipo=='Imagen')
              esimg = 1;
            turnoarray.detalles.push({
              titulo:body.titulos[p],
              respuesta:body.respuestas[i][p],
              segmenta: Variables[0].segmenta,
              variable: Variables[0].variable,
              esimagen: esimg
            })
        }else{
          err_general = 'El predio seleccionado no coincide con la información proporcionada';
          p = body.titulos.length;
        }

      }
      turnos.push(turnoarray);
    }
    if(turnos.length>0 && !err_general){
      for (var tu = 0; tu < turnos.length; tu++) {
      	if(err){
      		if(err_general)
      			err_general += '.<br>-'+err;
      		else
      			err_general = '<br>-'+err;
      	}
      	err = false;
      	var fecha_dispo = true;
      	var fecha_dif_1 = moment(fecha_act+' 00:00:00');
		    var fecha_dif_2 = moment(turnos[tu].fec+' 00:00:00');
		    var dif_dias = fecha_dif_2.diff(fecha_dif_1, 'days');

        var dias_max = Pre['dias_maximo'];

        var horas_add =0;
        if(Pre['horas_maxima'])
          horas_add = Pre['horas_maxima'];
        let fec_ini_r = moment(turnos[tu].fecha);
        let fec_pre_r = moment().add(horas_add,'h');
        let diferencia_ran = fec_ini_r.diff(fec_pre_r);
        //console.log(fec_ini_r,fec_pre_r,diferencia_ran);
        if(diferencia_ran<=0 && horas_add>0){
          err = 'El turno del '+turnos[tu].fec+' '+turnos[tu].hora+' no esta disponible';
          fecha_dispo = false;
        }else if(dif_dias>dias_max && dias_max>0){
      		err = 'El turno del '+turnos[tu].fec+' '+turnos[tu].hora+' no puede ser solicitado con mas de '+dias_max+' dias de antelación';
      		fecha_dispo = false;
      	}else if(dif_dias<0){
      		err = 'El turno del '+turnos[tu].fec+' '+turnos[tu].hora+' no esta disponible';
      		fecha_dispo = false;
      	}

      	if(fecha_dispo){
	        for (var i = 0; i < turnos[tu].detalles.length; i++) {
	          let datos_det = turnos[tu].detalles[i];
	          if(datos_det.segmenta){
	            //console.log(body.predio,turnos[tu].fecha,datos_det.variable);

	              const Horario = await Horarios.query()
	                .where('predio',body.predio)
	                .where('fecha',turnos[tu].fec)
	                .where('fecha','>=',fecha_act)
	                .where('tipo',datos_det.variable)
	                .whereRaw('find_in_set("'+datos_det.respuesta+'",tipo_operacion) <> 0')
	              if(Horario[0]){
	                //console.log(Horario);
	                if(Horario.length>0){
	                  if(err){ // comprobar error cuando hay mas de una segmenta por consultar y si lo termina el for para mandar el error
	                    i = turnos[tu].detalles.length;
	                  }else{
	                    for (var t = 0; t < Horario.length; t++) {
	                      let horar = Horario[t];
	                      f_tur = moment(turnos[tu].hora+':00', 'hh:mm:ss');
	                      f_ini = moment(horar.hora_a, 'hh:mm:ss');
	                      f_fin = moment(horar.hora_b, 'hh:mm:ss');
	                      var fec_tur = moment(f_tur).isBetween(f_ini, f_fin, undefined, '[]');
	                      if(fec_tur){
	                        t = Horario.length;
	                        f_tur_ini = moment(turnos[tu].fec+' '+turnos[tu].hora).format('YYYY-MM-DD HH:mm:ss');
	                        f_tur_fin = moment(turnos[tu].fec+' '+turnos[tu].hora).add(horar.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
	                        turnos[tu].fecha_fin = f_tur_fin;
	                        const turnos_rango = await Turno.query()
	                        .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
	                        .whereRaw('(estatus="0" OR estatus="1")')
	                        .where('id_predio',turnos[tu].id_predio)
	                        .havingRaw('fecha_m = "'+f_tur_ini+'"')
	                        .groupBy('id');

	                        if(turnos_rango.length>=horar.disponibilidad)
	                          err = 'No hay disponibilidad para el horario '+turnos[tu].fec+' '+turnos[tu].hora+', intente con otra hora';
	                        else{
	                          turnos[tu].id_tanda = horar.id_tanda;
	                          //err = false;
	                        }
	                      }else{
	                        /*if(err)
	                          err +=', rango valido ('+horar.hora_a+' - '+horar.hora_b+')';
	                        else
	                          err ='No existen turnos para la hora seleccionada, rango valido ('+horar.hora_a+' - '+horar.hora_b+')';*/
	                      }
	                    }
	                  }
	                }else{
	                  err = 'No hay disponibilidad de turnos para el '+turnos[tu].fec+' '+turnos[tu].hora;
	                }
	              }else
	                err = 'Para el turno del '+turnos[tu].fec+' '+turnos[tu].hora+' no tiene un horario establecido';


	          }
	        }

	        var sin_seg = turnos[tu].detalles.filter(p => p.segmenta == 1);
	        if(sin_seg.length==0){
	          const Horario = await Horarios.query()
	            .where('predio',body.predio)
	            .where('fecha',turnos[tu].fec)
	            .where('fecha','>=',fecha_act)
	            .where('tipo','todos')
	            .where('tipo_operacion','todos')
	            //console.log(Horarios);
	          if(Horario[0]){
	            if(Horario.length>0){
	              for (var t = 0; t < Horario.length; t++) {
	                if(err){ // comprobar error cuando hay mas de una segmenta por consultar y si lo termina el for para mandar el error
	                  t = Horario.length;
	                }
	                let horar = Horario[t];
	                f_tur = moment(turnos[tu].hora+':00', 'hh:mm:ss');
	                f_ini = moment(horar.hora_a, 'hh:mm:ss');
	                f_fin = moment(horar.hora_b, 'hh:mm:ss');
	                var fec_tur = moment(f_tur).isBetween(f_ini, f_fin, undefined, '[]');
	                if(fec_tur){
	                  t = Horario.length;
	                  f_tur_ini = moment(turnos[tu].fec+' '+turnos[tu].hora).format('YYYY-MM-DD HH:mm:ss');
	                  f_tur_fin = moment(turnos[tu].fec+' '+turnos[tu].hora).add(horar.frecuencia, 'minutes').format('YYYY-MM-DD HH:mm:ss');
	                  turnos[tu].fecha_fin = f_tur_fin;
	                  const turnos_rango = await Turno.query()
	                  .select('*',raw('DATE_FORMAT(fecha, "%Y-%m-%d %H:%i:%s")').as('fecha_m'),raw('DATE_FORMAT(fecha_fin, "%Y-%m-%d %H:%i:%s")').as('fecha_fin_m'))
	                  .whereRaw('(estatus="0" OR estatus="1")')
	                  .where('id_predio',turnos[tu].id_predio)
	                  .havingRaw('fecha_m = "'+f_tur_ini+'"')
	                  .groupBy('id');
	                  if(turnos_rango.length>=horar.disponibilidad){
	                    err = 'No hay disponibilidad para el horario '+turnos[tu].fec+' '+turnos[tu].hora+', intente con otra hora';
	                  }else{
	                    turnos[tu].id_tanda = horar.id_tanda;
	                    //err = false;
	                  }
	                }
	              }
	            }
	          }else{
	          	const Query = await Horarios.query()
  			      .where('predio',turnos[tu].id_predio)
  			      .where('tipo','todos')
  			      .where('tipo_operacion','todos')
  			      .select(raw('count(*)').as('total'));

    			    if(Query[0] && Query[0].total>0){
    			    	err = 'Para el turno del '+turnos[tu].fec+' '+turnos[tu].hora+' no tiene un horario establecido';
    			    }else{
    			    	turnos[tu].id_tanda2 = 1;
    			    }
              //console.log(Query,err,turnos[tu].id_predio);
	          }
	        }

	        if(turnos[tu].id_tanda || turnos[tu].id_tanda2){
	          var datos_t = {
	            id_chofer: turnos[tu].id_chofer,
	            nombre_chofer: turnos[tu].nombre_chofer,
	            id_predio: turnos[tu].id_predio,
	            fecha: turnos[tu].fecha,
	            fecha_fin: turnos[tu].fecha_fin,
	            estatus: turnos[tu].estatus,
              fecha_actualizado: moment().format('YYYY-MM-DD HH:mm:ss')
	          }
	          if(turnos[tu].id_tanda)
	            datos_t.id_tanda = turnos[tu].id_tanda;
	          if(turnos[tu].id_vehiculo)
	            datos_t.id_vehiculo = turnos[tu].id_vehiculo;
	          const inserturno = await Turno.query().insert(datos_t);
	          if(turnos[tu].detalles && inserturno){
	            for (var i = 0; i < turnos[tu].detalles.length; i++) {
	                let datos_det = turnos[tu].detalles[i];
	                const datos_d = {
	                  titulo:datos_det.titulo,
	                  segmenta:datos_det.segmenta,
	                  respuesta:datos_det.respuesta,
	                  id_turno: inserturno.id
	                }
	                await Detalles_Turnos.query().insert(datos_d).catch(err => {console.error(err);});
	            }
              // Enviar correo de turnos aprobados
              const turno_detalle = await Turno.query()
                .select('turno.*', 'chofer.correo as correo_chofer', 'predio.estatus AS estatus_predio')
                .join('predio', 'predio.id', 'turno.id_predio')
                .join('chofer', 'chofer.id', 'turno.id_chofer')
                .findById(inserturno.id)
                .catch(console.error);

              const correo_turno = await Emails.query()
                .findOne('tipo_correo', 'turno_presentado')
                .catch(console.error);


              // Enviar correo personalizado
              const variables = await Emails.variablesPorIdTurno(inserturno.id);
              const correo = {...correo_turno};
              correo.encabezado = reemplazarVariables(correo.encabezado, variables);
              correo.cuerpo = reemplazarVariables(correo.cuerpo, variables).split('\n').join('<br>');

              const config = {
                to: turno_detalle.correo_chofer,
                subject: 'Información sobre turnos',
                data: correo,
              };
              console.log('enviando correo...')
              sendMail('Turno_correo_base.html', config)
                .then((r) => console.log(r))
                .catch(console.error);
	          }
	        }
	    }
      }

    }


    if(err){
      	if(err_general)
      		err_general += '.<br>-'+err;
      	else
      		err_general = '<br>-'+err;
    }
    //console.log(turnos);
    //var fecha_ser = moment().format("YYYY-MM-DD HH:mm:ss");
    AR.enviarDatos({r:true,msj:err_general,turnos:turnos}, res);
  }else{
    AR.enviarDatos({r:false,msj:"No hay datos para guardar"}, res);
  }

});

router.get('/ruta-predio-chofer/:predio/:chofer', validar('admin','chofer'), async(req, res) => {

  const Pre = await Predio.query().findById(req.params.predio).catch(err => {console.error(err);})
  const Cho = await Chofer.query().findById(req.params.chofer).catch(err => {console.error(err);})
  AR.enviarDatos({predio:Pre,chofer:Cho}, res);
});



module.exports = router;
