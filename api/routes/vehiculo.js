const Admin = require('../models/Admin');
const Cod_Telefono = require('../models/Cod_Telefono');
const Chofer = require('../models/Chofer');
const Vehiculo = require('../models/Vehiculo');
const Tipo_Vehiculo = require('../models/Tipo_Vehiculo');
const Notificacion = require('../models/Notificacion');

const AR = require('../ApiResponser');
const validar = require('./sesion2');
const upload = require('multer')();
const router = require('express').Router();
const procesarImagen64 = require('../functions/procesarImagen64');

const sendMail = require('../mail');

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500


// Listar vehiculos.
router.get('/', validar('admin'), async(req, res) => {
  const params = req.query;

  const Query = Vehiculo.query();

  if (+params.page === +params.page)
    Query.page(params.page, +params.pagesize ? params.pagesize : undefined);

  if (params.filter && params.filter.length > 1){
  }

  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    })


  // await Vehiculo.query()
  //   .then(data => {
  //     AR.enviarDatos(data, res);
  //   })
  //   .catch(err => {
  //     console.error(err);
  //     res.sendStatus(HTTP_SERVER_ERROR)
  //   });
});


// Listar vehiculos por chofer.
router.get('/por-chofer/:id(\\d+)', validar('chofer'), async(req, res) => {
  await Vehiculo.query()
    .where('id_chofer', req.params.id)
    .then(r => AR.enviarDatos(r, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

router.get('/por-chofer-activos/:id(\\d+)', validar('chofer'), async(req, res) => {
  await Vehiculo.query()
    .where('id_chofer', req.params.id)
    .where('aprobacion','1')
    .then(r => AR.enviarDatos(r, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

// Registrar vehiculo.
router.post('/', upload.none(), validar('chofer'), async(req, res) => {
  const body = req.body;
  const datos = {
    id_chofer: body.id_chofer,
    tipo: body.tipo || body.tipo_vehiculo,
    patente: body.patente,
    vencimiento: body.vencimiento || body.vencimiento_registro,
    vencimiento_seguro: body.vencimiento_seguro || body.vencimiento_seguro,
    vencimiento_vtv: body.vencimiento_vtv || body.vencimiento_vtv,
  };
  if(body.patente2)
    datos.patente2 = body.patente2;

  // Validar datos
  let err = false;

  const tiene_vtv = false;

  if (!datos.tipo)
    err = 'Ingrese el tipo de vehículo';

  else if (!datos.id_chofer)
    err = 'No se especificó el chofer';

  else if (!datos.vencimiento)
    err = 'Ingrese el vencimiento del registro';

  else if (!datos.patente)
    err = 'Ingrese la patente';

  else if (!datos.vencimiento_seguro)
    err = 'Ingrese el vencimiento del seguro';

  else if (!datos.vencimiento_vtv && tiene_vtv)
    err = 'Ingrese el vencimiento del VTV';
  /*
  else if (!body.img_vtv && tiene_vtv)
    err = 'Ingrese la imagen del VTV';

  else if (!body.img_registro)
    err = 'Ingrese la imagen del registro';

  else if (!body.img_seguro)
    err = 'Ingrese la imagen del seguro';

  else if (!body.img_clausula)
    err = 'Ingrese la imagen del cláusula';

  else if (!body.img_art)
    err = 'Ingrese la imagen del ART';
  */
  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);


  datos.img = await procesarImagen64(body.img_registro, 'img_reg_');
  datos.img_vtv = await procesarImagen64(body.img_vtv, 'img_vtv_');
  datos.img_seguro = await procesarImagen64(body.img_seguro, 'img_seg_');
  datos.img_clausula = await procesarImagen64(body.img_clausula, 'img_cla_');
  datos.img_art = await procesarImagen64(body.img_art, 'img_art_');

  await Vehiculo.query()
    .insert(datos)
    .then(async data => {
      res.sendStatus(HTTP_OK);

      const chofer = await Chofer.query()
        .leftJoin('cod_pais', 'cod_pais.id', 'chofer.id_cod_telefono')
        .findById(data.id_chofer)
        .catch(console.error);

      const admins = await Admin.query().catch(console.error);
      const destinatarios = admins.map(adm => adm.correo);

      // let cod = await Cod_Telefono.query().findById(data.id_cod_telefono).catch(console.error);
      // if (!cod)
      //   cod = {cod:''};

      Notificacion.emitir({
        titulo: 'Vehículo registrado',
        contenido: `El chofer ${chofer.nombre.toUpperCase()} ${chofer.apellido.toUpperCase()}, teléfono ${chofer.codtelefono[0].cod}-${chofer.telefono} ha registrado un vehículo con patente ${data.patente}`,
        id_receptor: null, //todos
        usuario_receptor: Notificacion.usuario.ADMIN,
        id_emisor: data.id_chofer,
        usuario_emisor: Notificacion.usuario.CHOFER,
        id_usable: chofer.id,
      });
      /*
      const config = {
        to: destinatarios,
        subject: 'Vehículo registrado',
        data: {
          ocultar_nombre: true,
          nombre: chofer.nombre,
          apellido: chofer.apellido,
          correo: chofer.correo,
          telefono: chofer.telefono,
          cod_telefono: chofer.codtelefono[0].cod,
        }
      };

      sendMail('vehiculo_registrado.html', config)
        .then()
        .catch(console.error);
      */
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });

});


router.put('/reenviar/:id_vehiculo(\\d+)', upload.none(), validar('chofer'), async(req, res) => {
  const body = req.body;
  const datos = {
    id_chofer: body.id_chofer,
    tipo: body.tipo || body.tipo_vehiculo,
    patente: body.patente,
    vencimiento: body.vencimiento || body.vencimiento_registro,
    vencimiento_seguro: body.vencimiento_seguro || body.vencimiento_seguro,
    vencimiento_vtv: body.vencimiento_vtv || body.vencimiento_vtv,
    aprobacion: '0', //pendiente
  };

  // Validar datos
  let err = false;

  const tiene_vtv = false;

  if (!datos.tipo)
    err = 'Ingrese el tipo de vehículo';

  else if (!datos.id_chofer)
    err = 'No se especificó el chofer';

  else if (!datos.vencimiento)
    err = 'Ingrese el vencimiento del registro';

  else if (!datos.patente)
    err = 'Ingrese la patente';

  else if (!datos.vencimiento_seguro)
    err = 'Ingrese el vencimiento del seguro';

  else if (!datos.vencimiento_vtv && tiene_vtv)
    err = 'Ingrese el vencimiento del VTV';

  else if (!body.img_vtv && tiene_vtv)
    err = 'Ingrese la imagen del VTV';

  else if (!body.img_registro)
    err = 'Ingrese la imagen del registro';

  else if (!body.img_seguro)
    err = 'Ingrese la imagen del seguro';

  else if (!body.img_clausula)
    err = 'Ingrese la imagen del cláusula';

  else if (!body.img_art)
    err = 'Ingrese la imagen del ART';

  if (err)
    return AR.enviarError(err, res, HTTP_BAD_REQUEST);

  if (datos.img && datos.img.startsWith('data'))
    datos.img = await procesarImagen64(body.img_registro, 'img_reg_');

  if (datos.img_vtv && datos.img_vtv.startsWith('data'))
    datos.img_vtv = await procesarImagen64(body.img_vtv, 'img_vtv_');

  if (datos.img_seguro && datos.img_seguro.startsWith('data'))
    datos.img_seguro = await procesarImagen64(body.img_seguro, 'img_seg_');

  if (datos.img_clausula && datos.img_clausula.startsWith('data'))
    datos.img_clausula = await procesarImagen64(body.img_clausula, 'img_cla_');

  if (datos.img_art && datos.img_art.startsWith('data'))
    datos.img_art = await procesarImagen64(body.img_art, 'img_art_');

  await Vehiculo.query()
    .patchAndFetchById(req.params.id_vehiculo, datos)
    .then(async data => {

      const chofer = await Chofer.query()
        .leftJoin('cod_pais', 'cod_pais.id', 'chofer.id_cod_telefono')
        .findById(data.id_chofer)
        .catch(console.error);

      Notificacion.emitir({
        titulo: 'Vehículo registrado',
        contenido: `El chofer ${chofer.nombre.toUpperCase()} ${chofer.apellido.toUpperCase()}, teléfono ${chofer.codtelefono[0].cod}-${chofer.telefono} ha registrado un vehículo con patente ${data.patente}`,
        id_receptor: null, //todos
        usuario_receptor: Notificacion.usuario.ADMIN,
        id_emisor: chofer.id,
        usuario_emisor: Notificacion.usuario.CHOFER,
        id_usable: chofer.id,
      });

      res.sendStatus(HTTP_OK);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


router.put('/:id(\\d+)/aprobacion', upload.none(), validar('admin'), async(req, res) => {
  const id_vehiculo = req.params.id;
  const aprobacion = (req.body.aprobacion || req.body.estatus).toString();
  const motivo = req.body.motivo;

  if (!aprobacion || (aprobacion!=1 && aprobacion!=-1))
    return AR.enviarError('Valor inválido '+ aprobacion, res, HTTP_BAD_REQUEST);

  await Vehiculo.query()
    .patchAndFetchById(id_vehiculo, { aprobacion })
    .then(async data => {

      const accion = (data.aprobacion==1 ? 'aprobado' : 'rechazado')
      let contenido = `El vehículo con patente ${data.patente} ha sido ${accion}`;

      if (motivo && data.aprobacion == -1) {
        contenido += '. Motivo: ' + motivo;

        const chofer = await Chofer.query().findById(data.id_chofer).catch(console.error);
        const config = {
          to: chofer.correo,
          subject: 'Vehículo rechazado',
          data: {
            ocultar_nombre: true,
            nombre: chofer.nombre,
            apellido: chofer.apellido,
            correo: chofer.correo,
            telefono: chofer.telefono,
            cod_telefono: chofer.codtelefono[0].cod,
            motivo: motivo,
            patente: chofer.patente,
          }
        };

        sendMail('Vehiculo_rechazado.html', config)
          .then()
          .catch(console.error);
      }

      Notificacion.emitir({
        titulo: 'Vehiculo ' + accion,
        contenido: `El vehículo con patente ${data.patente} ha sido ${accion}`,
        id_receptor: data.id_chofer,
        usuario_receptor: Notificacion.usuario.CHOFER,
        push: true,
      });

      data.r = true;
      const tv = await Tipo_Vehiculo.query().findById(data.tipo).catch();
      data.tipo_vehiculo = tv.nombre;
      AR.enviarDatos(data, res);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR)
    });
});


router.delete('/:id(\\d+)', validar('chofer'), async(req, res) => {
  await Vehiculo.query()
    .findById(req.params.id)
    .del()
    .then(r => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});

module.exports = router;
