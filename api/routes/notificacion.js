const Notificacion = require('../models/Notificacion');

const AR = require('../ApiResponser');
const validar = require('./sesion2');
const upload = require('multer')();
const router = require('express').Router();

const
  HTTP_OK = 200,
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_UNAUTHORIZED = 401,
  HTTP_SERVER_ERROR = 500


router.get('/', validar('admin','chofer'), async(req, res) => {
  const tipo_usuario = req.headers['tipo_usuario_validado'];
  const id_usuario = req.query.id_usuario || req.headers.id_usuario;
  const nuevas = req.query.hasOwnProperty('new');

  const Query = Notificacion.query()
    .where({ usuario_receptor: tipo_usuario, id_receptor: id_usuario })
    .orderBy('fecha', 'desc')

  if (nuevas)
    Query.where('estatus', 0);

  Query.limit(20);

  await Query
    .then(data => AR.enviarDatos(data, res))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


// Marcar como leidas.
router.put('/estatus', validar('admin','chofer'), async(req, res) => {
  const tipo_usuario = req.headers['tipo_usuario_validado'];
  const id_usuario = req.query.id_usuario || req.headers.id_usuario;

  await Notificacion.query()
    .where({ id_receptor: id_usuario, usuario_receptor: tipo_usuario })
    .patch({ estatus: 1 })
    .limit(10)
    // .del()
    .then(() => res.sendStatus(HTTP_OK))
    .catch(err => {
      console.error(err);
      res.sendStatus(HTTP_SERVER_ERROR);
    });
});


module.exports = router;