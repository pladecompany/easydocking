const nodemailer = require('nodemailer');
const ejs = require('ejs');
const { mailConfig } = require('../config');


let testmailConfig = {
    // pool: true,
    host: "0.0.0.0",
    port: 1025,
    auth: {
      user: "no-responder@easydocking.com.ar",
      pass: "ndLXhQgb+t^U"
    },
    tls: {
      rejectUnauthorized: false
    }
  }


async function send(archivoTemplate, opciones) {
  const template = __dirname + '/templates/' + archivoTemplate;

  return nodemailer
  .createTransport(mailConfig)
  // .createTransport(testmailConfig)
  .sendMail({
    ...opciones,
    from:'easydocking <no-responder@easydocking.com.ar>',
    html: await ejs.renderFile(template, opciones.data),
    attachments: [{
      path: __dirname + '/logo.jpeg',
      cid: 'logo',
    }],
  });
};



module.exports = send;
