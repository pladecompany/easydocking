const nodemailer = require('nodemailer');
const ejs = require('ejs');
const fs = require('fs');
const TEMPDIR = __dirname + '/templates/';


const template = process.argv[2];
console.log('Render:', TEMPDIR + template);

const data = {
  nombre: 'NOMBRE',
  apellido: 'APELLIDO',
  pass: 'PASS',
  patente: 'PATENTE',
  ocultar_nombre: true,
  ocultar_informa: true,
  predios_aprobados: ['Predio1', 'Predio2'],
};


ejs.renderFile(TEMPDIR + template, data).then(data => {
    fs.writeFileSync(__dirname + '/test/' + template, data);
});

