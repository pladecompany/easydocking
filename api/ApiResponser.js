class ApiResponser {
    static enviarDatos(data, res) {
        return res.status(200).send(data);
    }

    static enviarMensaje(msg, res) {
        return res.status(200).send({
            msg: msg,
            code: 200
        });
    }

    static enviarAuth(res, status, token = null, user = null) {
        if (!status)
            res.status(401).send({
                auth: false,
                token: null
            });
        else
            res.status(200).send({
                auth: true,
                token: token,
                user: user
            });
    }

    static enviarError(mensaje, res, codigo) {
        return res.status(codigo).send({
            msg: mensaje
        });
    }
}

module.exports = ApiResponser;
