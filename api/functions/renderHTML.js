/*
 * Renderizar HTML
 * 
 * render(html[, data])
 * 
 * Una linea:
 *   <%= variable %>
 * 
 * Multilinea:
 *  <% for ( var i = 0; i < users.length; i++ ) { %>
 *    <p> <%= users[i].url %> </p>
 *  <% } %>
 * 
 * 
 * > html = "<p><%= x %></p>"
 * > data = { x: "TEXTO" }
 *
 * > render(html, data) //-> "<p>TEXTO</p>"
 * 
 *
 * @param {string} html
 * @param {object} data
 * @return {string | function}
 *
 * @copyright John Resig - https://johnresig.com/ - MIT Licensed
 */


module.exports = function renderHTML(str, data){
  const fn = new Function("obj",
      "var p=[],print=function(){p.push.apply(p,arguments);};" +

      // Introduce the data as local variables using with(){}
      "with(obj){p.push('" +

      // Convert the template into pure JavaScript
      str
        .replace(/[\r\t\n]/g, " ")
        .split("<%").join("\t")
        .replace(/((^|%>)[^\t]*)'/g, "$1\r")
        .replace(/\t=(.*?)%>/g, "',$1,'")
        .split("\t").join("');")
        .split("%>").join("p.push('")
        .split("\r").join("\\'")
    + "');}return p.join('');");
   
  // Provide some basic currying to the user
  return data ? fn( data ) : fn;
}
