const fs = require('fs');
const genToken = require('./genToken');
const config = require('../config');


async function procesarImagen64(data, prefijo='') {
  if (typeof data !== 'string' || !data.startsWith('data:image'))
    return null;

  const token = await genToken(30);
  const extension = getImageExt(data);
  const archivo = prefijo + token + extension;

  await fs.writeFile(
    __dirname + '/../public/uploads/' + archivo,
    new Buffer.from(data.split(',')[1], 'base64'),
    'base64',
    function(err){ err && console.error(err) }
  );

  return config.rutaArchivo(archivo);
}


function getImageExt(data) {
  const match = data.match(/image\/.*;/);
  if (!match || !match[0])
    return false;

  if (match[0] === 'image/jpg;' || match[0] === 'image/jpeg;')
    return '.jpg';

  if (match[0] === 'image/png;')
    return '.png';
}


module.exports = procesarImagen64;