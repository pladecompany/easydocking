module.exports = function reemplazarVariables(str, obj) {
  for (let [varname, value] of Object.entries(obj)) {
    if (str.includes(varname)) {
      str = str.split(varname).join(value);
    }
  }
  return str;
}
