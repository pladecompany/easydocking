const _ = require("lodash");
module.exports = function(socket) {
  return function(data) {
    let tipo_usu = "";
    if(data.tipo_usuario =='chofer') tipo_usu = 'chofer';
    else if(data.tipo_usuario =='predio') tipo_usu = 'predio';
    let set = {
      nombre: `${data.nombre}`,
      id: data.id,
      tabla: tipo_usu,
      roomChat: data.roomChat
    };
    console.log(set);
    global.USUARIOSC.push({
      socket: socket.id,
      user: set
    });

    IO.sockets.emit("conectadosOnline", global.USUARIOSC);
  };
};
