const desconectar = require("../functions/desconectar");
module.exports = function(socket){
    return function(data) {
            console.log("DESCONECTADO: " + socket.id);
            desconectar(socket.id);
            IO.sockets.emit("conectadosOnline", global.USUARIOSC);    
    }
}