const _ = require("lodash");
module.exports = {
  async enviarData(socketReceptor, commit, data) {
    for (let index = 0; index < socketReceptor.length; index++) {
      let __to = socketReceptor[index];
      global.IO.to(__to).emit(commit, data);
    }
  },
  encontrarID(socket) {
    console.log("Encontrando emisor del socket: " + socket);
    let userData = _.find(global.USUARIOSC, o => {
      return o.socket == socket;
    });

    if (userData) return userData.user.id;
  },
  encontrarSocket(id, tabla) {
    let s = [];
    for (let index = 0; index < global.USUARIOSC.length; index++) {
    let o = global.USUARIOSC[index];
      if(String(o.user.id) == String(id) && String(o.user.tabla) == String(tabla)) {
        s.push(o.socket)
      }
    }
    return s;
  }
};
