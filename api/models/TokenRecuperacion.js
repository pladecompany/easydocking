"use strict";
const { Modelo } = require("./Modelo");

class TokenRecuperacion extends Modelo {
  static get tableName() {
    return "token_recuperacion";
  }
}

module.exports = TokenRecuperacion;
