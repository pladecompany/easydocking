"use strict";
const moment = require("moment");
const { Modelo } = require("./Modelo");

class Rooms_chats extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "rooms_list";
    }
    $afterGet(queryContext) {
        return this.ag();
    }
    async datos() {
        this.fec_reg_room = moment(this.fec_reg_room).format("YYYY-MM-DD HH:mm:ss");
    }

    async ag() {
        await this.datos();
    }
}
module.exports = Rooms_chats;
