"use strict";
const { raw } = require("objection");
const { Modelo } = require("./Modelo");
const sendMail = require('../mail');
const Turno = require('../models/Turno');
const moment = require('moment');


class Emails extends Modelo {
  static get tableName() {
    return "emails";
  }


  static get tipo() {
    return {
      TURNO_APROBADO: 'turno_aprobado',
      TURNO_RECHAZADO: 'turno_rechazado',
      TURNO_EDITADO: 'turno_editado',
      TURNO_PRESENTADO: 'turno_presentado',
    };
  }



// -2: Cancelado por predio
// -1: Cancelado por usuario
// 0: Sin aprobar
// 1: Aprobado
// 2: Ingresado
// 3: Vencido


  static reemplazarVariables(str, obj) {
    for (let [varname, value] of Object.entries(obj)) {
      if (str.includes(varname)) {
        str = str.split(varname).join(value);
      }
    }
    return str;
  }


  static async variablesPorIdTurno(id_turno) {
    const turno = await Turno.query()
      .select(
        'turno.*',
        'chofer.nombre AS nombre_chofer',
        'predio.nombre AS nombre_predio',
        raw("DATE_FORMAT(turno.fecha, '%d/%m/%Y')").as('fecha_dmy'),
        raw("DATE_FORMAT(turno.fecha, '%H:%i %p')").as('hora'),
      )
      .join('chofer', 'chofer.id', 'turno.id_chofer')
      .join('predio', 'predio.id', 'turno.id_predio')
      .where('turno.id', id_turno)
      .first();

      return {
        '$nombre': turno.nombre_chofer,
        '$fecha_turno': turno.fecha_dmy,
        '$hora_turno': turno.hora,
        '$predio': turno.predio,
      };
  }


  static async enviarTurnoPorEstatus(id_turno, estatus, destinatario, variables) {
    const turno = await Turno.query()
      .select(
        'turno.*',
        'chofer.nombre AS nombre_chofer',
      )
      .join('chofer', 'chofer.id', 'id_chofer')
      .where('turno.id', id_turno);

    const correo = this

    this.reemplazarVariables();

    return new Promise((resolve, reject) => {
      const { destinatario, encabezado, cuerpo, variables } = op;
      return sendMail('Turno_correo_base.html', {encabezado,cuerpo})
        .then(resolve).catch(reject);
    });
  }
}

module.exports = Emails;
