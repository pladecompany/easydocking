"use strict";
const { Modelo } = require("./Modelo");

class Detalles_Predio extends Modelo {
  static get tableName() {
    return "detalles_predio";
  }
}

module.exports = Detalles_Predio;
