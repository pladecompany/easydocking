"use strict";
const { Modelo } = require("./Modelo");

class Tipo_Vehiculo extends Modelo {
  static get tableName() {
    return "tipo_vehiculo";
  }
}

module.exports = Tipo_Vehiculo;
