"use strict";
const { Modelo } = require("./Modelo");
const Admin = require("./Admin");
const Chofer = require("./Chofer");

const Firebase = new (require("../routes/firebase"))();

class Notificacion extends Modelo {
  static get tableName() {
    return "notificaciones";
  }

  static get usuario() {
    return {
      ADMIN: 'admin',
      CHOFER: 'chofer',
      TODOS: 'todos',
    };
  }


  static async emitir(op) {
    const titulo = op.titulo;
    const contenido = op.contenido || op.cuerpo;

    const id_receptor = op.id_receptor || null;
    const usuario_receptor = op.usuario_receptor;

    const id_emisor = op.id_emisor || null;
    const usuario_emisor = op.usuario_emisor;

    const con_push = op.push || op.firebase;
    const id_usable = op.id_usable || null;

    if (!Object.values(this.usuario).includes(usuario_receptor))
      return console.error('Error al emitir notificacion: usuario_receptor no valido: ', usuario_receptor);


    const notificaciones = [];

    if (id_receptor) // Un solo receptor.
    {
      notificaciones.push({
        titulo: titulo,
        contenido: contenido,
        usuario_receptor: usuario_receptor,
        id_receptor: id_receptor,
        usuario_emisor: usuario_emisor,
        id_emisor: id_emisor,
        id_usable: id_usable,
      });
    }

    else // Multiples receptores.
    {
      // A todos los admins.
      if (usuario_receptor == this.usuario.ADMIN || usuario_receptor == this.usuario.TODOS)
      {
        const ids_admin = await this._listar_ids_admin();

        ids_admin.forEach(id => notificaciones.push({
          titulo: titulo,
          contenido: contenido,
          usuario_receptor: this.usuario.ADMIN,
          id_receptor: id,
          id_usable: id_usable,
          usuario_emisor: usuario_emisor,
          id_emisor: id_emisor,
        }));
      }

      // A todos los choferes.
      if (usuario_receptor == this.usuario.CHOFER || usuario_receptor == this.usuario.TODOS)
      {
        const ids_chofer = await this._listar_ids_chofer();

        ids_chofer.forEach(id => notificaciones.push({
          titulo: titulo,
          contenido: contenido,
          usuario_receptor: this.usuario.CHOFER,
          id_receptor: id,
          id_usable: id_usable,
          usuario_emisor: usuario_emisor,
          id_emisor: id_emisor,
        }));
      }
    }

    const notif = await this
      .query()
      .insertGraph(notificaciones)
      .then(async() => {
        notificaciones.forEach(not => {
          IO.sockets.emit(
            not.usuario_receptor==this.usuario.TODOS ? not.usuario_receptor : not.usuario_receptor+not.id_receptor,
            not
          );
        });

        if (con_push) {
          for (let not of notificaciones) {
            const datap = {
              id_usuario: not.id_receptor,
              title: not.titulo,
              body: not.contenido,
              tablausuario: not.usuario_receptor,
            };
            await Firebase.enviarPushNotification(datap, function(datan) { console.log(datan); });
          }
        }
      })
      .catch(console.error);

    return notif;
  }


  static _listar_ids_admin() {
    return new Promise((resolve, reject) => {
      Admin.query()
        .select('id')
        .then(data => resolve(data.map(e => e.id)))
        .catch(reject);
    });
  }
  static _listar_ids_chofer() {
    return new Promise((resolve, reject) => {
      Chofer.query()
        .select('id')
        .then(data => resolve(data.map(e => e.id)))
        .catch(reject);
    });
  }


}

module.exports = Notificacion;
