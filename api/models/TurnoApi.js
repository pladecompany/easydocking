"use strict";
const {raw} = require('objection');
const { Modelo } = require("./Modelo");
const Detalles_Turnos = require('../models/Detalles_Turnos');
const Predio = require('../models/Predio');
const Vehiculo = require('../models/Vehiculo');
const Chofer = require('../models/Chofer');

class Turno extends Modelo {
  static get tableName() {
    return "turno";
  }

    $afterGet(queryContext) {
    	return this.ag();
    }

    async detalles() {
        if(this.id){
            var res = await Detalles_Turnos.query()
                .where("id_turno", this.id).select('titulo','respuesta');
            if (res[0]){
            	var valor = res;
            }
            else var valor = [];
            this.detalles = valor;
        }
    }

    async predio() {
        if(this.id_predio){
            var res = await Predio.query()
                .where("id", this.id_predio).select('nombre');
            if (res[0]) var valor = res[0].nombre;
            else var valor = '';
            this.predio = valor;
        }
    }

    async vehiculo() {
        if(this.id_vehiculo){
            var res = await Vehiculo.query().where("id", this.id_vehiculo).select('patente','vencimiento',"vencimiento_seguro","vencimiento_vtv","img","img_vtv","img_seguro","img_clausula","img_art");
            if (res[0]) var valor = res[0];
            else var valor = [];
            this.vehiculo = valor;
        }
    }

    async chofer() {
        if(this.id_chofer){
            var res = await Chofer.query().where("id", this.id_chofer).select('nombre','apellido','correo','telefono','lat_cho','lon_cho','dni',raw('(SELECT t.cod FROM cod_pais t WHERE t.id=id_cod_telefono)').as('cod_tlf'));
            if (res[0]) var valor = res[0];
            else var valor = [];
            this.chofer = valor;
        }
    }

    async ag() {
        await this.predio();
        await this.vehiculo();
        await this.detalles();
        await this.chofer();
    }
}

module.exports = Turno;
