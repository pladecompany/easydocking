"use strict";
const moment = require("moment");
const { Modelo } = require("./Modelo");
const Chofer = require("../models/Chofer");
const Predio = require("../models/Predio");

class Rooms_chats extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "rooms_chats";
    }
    $afterGet(queryContext) {
        return this.ag();
    }
    async datos() {
        if(this.id_usuario && this.tipo_usuario == "chofer") {

            var u = await Chofer.query()
                .where("id", this.id_usuario).catch(err => { });
            if (u[0]) var usu = u[0];
            else var usu = { nombre: '', apellido: ''};
            this.nombre = usu.nombre + " " + usu.apellido;
        } else if(this.id_usuario && this.tipo_usuario == "predio") {

            var u = await Predio.query()
                .where("id", this.id_usuario).catch(err => { });
            if (u[0]) var usu = u[0];
            else var usu = { nombre: ''};
            this.nombre = usu.nombre;
        }

        this.fec_reg_mensaje = moment(this.fec_reg_mensaje).format("YYYY-MM-DD HH:mm:ss");
        this.hidden = true;
        if (this.tipo_msj == 2) {
            this.url_img = "";
            this.button_toggle = false;
        }
    }

    async ag() {
        await this.datos();
    }
}
module.exports = Rooms_chats;
