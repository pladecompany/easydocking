"use strict";
const { Modelo } = require("./Modelo");

class Vehiculo extends Modelo {
  static get tableName() {
    return "vehiculo";
  }
}

module.exports = Vehiculo;
