"use strict";
const { Modelo } = require("./Modelo");
const Detalles_Turnos = require('../models/Detalles_Turnos');
const Predio = require('../models/Predio');
const Vehiculo = require('../models/Vehiculo');

class Turno extends Modelo {
  static get tableName() {
    return "turno";
  }

    $afterGet(queryContext) {
    	return this.ag();
    }

    async detalles() {
        if(this.id){
            var res = await Detalles_Turnos.query()
                .where("id_turno", this.id);
            if (res[0]){
            	for (var i = 0; i < res.length; i++) {
			        if(res[i].segmenta)
			          res[i].segmenta = true;
			        else
			          res[i].segmenta = false;
			    }
            	var valor = res;
            }
            else var valor = [];
            this.detalles = valor;
        }
    }

    async predio() {
        if(this.id_predio){
            var res = await Predio.query()
                .where("id", this.id_predio).select('nombre');
            if (res[0]) var valor = res[0].nombre;
            else var valor = '';
            this.predio = valor;
        }
    }

    async vehiculo() {
        if(this.id_vehiculo){
            var res = await Vehiculo.query().where("id", this.id_vehiculo);

            if (res[0]) var valor = res[0];
            else var valor = [];
            this.vehiculo = valor;
        }
    }

    async ag() {
        await this.predio();
        await this.vehiculo();
        await this.detalles();
    }
}

module.exports = Turno;
