"use strict";
const { Modelo } = require("./Modelo");
const Detalles_Predio = require('../models/Detalles_Predio');

class Predio extends Modelo {
  static get tableName() {
	return "predio";
  }

	static get relationMappings() {
		const Usuario = require('./Usuario');
		return {

      'usuario': {
        relation: Modelo.ManyToManyRelation,
        modelClass: Usuario,
        join: {
          from: 'predio.id',
          through: {
            from: 'usuario_predio.id_predio',
            to: 'usuario_predio.id_usuario',
          },
          to: 'usuario.id'
        },
      },

		};
	}

	$afterGet(queryContext) {
		return this.ag();
	}

	async detalles() {
		if(this.id){
			var res = await Detalles_Predio.query()
				.where("id_predio", this.id);
			if (res[0]){
				for (var i = 0; i < res.length; i++) {
					if(res[i].segmenta)
					  res[i].segmenta = true;
					else
					  res[i].segmenta = false;
					if(res[i].obligatorio)
					  res[i].obligatorio = true;
					else
					  res[i].obligatorio = false;
				}
				var valor = res;
			}
			else var valor = [];
			this.detalles = valor;
		}
		this.vistos = 0;
	}

	async ag() {
		await this.detalles();
	}
}

module.exports = Predio;
