"use strict";
const { Modelo } = require("./Modelo");

class Admin extends Modelo {
  static get tableName() {
    return "admin";
  }
}

module.exports = Admin;
