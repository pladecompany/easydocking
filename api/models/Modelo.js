const { Model } = require("objection");
const { DbErrors } = require("objection-db-errors");

class Modelo extends DbErrors(Model) {}

module.exports = {
  Modelo
};
