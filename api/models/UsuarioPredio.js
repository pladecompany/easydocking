"use strict";
const { Modelo } = require("./Modelo");

class UsuarioPredio extends Modelo {
  static get tableName() {
    return "usuario_predio";
  }
}

module.exports = UsuarioPredio;
