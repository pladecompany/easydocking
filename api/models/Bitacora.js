"use strict";
const moment = require("moment");
const { Modelo } = require("./Modelo");

class Bitacora extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "bitacora";
    }
    $afterGet(queryContext) {
        return this.ag();
    }
    async datos() {
        if(this.fecha)
            this.fecha = moment(this.fecha).format("DD-MM-YYYY HH:mm");
    }

    async ag() {
        await this.datos();
    }
}
module.exports = Bitacora;
