"use strict";
const { Modelo } = require("./Modelo");
const Vehiculo = require('../models/Vehiculo');
const Asignar_Predio = require('../models/Asignar_Predio');
const Cod_Telefono = require('../models/Cod_Telefono');

class Chofer extends Modelo {
  	static get tableName() {
    	return "chofer";
  	}

  	$afterGet(queryContext) {
    	return this.ag();
  	}

  	async vehiculos() {
        if(this.id){
            var res = await Vehiculo.query()
                .select('vehiculo.*', 'tipo_vehiculo.nombre AS tipo_vehiculo', 'tipo_vehiculo.vtv')
                .where("id_chofer", this.id)
                .leftJoin('tipo_vehiculo', 'vehiculo.tipo', 'tipo_vehiculo.id');
            if (res[0]) var valor = res;
            else var valor = [];
            this.vehiculos = valor;
        }
    }
    async predios() {
        if(this.id){
            var res = await Asignar_Predio.query().join('predio','predio.id','asignar_predio.id_predio')
                .where("asignar_predio.id_chofer", this.id).select('asignar_predio.*','predio.nombre','predio.url','predio.estatus','predio.horas_maxima');
            if (res[0]) var valor = res;
            else var valor = [];
            this.predios = valor;
        }
    }
    async codtelefono() {
        if(this.id_cod_telefono){
            var res = await Cod_Telefono.query()
                .where("id", this.id_cod_telefono);
            if (res[0]) var valor = res;
            else var valor = [];
            this.codtelefono = valor;
        }
    }

    async ag() {
        await this.vehiculos();
        await this.predios();
        await this.codtelefono();
    }
}

module.exports = Chofer;
