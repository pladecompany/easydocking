"use strict";
const moment = require("moment");
const { Modelo } = require("./Modelo");

class Horarios extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "horarios";
    }
    $afterGet(queryContext) {
        return this.ag();
    }
    async datos() {
        if(this.fecha)
            this.fecha = moment(this.fecha).format("YYYY-MM-DD");
        if(this.hora_a)
            this.hora_a = moment(`2021-01-01 ${this.hora_a}`).format("HH:mm");
        if(this.hora_b)
            this.hora_b = moment(`2021-01-01 ${this.hora_b}`).format("HH:mm");
        if(this.disponibilidad){
            if (parseInt(this.disponibilidad) == 0) {
                this.laborable = false;
            } else {
                this.laborable = true;
            }
        }
    }

    async ag() {
        await this.datos();
    }
}
module.exports = Horarios;
