"use strict";
const { Modelo } = require("./Modelo");

class Usuario extends Modelo {
  static get tableName() {
    return "usuario";
  }


  static get relationMappings() {
    const Predio = require('./Predio');
    return {
      'predios': {
        relation: Modelo.ManyToManyRelation,
        modelClass: Predio,
        join: {
          from: 'usuario.id',
          through: {
            from: 'usuario_predio.id_usuario',
            to: 'usuario_predio.id_predio',
          },
          to: 'predio.id'
        },
      },
    };

  }

}

module.exports = Usuario;
