//Importar librerias
const Knex = require("knex");
const morgan = require("morgan");
const express = require("express");
const promiseRouter = require("express-promise-router");
const bodyParser = require("body-parser");
const verificacionautomatica = require("./routes/verificar.js")
const cors = require('cors');
const os =  require("os");
const Server = os.hostname();
const knexConfig = require("./knexfile.js");
const https = require("https");
const fs = require("fs");

var cron = require('node-cron');

if (Server == knexConfig.Hosname) {
  var privateKey  = fs.readFileSync('/etc/ssl/private/inthecompanies.key', 'utf8');
  var certificate = fs.readFileSync('/etc/ssl/inthecompanies_com.crt', 'utf8');
  var ca = fs.readFileSync('/etc/ssl/inthecompanies_com.ca-bundle', 'utf8');
  var credentials = {ca, key: privateKey, cert: certificate};
}


//Importar Config
const { Model } = require("objection");

//Importar Validador de errores
const ValidadorDeErrores = require("./ValidarErrores");
var knex;

if (Server == knexConfig.Hosname) {
  knex = Knex(knexConfig.production);
} else {
    //knex = Knex(knexConfig.production);
    knex = Knex(knexConfig.development);
}

Model.knex(knex);

const router = promiseRouter();

//CORS middleware
const allowCrossDomain = function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
};

//Iniciar Express
const app = express()
  .use(express.json({limit: '999mb'}))
  .use(cors())
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(morgan("dev"))
  .use(router)
  // .use(allowCrossDomain)
  .set("json spaces", 2);
  app.all('*', function(req, res,next) {
    var responseSettings = {
      "AccessControlAllowOrigin": req.headers.origin,
      "AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
      "AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
      "AccessControlAllowCredentials": true
    };
    res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);

    if ('OPTIONS' == req.method) {
      res.sendStatus(200);
    }
    else {
      next();
    }


});

//Definir las rutas en la carpeta "routes"
definirRutas([
  'auth',
  'cod_telefono',
  'chofer',
  'predio',
  'vehiculo',
  'tipo_vehiculo',
  'admin',
  'notificacion',
  'rooms',
  'config',
  'tokensfirebase',
  'horarios',
  'emails',
]);
//
app.use(ValidadorDeErrores);

//Definir rutas de archivos
app.get("/public/uploads/:filename", function(req, res) {
  console.log(req.params.filename);
  res.sendFile(__dirname + `/public/uploads/${req.params.filename}`);
});

//Iniciar el servidor en un puerto
if (Server == knexConfig.Hosname){
  var httpsServer = https.createServer(credentials, app);
  httpsServer.listen(knexConfig.PORT);
}
else {
var server = app.listen(knexConfig.PORT, () => {
  console.log(
    "API Corriendo en puerto: %s",
    server.address().port
  );
});
}

//--------------------------------------------INICIO SOCKET-------------------
if (Server == knexConfig.Hosname){
  var io = require("socket.io")(httpsServer);
  } else {
  var io = require("socket.io")(server);
  }


  global.USUARIOSC = []; // para usar el arreglo de usuarios conectados en cualquier parte del api...(donde sea)
  global.IO = io; // para hacer emits desde cualquier parte del api (de donde sea)
  global.SOCKET = null;
  io.sockets.on("error", e => console.log(e));
  io.sockets.on("connection", socket => {
    
    global.SOCKET = socket;
    socket.pingTimeout = 100000;
    socket.upgradeTimeout = 100000;

    socket.emit("connect");

    socket.on("conectar", require("./socketEvents/conectar")(socket));

    socket.on("disconnect", require("./socketEvents/desconectar")(socket));


  });

  //-----------------------------------------------------------------FIN SOCKET

function definirRutas(rutas) {
  rutas.forEach(ruta => {
    app.use(`/${ruta}`, require(`./routes/${ruta}`));
  });
}


global.TOKENAPIEASY = '';
global.FECHA_TOKENAPIEASY = '';

//cron
cron.schedule("* * * * *", async function() {
  verificacionautomatica.verificarturnos();
});


cron.schedule("0 7 * * *", async function() {
  verificacionautomatica.verificadocumentos();
});

