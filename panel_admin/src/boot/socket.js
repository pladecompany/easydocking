import io from 'socket.io-client';
import config from '../config';

export default async ({ Vue }) => {
	Vue.prototype.$socket= await io(config.API_URL) //route to the server where you started your socket (and its port)
}