import axios from 'axios'
import config from '../config'
import { LocalStorage } from 'quasar'

// Crear instancia de axios.
const axiosAPI = axios.create({
  baseURL: config.API_URL,
})

// Configurar los headers globalmente.
Object.assign(axiosAPI.defaults.headers.common, config.headers)

// Incluir token en cada peticion.
axiosAPI.interceptors.request.use(
  config => {
    const token = LocalStorage.getItem('TOKEN')
    const id = LocalStorage.getItem('ID')
    const type = LocalStorage.getItem('TYPE')

    if (token) {
      config.headers.token = token
    }
    if (id) {
      config.headers.id_usuario = id
    }
    config.headers.tipo_usuario = type;
    return config
  },
  error => { Promise.reject(error) }
)

// Para que este disponible en los componentes y en store.
export default ({ store, Vue }) => {
  Vue.prototype.$axios = axios
  store.$axios = axios

  Vue.prototype.$api = axiosAPI
  store.$api = axiosAPI

  axiosAPI.interceptors.response.use(
    response => response,
    error => {
      if(error.response) {
        const type = LocalStorage.getItem('TYPE')
        const {status} = error.response;
        if (status === 401) {
          LocalStorage.set('error_sesion', 1); // Notificar en el Login.
          let uri = `#/Login${type == 'admin' ? '':'_predio'}`;
          store.dispatch('auth/salir')
          .then(() =>  {
            document.location.href = uri;
          });
        }
      }
      return Promise.reject(error);
    }
  );

}
