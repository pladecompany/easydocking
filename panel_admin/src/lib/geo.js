    export default function geo(){
        return new Promise((resolve, reject) => {
          var device = document.addEventListener("deviceready", function() {
            if (canRequest == "") {
              cordova.plugins.locationAccuracy.canRequest(canrequest, ErrorCant);
            } else {
              //canrequest();
            }
            function canrequest() {
              canRequest = 1;
              cordova.plugins.locationAccuracy.request(
                function(success) {
                  navigator.geolocation.getCurrentPosition(
                      function(pos) {
                        resolve({
                            lat: pos.coords.latitude,
                            lon: pos.coords.longitude
                        });
                      },
                      function(error) {
                          reject(error);
                      }
                  );
                },
                function(error) {
                  console.log(error);
                  navigator.geolocation.getCurrentPosition(
                    function(pos) {
                      resolve({
                        lat: pos.coords.latitude,
                        lon: pos.coords.longitude
                      });
                    },
                    function(error) {
                      reject(error);
                    }
                  );
                  if (
                    error.code !==
                    cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED
                  ) {
                      reject(error);
                    }
                },
                cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY
              );
            }
            function ErrorCant(error) {
                reject(error);
            }
          });
          if (!device) {
            navigator.geolocation.getCurrentPosition(
              function(pos) {
                resolve({
                    lat: pos.coords.latitude,
                    lon: pos.coords.longitude
                });
              },
              function(error) {
                reject(error);
              }
            );
          }
        })
    }

