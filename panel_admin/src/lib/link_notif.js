export default {


  notif_ir(row) {
    const path = this.notif_get_path(row, true);
    if (path) {
      this.$router.push({ path: path });
    }
  },


  notif_tiene_link(row) {
    return Boolean(this.notif_get_path(row));
    // if (this.get_path(row)) {
    //   return true;
    // }
    // return false;
  },


  notif_get_path(row, guardar_accion=false) {
    let path, accion;

    if (row.contenido.includes('vehículo') && row.contenido.includes('registrado')) {
      accion = { id:row.id_emisor, accion:'ver' };
      path = '/Choferes'
    }

    if (row.contenido.includes('chofer') && row.contenido.includes('pendiente') && row.contenido.includes('aprobar')) {
      accion = { id: row.id_usable, accion: 'aprobar' };
      path = '/Choferes'
    }

    if (row.contenido.includes('chofer') && row.contenido.includes('solicitado') && row.contenido.includes('predios')) {
      accion = { id: row.id_usable, accion: 'aprobar_predios' };
      path = '/Choferes'
    }


    if (guardar_accion) {
      // if (this.$route.fullPath == path) {
      //   path += '?_'
      // }
      this.$q.localStorage.set('accion_chofer', accion);
    }

    return path;
  },

}