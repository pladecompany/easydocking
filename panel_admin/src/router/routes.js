
const routes = [
  // Redirigir al home.
  {
    path: '/',
    beforeEnter: (to, from, next) => next('/Login')
  },

  {
    path: '/',
    component: () => import('layouts/Menu.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Bienvenido', path: '/Bienvenido', component: () => import('pages/Bienvenido.vue') },
      { name: 'Administradores', path: '/Administradores', component: () => import('pages/Administradores.vue') },
      { name: 'Choferes', path: '/Choferes', component: () => import('pages/Choferes.vue') },
      // { name: 'Ingresos', path: '/Ingresos', component: () => import('pages/Ingresos.vue') },
      { name: 'Predios', path: '/Predios', component: () => import('pages/Predios.vue') },
      // { name: 'Operaciones', path: '/Operaciones', component: () => import('pages/Operaciones.vue') },
      { name: 'Vehiculos', path: '/Vehiculos', component: () => import('pages/Vehiculos.vue') },
      { name: 'Paises', path: '/Paises', component: () => import('pages/Paises.vue') },
      { name: 'Notificaciones', path: '/Notificaciones', component: () => import('pages/Notificaciones.vue') },
      // { name: 'Usuarios', path: '/Usuarios', component: () => import('pages/Usuarios.vue') },
      { name: 'Horarios', path: '/Horarios', component: () => import('pages/user-predio/Horarios.vue') },
      { name: 'Calendario', path: '/Calendario', component: () => import('pages/user-predio/Calendario.vue') },
      { name: 'Horario-Listado', path: '/Horario-Listado/:fecha', component: () => import('pages/user-predio/Listado.vue') },
      { name: 'Emails', path: '/Emails', component: () => import('pages/Emails.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Empty.vue'),
    meta: { requiresAuth: true },
    children: [
      { name: 'Chat', path: '/Chat/:predio/:chofer?/:token?', component: () => import('pages/Chat.vue') },
      // { name: 'Usuarios', path: '/Usuarios', component: () => import('pages/Usuarios.vue') },
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Seguimiento', path: '/Seguimiento/:predio/:token', component: () => import('pages/Seguimiento.vue') },
      { name: 'Seguimiento', path: '/Seguimiento/:predio/:chofer/:token', component: () => import('pages/Seguimiento.vue') },
    ]
  },
/*
  {
    path: '/Usuarios',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Usuarios',path: '/Usuarios', component: () => import('pages/Usuarios.vue') },
    ]
  },*/
   {
    path: '/Login',
    component: () => import('layouts/Empty.vue'),
    children: [
      { name: 'Login',path: '/Login', component: () => import('pages/Login.vue') },
      { name: 'Login_predio',path: '/Login_predio', component: () => import('pages/user-predio/LoginUsers.vue') },
    ]
  },
]

// No encontrado.
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue')
})

export default routes
