import { LocalStorage } from 'quasar'
import { API_URL } from '../../config';


function hours_diff(timestamp1, timestamp2) {
  return (timestamp2 - timestamp1) / 1000 / 60 / 60;
}

function defaultState() {
  return {
    zonas: LocalStorage.getItem('TZ_ZONAS'),
    last_fetch: LocalStorage.getItem('TZ_LASTFETCHED'),
    apis: [
      'https://worldtimeapi.org/api/timezone',
      API_URL + 'horarios/zonas?tipo_usuario=admin&token='+LocalStorage.getItem('TOKEN'),
      'http://worldtimeapi.org/api/timezone'
    ],
  }
}

const getters = {
  zonas: state => state.zonas,
  apis: state => state.apis,
}

const mutations = {
  SET_ZONAS: (state, zonas) => {
    state.zonas = zonas;
    LocalStorage.set('TZ_ZONAS', zonas);
    LocalStorage.set('TZ_LASTFETCHED', Date.now());
  },
};

const actions = {
  update(ctx) {
    return new Promise(async (resolve, reject) => {
      if (LocalStorage.getItem('TZ_ZONAS')) {
        const last = LocalStorage.getItem('TZ_LASTFETCHED');

        if (hours_diff(Date.now(), last) < 336) {
          return resolve(LocalStorage.getItem('TZ_ZONAS'));
        }
      }

      await this.$api.get('/config/zonas_horarias')
        .then(res => {
          const zonas = res?.data;
          if (zonas && !ctx.getters.zonas) {
            ctx.commit('SET_ZONAS', zonas);
          }
        })

      let r = null;
      for (let API of ctx.getters.apis) {
        await this.$axios({
          baseURL: API,
          method: 'GET',
        })
        .then(res => {
          const zonas = res?.data;
          if (!zonas) {
            r = false;
          }
          else {
            r = true;
            ctx.commit('SET_ZONAS', zonas);
            resolve(zonas);
          }
        })
        .catch(err => {
          r = false;
        });

        if (r) {
          break;
        }
      }
      reject();
    });
  },


  fetch(ctx, tz) {
    return new Promise(async(resolve, reject) => {
      let r = false;
      let lap = 0;
      for (let API of ctx.getters.apis) {
        await this.$axios({
          baseURL: API,
          url: tz,
          method: 'GET',
        })
        .then(res => {
          r = true;
          resolve(res.data);
        })
        .catch(err => {
          r = false;
          console.error('');
        });
        if (r) break;
      }

    });
  },
};


export default {
  namespaced: true,
  state: defaultState,
  mutations,
  getters,
  actions,
}

