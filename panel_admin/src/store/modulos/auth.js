import { LocalStorage } from 'quasar'

function defaultState() {
  return {
    token: LocalStorage.getItem('TOKEN'),
    datos: LocalStorage.getItem('DATOS'),
    type: LocalStorage.getItem('TYPE'),
    id: LocalStorage.getItem('ID'),
    checking_token: false,
    telefono_soporte: null,
    predios: LocalStorage.getItem('PREDIOS'),
  }
}


const getters = {
  token: state => state.token,
  logueado: state => Boolean(state.token),
  datos: state => state.datos,
  type: state => state.type,
  id: state => state.id,
  checking_token: state => state.checking_token,
  telefono_soporte: state => state.telefono_soporte,
  predios: state => state.predios || [],
  predio_seleccionado: state => (state.predios || []).find(e => e.id==state.datos?.id_predio),
}


const mutations = {
  SET_ID: (state, id) => {
    state.id=id
    LocalStorage.set('ID', id)
  },
  SET_TOKEN: (state, token) => {
    state.token=token;
    LocalStorage.set('TOKEN', token)
  },
  SET_DATOS: (state, datos) => {
    state.datos=datos;
    LocalStorage.set('DATOS', datos)
  },
  SET_TYPE: (state, type) => {
    state.type=type;
    LocalStorage.set('TYPE', type)
  },
  SET_CHECKING_TOKEN: (state, v) => {
    state.checking_token = v;
  },
  SET_TELEFONO_SOPORTE: (state, v) => {
    state.telefono_soporte = v;
  },
  SET_PREDIOS: (state, v) => {
    state.predios = v;
    LocalStorage.set('PREDIOS', v)
  },

  SET_ID_PREDIO: (state, v) => {
    state.datos.id_predio = v;
    LocalStorage.set('DATOS', state.datos)
  },

  DEL_ID: (state) => {
    state.id=null;
    LocalStorage.remove('ID')
  },
  DEL_TYPE: (state) => {
    state.type=null;
    LocalStorage.remove('TYPE')
  },
  DEL_TOKEN: (state) => {
    state.token=null;
    LocalStorage.remove('TOKEN')
  },
  DEL_DATOS: (state) => {
    state.datos=null;
    LocalStorage.remove('DATOS')
  },
  DEL_TELEFONO_SOPORTE: (state) => {
    state.telefono_soporte = null;
  },
  DEL_PREDIOS: (state, v) => {
    state.predios = null;
  },
  DEL_ID_PREDIO: (state, v) => {
    state.datos.id_predio = null;
  },

  RESET_STATE: state => Object.assign(state, defaultState()),
}


const actions = {
  _process_login_data(ctx, datos) {
    return new Promise((resolve, reject) => {
      ctx.commit('SET_ID', datos.id)
      ctx.commit('SET_DATOS', datos)
      ctx.commit('SET_TYPE', datos.type)
      ctx.commit('SET_TOKEN', datos.token)
      ctx.commit('SET_PREDIOS', datos.predios);
      resolve(datos)
    })
  },

  _process_self_token(ctx, token) {
    return new Promise((resolve, reject) => {
      ctx.commit('SET_TOKEN', token)
      resolve(token)
    })
  },

  login(ctx, datos) {
    const correo = datos.email || datos.correo
    const pass = datos.pass || datos.password || datos.clave
    return new Promise((resolve, reject) => {
      this.$api.post('/auth/login', {correo,pass})
        .then(resp => {
          const data = resp.data
          if(!data && !data.token){
            return reject(resp)
          }
          ctx.dispatch('_process_login_data', data).then(resolve, reject)
        })
        .catch(reject)
    })
  },

  login_user(ctx, datos) {
    const correo = datos.email || datos.correo
    const pass = datos.pass || datos.password || datos.clave
    return new Promise((resolve, reject) => {
      this.$api.post('/auth/login-user', {correo,pass})
        .then(resp => {
          const data = resp.data
          if(!data && !data.token){
            return reject(resp)
          }
          ctx.dispatch('_process_login_data', data).then(resolve, reject)
        })
        .catch(reject)
    })
  },

  salir(ctx) {
    return new Promise((resolve, reject) => {
      ctx.commit('DEL_ID')
      ctx.commit('DEL_DATOS')
      ctx.commit('DEL_TYPE')
      ctx.commit('DEL_TOKEN')
      ctx.commit('DEL_PREDIOS')
      resolve()
    })
  },

  seleccionar_predio(ctx, id_predio) {
    ctx.commit('SET_ID_PREDIO', id_predio);
  },

  fetch(ctx) {
    /* Recuperar los datos cuando se esta logueado */
    return new Promise((resolve, reject) => {
      let uri = '/admin/'+ctx.getters['id'];
      if (ctx.getters['type'] === "user-predio") {
          uri = '/auth/user-predio/'+ctx.getters['id'];
      }
      this.$api.get(uri)
        .then(resp => {
          const data = resp.data
          if (!data) return reject(resp)
          ctx.dispatch('_process_login_data', data)
        })
        .catch(reject);
    });
  },

  check_token(ctx) {
    /*
     * Verificar si el token todavia es valido
     * 1: valido
     * 0: invalido
     * -1: error (no se pudo comprobar)
     */
    return new Promise((resolve, reject) => {
      if (ctx.getters['checking_token']) {
        return resolve(-1);
      }
      ctx.commit('SET_CHECKING_TOKEN', true)

      const token = LocalStorage.getItem('TOKEN')
      this.$api.post('/auth/check-token', { token })
        .then(() => {
          ctx.commit('SET_TOKEN', token)
          resolve(1)
        })
        .catch(r => {
          const online = r.response && r.request;
          if (r.response && r.response.status == 401)
            resolve(0)
          else
            resolve(-1)
        })
        .finally(() => ctx.commit('SET_CHECKING_TOKEN', false))

    });
  },

  fetch_telefono_soporte(ctx) {
    return new Promise((resolve, reject) => {
      this.$api
        .get('/config/telefono_soporte')
        .then(res => {
          ctx.commit('SET_TELEFONO_SOPORTE', res.data.telefono_soporte);
          resolve(res.data);
        })
        .catch(reject)
    });
  },

}


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
