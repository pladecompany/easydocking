import { LocalStorage } from 'quasar'


function defaultState() {
  return {
    notificaciones: [],
  }
}

const getters = {
  notificaciones: state => state.notificaciones,
  pendientes: state => (state.notificaciones||[]).filter(n => n.estatus==0).length,
  numero: state => state.notificaciones?.length || 0,
}

const mutations = {
  SET: (state, data) => { state.notificaciones = data },
  RESET_STATE: state => Object.assign(state, defaultState()),
}

const actions = {
  actualizar(ctx) {
    return new Promise((resolve, reject) => {
      const id_usu = ctx.rootGetters['auth/id'];
      if(id_usu && ctx.rootGetters["auth/type"] == 'admin'){
        const params = {
          id_usuario: id_usu,
        };
	      this.$api
	        .get('/notificacion', {params})
	        .then(res => {
	          ctx.commit('SET', res.data);
	          resolve(res.data);
	        })
	        .catch(reject)
    	}
    });
  },

  marcar_leidas(ctx) {
    return new Promise((resolve, reject) => {
      this.$api
        .put('/notificacion/estatus')
        .then(resolve)
        .catch(reject)
    });
  },
}


export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
