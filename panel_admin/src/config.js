/*
 * Parametros generales.
 */

const config = Object.freeze({
  //API_URL: 'https://inthecompanies.com:4441/',
  // API_URL: 'http://192.168.0.199:3000/',
  // API_URL: 'http://localhost:3000/',
  //API_URL: 'https://easy-docking.com.ar:49876/',
  API_URL: 'https://app.easy-docking.com.ar:8080/',
  empresa: '1',
  descuento: '0.1',
  loginURL: 'http://localhost:1210/login',
  headers: {
    // 'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'tipo-cliente': 'app',
  },
})

export default config

export const API_URL = config.API_URL
export const loginURL = config.loginURL
export const headers = config.headers
export const empresa = config.empresa
export const DESCUENT = config.descuento
