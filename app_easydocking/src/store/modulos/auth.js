import { LocalStorage } from 'quasar'

function defaultState() {
  return {
    token: LocalStorage.getItem('TOKEN') || null,
    tipoUsuario: null,
    data: LocalStorage.getItem('DATA') || null,
    conectado: LocalStorage.getItem('CONECTADO') || false,
    network: LocalStorage.getItem('NET') || null,
  }
}

const getters = {
  token: state => state.token,
  logueado: state => Boolean(state.token),
  tipoUsuario: state => state.tipoUsuario,
  data: state => state.data,
  conectado: state => state.conectado,
  network: state => state.network,
}


const mutations = {
  SET_ID: (state, id) => { state.id = id },
  SET_TOKEN: (state, token) => { state.token = token },
  SET_DATA: (state, data) => { state.data = data },
  SET_CONECTADO: (state, val) => { state.conectado = Boolean(val) },

  // DEL_ALL: state => state.replaceState(defaultState()),
  RESET_STATE: state => Object.assign(state, defaultState()),
}


const actions = {
  registrar(ctx, data) {
    return new Promise((resolve, reject) => {

      this.$api.post('/usuarios/registrar', data)
      // Promise.resolve()
        .then(resp => {
          if(resp.data && resp.data.r) {
            LocalStorage.set('NET', resp.data.network)
            LocalStorage.set('DATA', data)
            LocalStorage.set('CONECTADO', true)
            ctx.commit('RESET_STATE')
            return resolve(resp&&resp.data ? resp.data : resp)
          }

          reject(resp);
        })
        .catch(reject)
    })
  },

  conectar(ctx) {
    return new Promise((resolve, reject) => {
      LocalStorage.set('CONECTADO', true)
      ctx.commit('RESET_STATE')
    })
  },
  desconectar(ctx) {
    return new Promise((resolve, reject) => {
      LocalStorage.set('CONECTADO', false)
      ctx.commit('RESET_STATE')
      resolve();
    })
  },

  salir({commit}) {
    return new Promise((resolve, reject) => {
      LocalStorage.remove('DATA')
      LocalStorage.remove('CONECTADO')
      // LocalStorage.remove('NET')
      commit('RESET_STATE')
      resolve()
    })
  },

  fetch(ctx) {
    return new Promise((resolve, reject) => {
      this.$api.get('/login/data')
        .then(r => {
          if (!r.data || !r.data.name) return reject(r);

          LocalStorage.set('NET', r.data);
        });
    });
  },
}




export default {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions,
}
