import axios from "axios";
import {API_URL} from '../config'
import { Notify } from 'quasar'

export default { 
    Firebase: function(raiz) {
        var este = this;
        window.FirebasePlugin.getToken(function(tokenFirebase) {
            var data = {
                //id_usuario: store.data.id ? store.data.id : null,
                tokenFirebase: tokenFirebase,
                //tablausuario: t_u
            };
            axios
                .post(API_URL+"notificaciones/agregar-eliminar-firebase", data)
                .then(resp => { console.log(resp); })
                .catch(err => {
                    alert(err);
                });
        });

        window.FirebasePlugin.onTokenRefresh(function(tokenFirebase) {
            var data = {
                tokenFirebase: tokenFirebase,
            };
            axios
                .post(API_URL+"notificaciones/agregar-eliminar-firebase", data)
                .then(resp => { console.log(resp); })
                .catch(err => {
                    console.log(err);
                });
        });
        window.FirebasePlugin.onMessageReceived(function(data) {
            // App abierta
            if (data.tap) { 
                setTimeout(function(){
                    if(data.tipo_noti=='0'){
                        location.href = "index.html#/" + data.ruta + "";
                        //raiz.$router.push(data.ruta);
                    }else if(data.tipo_noti=='1'){
                        location.href = "index.html#/" + data.ruta + "/"+data.id_usable;
                        //raiz.$router.push(data.ruta+'/'+data.id_usable);
                    }
                }, 2000);
            }else{
                Notify.create({
                    message: 'Tienes una nueva notificación',
                    icon: 'notifications_active',
                    position: 'top'
                });
            }
            
            var googleMID = new Date().getTime();
            if (data.google.message_id) {
                googleMID = data.google.message_id;
            } 
        });
    },
    clickNotification(data) {
        if (data.tipo_noti) {

            var Bell = document.querySelector('#BellNoti');
            if (!Bell.classList.contains("__hasNoti")) {
                Bell.classList.add("__hasNoti");
            }
            alert(data.title);
            //M.toast({
            //    html: data.title
            //});
        }
    }
};
